<?php
namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Description of SortByDateForm
 *
 * @author kkk
 */
class SortByDateForm extends Model{
    
    public $ord = "DESC";

    /**
     * {@inheritdoc}
     */
    public function rules(){
        return [
            ['ord', 'myRule'],
        ];
    }
    
    public function myRule($attr){
        if(!in_array($this->$attr, ['DESC', 'ASC'])){
            $this->addError($attr, 'Ошибка! Не задано условие сортировки');
        }
    }
    
    public function getOrd(){
        if ($this->ord == "DESC") {$this->ord = "ASC";}
        else {$this->ord = "DESC";}
        return $this->ord;
    }
}
