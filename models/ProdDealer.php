<?php

namespace app\models;

use app\controllers\PartnersController;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "products".
 *
 * @property int $id ID записи в таблице продуктов
 * @property string|null $name Название товара
 * @property float|null $price Цена
 * @property float|null $volume Объем товара
 * @property string|null $uniq_id Уникальный индитификатор
 * @property string|null $img картинка к товару
 * @property string|null $home_page Страница с описание
 * @property int|null $active Наличие товара
 *
 * @property BasketItems[] $basketItems
 * @property LinkGoodsProducts[] $linkGoodsProducts
 */
class ProdDealer extends ActiveRecord{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'products';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['price', 'volume'], 'number'],
            [['active'], 'integer'],
            [['name'], 'string', 'max' => 256],
            [['uniq_id', 'img', 'home_page'], 'string', 'max' => 128],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID записи в таблице продуктов',
            'name' => 'Название товара',
            'price' => 'Цена',
            'volume' => 'Объем товара',
            'uniq_id' => 'Уникальный индитификатор',
            'img' => 'картинка к товару',
            'home_page' => 'Страница с описанием',
            'active' => 'Наличие товара',
        ];
    }

    /**
     * Gets query for [[BasketItems]].
     *
     * @return \yii\db\ActiveQuery|BasketItemsQuery
     */
    public function getBasketItems()
    {
        return $this->hasMany(BasketItems::className(), ['prod_id' => 'id']);
    }

    /**
     * Gets query for [[LinkGoodsProducts]].
     *
     * @return \yii\db\ActiveQuery|LinkGoodsProductsQuery
     */
    public function getLinkGoodsProducts()
    {
        return $this->hasMany(LinkGoodsProducts::className(), ['id_products' => 'id']);
    }
    /**
     * Получить весь список товаров
     * Такая сортиорвка обязательна для dealer/index (выбор товара в корзину) ORDER BY `g`.`id` ASC, `p`.`volume`
     * 
     * <b>На выходе массив масивов (результат метода queryAll() )</b>
     * <ul>
     *  <li>[prods_id] => 1</li>
     *  <li>[prods_name] => ЭМИЗ ПРЕМИУМ</li>
     *  <li>[prods_price] => 1000.00</li>
     *  <li>[prods_volume] => 0.50</li>
     *  <li>[prods_uniqid] => emiz_premium_05</li>
     *  <li>[prods_img] => products_premium_05.png</li>
     *  <li>[prods_homepage] => index</li>
     *  <li>[prods_active] => 1</li>
     *  <li>[goods_id] => 1</li>
     *  <li>[goods_name] => ЭМИЗ ПРЕМИУМ</li>
     *  <li>[goods_uniqid] => emiz_premium</li>
     *  <li>[goods_shortdesc] => ...</li>
     *  <li>[goods_notes] => ...|...</li>
     *  <li>[goods_sostav] => </li>
     *  <li>[goods_active] => 1</li>
     * </ul>
     * 
     * @return type
     */
    public function getAllProd(){
        $sql="SELECT `p`.`id`AS`prods_id`, 
            `p`.`name` AS `prods_name`,  
            `p`.`price` AS `prods_price`,
            `p`.`volume` AS `prods_volume`,
            `p`.`uniq_id` AS `prods_uniqid`,
            `p`.`img` AS `prods_img`,
            `p`.`home_page` AS `prods_homepage`,
            `p`.`active` AS `prods_active`,
            `g`.`id`AS`goods_id`,
            `g`.`name` AS `goods_name`,
            `g`.`uniq_id` AS `goods_uniqid`,
            `g`.`short_desc` AS `goods_shortdesc`,
            `g`.`notes` AS `goods_notes`,
            `g`.`sostav` AS `goods_sostav`,
            `g`.`active` AS `goods_active`
        FROM `products` `p`
        LEFT JOIN `link_goods_products` `lgp` ON(`lgp`.`id_products`=`p`.`id`)
        LEFT JOIN `goods` `g` ON(`g`.`id`=`lgp`.`id_goods`)
        WHERE `p`.`active`=1
        ORDER BY `g`.`id` ASC, `p`.`volume`";
        
        return Yii::$app->db->createCommand($sql)->queryAll();
    }
    
}
