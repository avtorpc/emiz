<?php
namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Filter admin/clients
 *
 * @author kkk
 */
class ClientForm extends Model{

    public $sort = 4;
    public $ord = " `d_in` ";
    public $count_c = 0;
    public $count_d = 0;
    public $list_items=[];

    /**
     * {@inheritdoc}
     */
    public function rules(){
        return [
            [['sort'], 'integer', 'message' => 'Ошибка! Не павильно задано условие сортировки'],
        ];
    }
    /**
     * Иницилизация модели
     */
    public function inits( $basket_table ){

        if($this->sort){
            if($this->sort == 1){//по сумме
                $this->ord = " `summ` ";
            }elseif($this->sort == 2){//по кол-ву товара
                $this->ord = " `tvr` ";
            }elseif($this->sort == 3){//по кол-ву заказов
                $this->ord = " `bsk` ";
            }elseif($this->sort == 4){//по дате регистрации
                $this->ord = " `d_in` ";
            }
        }

        $this->list_items=$this->listGroup( $basket_table );
        $this->getCount();
    }
    /**
     * Пункты OPTION сортировки
     *
     * @return type
     */
    public function getOptionSort(){
        return [
            1 => 'по сумме',
            2 => 'по кол-ву товара',
            3 => 'по кол-ву заказов',
            4 => 'по дате регистрации',
        ];
    }
    /**
     * Список купленных товаров
     *
     * @return type
     */
    private function listGroup( $basket_table ){
        $sql="
            (SELECT `lbi`.`id_basket` AS `fgr`, `b`.`user_id` AS `usr_id`, `b`.`id` AS `basket_id`, MAX(CONCAT(`b`.`name`, ' ', `b`.`surname`))AS`name`,
                MAX(`b`.`city`)AS`city`, MAX(`b`.`phone`)AS`phone`, COUNT(DISTINCT `lbi`.`id_basket`)AS`bsk`, COUNT(*)AS`tvr`,
                SUM(`i`.`unit_price` * `i`.`cnt`)AS`summ`, 'C' AS`utype`, MAX(`b`.`date_in`)AS`d_in`
            FROM " . $basket_table . " AS `b`   
            LEFT JOIN `link_basket_basket_items` `lbi` ON(`b`.`id`=`lbi`.`id_basket`)
            LEFT JOIN `basket_items` `i` ON(`i`.`id`=`lbi`.`id_basket_item`)
            WHERE `b`.user_id IS NULL
            GROUP BY `lbi`.id_basket )
        UNION ALL
            (SELECT `b`.`user_id` AS `fgr`, `b`.`user_id` AS `usr_id`, `b`.`id` AS `basket_id`, MAX(CONCAT(`b`.`name`, ' ', `b`.`surname`))AS`name`,
                MAX(`b`.`city`)AS`city`, MAX(`b`.`phone`)AS`phone`, COUNT(DISTINCT `lbi`.`id_basket`)AS`bsk`, COUNT(*)AS`tvr`,
                SUM(`i`.`unit_price` * `i`.`cnt`)AS`summ`, 'D' AS`utype`, MAX(`b`.`date_in`)AS`d_in`
            FROM " . $basket_table . " AS `b`   
            LEFT JOIN `link_basket_basket_items` `lbi` ON(`b`.`id`=`lbi`.`id_basket`)
            LEFT JOIN `basket_items` `i` ON(`i`.`id`=`lbi`.`id_basket_item`)
            WHERE `b`.user_id IS NOT NULL
            GROUP BY `b`.user_id )
            ORDER BY {$this->ord} DESC";

            return Yii::$app->db->createCommand($sql)->queryAll();
    }
    /**
     * Подсчет количества диллеров клиентов
     */
    private function getCount(){
        if($this->list_items){

            $dd = [];
            $dd = array_count_values(array_column($this->list_items, 'utype'));

            $this->count_c = $dd['C'];
            $this->count_d = $dd['D'];

            unset($dd);
        }
    }
}
