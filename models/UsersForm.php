<?php

namespace app\models;

use Yii;
use app\controllers\PartnersController;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "users".
 *
 * @property int $id ID записи в таблице пользователя
 * @property string $login Логин
 * @property string|null $password Пароль
 * @property string|null $role Роль на сайте
 * @property string|null $home_page Домашняя станица по умолчанию
 * @property string|null $nickname Короткое имя
 *
 * @property Basket[] $baskets
 * @property Basket[] $baskets0
 * @property LinkUsersAdress[] $linkUsersAdresses
 * @property Adress[] $adresses
 * @property LinkUsersAdrkont[] $linkUsersAdrkonts
 * @property Adress[] $adrkonts
 * @property LinkUsersCard[] $linkUsersCards
 * @property Card[] $cards
 * @property LinkUsersUsersInfo[] $linkUsersUsersInfos
 */
class UsersForm extends ActiveRecord{
    
    public $password_0 = null;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }
    /**
     * {@inheritdoc}
     * Внимание! Не делать [password - required] (т.к. на UPDATE пустое поле не меняет пароль)
     * Внимание! Не делать [login - unique] (т.к. есть еще форма UPDATE)
     */
    public function rules()
    {
        return [
            [['login'], 'required'],
            [['id'], 'integer'],
            [['password_0'], 'string', 'min' => 6],
            [['login', 'home_page', 'nickname'], 'string', 'max' => 128],
            [['password'], 'string', 'max' => 256],
            [['role'], 'string', 'max' => 10],
            
        ];
    }
   /**
    * Вернуть пароль в виде хеша
    * 
    * @return type
    */
    public function getPass(){
        return password_hash($this->password, PASSWORD_DEFAULT);
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID записи в таблице пользователя',
            'login' => 'Логин',
            'password' => 'Пароль',
            'password_0' => 'Пароль',
            'role' => 'Роль на сайте',
            'home_page' => 'Домашняя станица по умолчанию',
            'nickname' => 'Короткое имя',
        ];
    }
    /**
     * Получить личные данные зарегистрированного пользователя
     * 
     * <b>На выходе одномерный массив (результат метода queryOne() )</b>
     * <ul>
     *  <li>[users_id] => 3</li>
     *  <li>[users_login] => admin@ru.ru</li>
     *  <li>[users_role] => admin</li>
     *  <li>[users_homepage] => /admin/</li>
     *  <li>[users_nickname] => Great Dealer</li>
     *  <li>[info_name] => Марк</li>
     *  <li>[info_famuly] => Захаров</li>
     *  <li>[info_otchestvo] => Петрович</li>
     *  <li>[info_birth] => 1972-04-26</li>
     *  <li>[info_phone] => 8-916-777-7777</li>
     *  <li>[info_email] => psalec@yandex.ru</li>
     *  <li>[info_idtelegramm] => 123456789</li>
     *  <li>[info_organization] => ООО Чоп Москва ул Ленина</li>
     * </ul>
     * 
     * @param type $id
     * @return type
     */
    public function getCurrUser($id){
        return Yii::$app->db->createCommand("SELECT `u`.`id` AS `users_id`,
                                                    `u`.`login` AS `users_login`,
                                                    `u`.`role` AS `users_role`,
                                                    `u`.`home_page` AS `users_homepage`,
                                                    `u`.`nickname` AS `users_nickname`,
                                                    `i`.`name` AS `info_name`,
                                                    `i`.`famuly` AS `info_famuly`,
                                                    `i`.`otchestvo` AS `info_otchestvo`,
                                                    `i`.`birth` AS `info_birth`,
                                                    `i`.`phone` AS `info_phone`,
                                                    `i`.`email` AS `info_email`,
                                                    `i`.`id_telegramm` AS `info_idtelegramm`,
                                                    `i`.`organization` AS `info_organization`
                                                FROM `users` `u`
                                                LEFT JOIN `link_users_users_info` `lui`ON(`lui`.`id_users`=`u`.`id`)
                                                LEFT JOIN `user_info` `i`ON(`i`.`id`=`lui`.`id_users_info`)
                                                WHERE `u`.`id`=:id")->bindValues( [
                                                                                    ':id' => $id
                                                                                ] )->queryOne();

    }
    /**
     * Получить личные данные дилера с адресами
     * 
     * @param type $id
     * @return type
     */
    public function getCurrDealer($id){
        return Yii::$app->db->createCommand("SELECT `u`.`id` AS `users_id`,
                                                        `u`.`login` AS `users_login`,
                                                        `i`.`id` AS `info_id`,
                                                        `i`.`name` AS `info_name`,
                                                        `i`.`famuly` AS `info_famuly`,
                                                        `i`.`otchestvo` AS `info_otchestvo`,
                                                        `i`.`birth` AS `info_birth`,
                                                        `i`.`phone` AS `info_phone`,
                                                        `i`.`email` AS `info_email`,
                                                        `i`.`id_telegramm` AS `info_idtelegramm`,
                                                        `luk`.`adrkonts_id`,
                                                        `a`.`city`AS`adr_city`,
                                                        `a`.`street`AS`adr_street`,
                                                        `a`.`house`AS`adr_house`,
                                                        `a`.`korpus`AS`adr_korpus`,
                                                        `a`.`apartament`AS`adr_apartament`,
                                                        `a`.`pochta_index`AS`adr_pindex`,
                                                        (
                                                            SELECT COUNT(*)
                                                            FROM `basket` `b`
                                                            WHERE `b`.`user_id`=`u`.`id`
                                                        )AS`bsk`,
                                                        (
                                                            SELECT COUNT(*)
                                                            FROM `link_basket_basket_items` `lbi`
                                                            LEFT JOIN `basket` `b`ON(`b`.`id`=`lbi`.`id_basket`)
                                                            WHERE `b`.`user_id`=`u`.`id`
                                                        )AS`tvr`,
                                                        (
                                                            SELECT SUM((
                                                                SELECT SUM((`ii`.`unit_price` * `ii`.`cnt`))
                                                                FROM link_basket_basket_items `bb`
                                                                LEFT JOIN `basket_items` `ii` ON(`ii`.`id`=`bb`.`id_basket_item`)
                                                                WHERE `bb`.`id_basket`=`b`.`id`
                                                            ) + `b`.`transport`)
                                                            AS`summ_basket`
                                                            FROM `basket` `b`
                                                            WHERE `b`.`user_id`=`u`.`id`
                                                            GROUP BY `b`.`user_id`
                                                        )AS`summ`
                                                FROM `users` `u`
                                                LEFT JOIN `link_users_users_info` `lui`ON(`lui`.`id_users`=`u`.`id`)
                                                LEFT JOIN `user_info` `i`ON(`i`.`id`=`lui`.`id_users_info`)
                                                LEFT JOIN `link_users_adrkont` `luk`ON(`luk`.`users_id`=`u`.`id`)
                                                LEFT JOIN `adress` `a`ON(`luk`.`adrkonts_id`=`a`.`adress_id` AND `a`.`active`=1)
                                                WHERE `u`.`id`=:id")->bindValues( [
                                                                                    ':id' => $id
                                                                                ] )->queryOne();

    }
    /**
     * Gets query for [[Baskets]].
     *
     * @return \yii\db\ActiveQuery|BasketQuery
     */
    public function getBaskets()
    {
        return $this->hasMany(Basket::className(), ['dealer_id' => 'id']);
    }

    /**
     * Gets query for [[Baskets0]].
     *
     * @return \yii\db\ActiveQuery|BasketQuery
     */
    public function getBaskets0()
    {
        return $this->hasMany(Basket::className(), ['user_id' => 'id']);
    }

    /**
     * Gets query for [[LinkUsersAdresses]].
     *
     * @return \yii\db\ActiveQuery|LinkUsersAdressQuery
     */
    public function getLinkUsersAdresses()
    {
        return $this->hasMany(LinkUsersAdress::className(), ['users_id' => 'id']);
    }

    /**
     * Gets query for [[Adresses]].
     *
     * @return \yii\db\ActiveQuery|AdressQuery
     */
    public function getAdresses()
    {
        return $this->hasMany(Adress::className(), ['adress_id' => 'adress_id'])->viaTable('link_users_adress', ['users_id' => 'id']);
    }

    /**
     * Gets query for [[LinkUsersAdrkonts]].
     *
     * @return \yii\db\ActiveQuery|LinkUsersAdrkontQuery
     */
    public function getLinkUsersAdrkonts()
    {
        return $this->hasMany(LinkUsersAdrkont::className(), ['users_id' => 'id']);
    }

    /**
     * Gets query for [[Adrkonts]].
     *
     * @return \yii\db\ActiveQuery|AdressQuery
     */
    public function getAdrkonts()
    {
        return $this->hasMany(Adress::className(), ['adress_id' => 'adrkonts_id'])->viaTable('link_users_adrkont', ['users_id' => 'id']);
    }

    /**
     * Gets query for [[LinkUsersCards]].
     *
     * @return \yii\db\ActiveQuery|LinkUsersCardQuery
     */
    public function getLinkUsersCards()
    {
        return $this->hasMany(LinkUsersCard::className(), ['users_id' => 'id']);
    }

    /**
     * Gets query for [[Cards]].
     *
     * @return \yii\db\ActiveQuery|CardQuery
     */
    public function getCards()
    {
        return $this->hasMany(Card::className(), ['card_id' => 'card_id'])->viaTable('link_users_card', ['users_id' => 'id']);
    }

    /**
     * Gets query for [[LinkUsersUsersInfos]].
     *
     * @return \yii\db\ActiveQuery|LinkUsersUsersInfoQuery
     */
    public function getLinkUsersUsersInfos()
    {
        return $this->hasMany(LinkUsersUsersInfo::className(), ['id_users' => 'id']);
    }

}
