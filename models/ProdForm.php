<?php
/**
 * Created by PhpStorm.
 * User: avtorpc
 * Date: 2020-03-23
 * Time: 00:05
 */

namespace app\models;


use Yii;
use yii\base\Model;


class ProdForm extends Model {

    public $id = null;
    public $name = null;
    public $price = null;
    public $volume = null;
    public $goods_link = null;
    public $goods_name = null;
    public $uniq_id = null;
    public $h = null;
    public $w = null;
    public $n = null;
    public $active = null;
    public $list_goods = null;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[ 'name'], 'required', 'message' => 'Название товара не может быть пустым'],
            [[ 'price'], 'required', 'message' => 'Цена не может быть пустой'],
            [[ 'volume' ], 'required', 'message' => 'Объем товара не может быть пустым'],
            [[ 'uniq_id' ], 'required', 'message' => 'Уникальный идентификатор не может быть пустым'],
            [[ 'h' ], 'required', 'message' => 'Высота упаковки не может быть пустой'],
            [[ 'w' ], 'required', 'message' => 'Ширина упаковки не может быть пустой'],
            [[ 'n' ], 'required', 'message' => 'Вес товара может быть пустым'],        
            [[ 'id', 'name', 'price', 'volume', 'active', 'goods_link', 'uniq_id', 'h', 'w', 'n' ], 'trim']
        ];
    }


    public function getID(){

        return $this->id;
    }


    public function getName(){

        return $this->name;
    }

    public function getPrice(){

        return $this->price;
    }

    public function getVolume(){

        return $this->volume;
    }

    public function getGoodsLink(){

        return $this->goods_link;
    }

    public function getActive(){

        if( $this->active == 'checked' OR $this->active == 'on') {

            return 1;
        } else {

            return 0;
        }
    }

    public function getActiveForForm(){

        if( $this->active == 1 OR $this->active == 'checked' OR $this->active == 'on' ) {

            return 'checked';
        } else {

            return '';
        }
    }

    public function getListGoods(){

        return $this->list_goods;
    }

    public function getGoodsName(){

        return $this->goods_name;
    }

    public function getUniqID(){

        return $this->uniq_id;
    }
    
    public function getHeight() {
        
        return $this->h;
    }
    
    public function getWeigth(){
        
        return $this->w;
    }

    public function getNetto() {
        
        return $this->n;
    }

    public function setID( $value ){

        $this->id = trim( $value );
    }

    public function setName( $value ) {

        $this->name = trim( $value );
    }

    public function setPrice( $value ) {

        $this->price = trim( $value );
    }

    public function setVolume( $value ) {

        $this->volume = trim( $value );
    }

    public function setGoodsLink( $value ) {

        $this->goods_link = trim( $value );
    }

    public function setListGoods( $value ){

        $this->list_goods = $value;
    }

    public function setUniqID( $value ){

        $this->uniq_id = $value;
    }
    
    public function setHeight($value){
        
        $this->h = $value;
    }
    
    public function setWeigth($value){
        
        $this->w = $value;
    }
    
    public function setNetto($value){
        
        $this->n = $value;
    }

    public function setActive( $value ){

        if( $value == 1 ){
            $this->active = 'checked';
        } else {
            $this->active = '';
        }

    }

    public function setGoodsName( $value ){

        $this->goods_name = $value;
    }

}