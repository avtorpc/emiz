<?php
/**
 * Created by PhpStorm.
 * User: avtorpc
 * Date: 2020-03-23
 * Time: 00:05
 */

namespace app\models;

use app\controllers\PartnersController;
use Yii;
use yii\base\Model;


class BasketForm extends Model {

    public $id = null;
    public $name = null;
    public $surname = null;
    public $middle_name = null;
    public $index = null;
    public $city = null;
    public $address = null;
    public $home = null;
    public $corpus = null;
    public $flat = null;
    public $phone = null;
    public $email = null;
    public $transport = null;
    public $type_transport = null;
    public $card_number = null;
    public $card_date1 = null;
    public $card_date2 = null;
    public $card_numberback = null;
    public $payment_type = null;
    public $remember = null;


    public function rules()
    {
        return [
            // username and password are both required
            'trimming' => [[ 'id',
                             'name',
                             'surname',
                             'middle_name',
                             'index',
                             'city',
                             'address',
                             'home',
                             'corpus',
                             'flat',
                             'phone',
                             'email',
                            'card_number',
                            'card_date1',
                            'card_date2',
                            'card_numberback',
                            'payment_type',
                            'transport',
                            'type_transport',
                            'remember' ], 'trim']
        ];
    }


    public function getID(){

        return $this->id;
    }


    public function getName(){

        return $this->name;
    }

    public function getSurname(){

        return $this->surname;
    }

    public function getMiddleName(){

        return $this->middle_name;
    }

    public function getIndex(){

        return $this->index;
    }

    public function getCity(){

        return $this->city;
    }

    public function getAddress(){

        return $this->address;
    }

    public function getHome(){

        return $this->home;
    }

    public function getCorpus(){

        return $this->corpus;
    }

    public function getFlat(){

        return $this->flat;
    }

    public function getPhone(){

        return $this->phone;
    }

    public function getEmail(){

        return $this->email;
    }

    public function getCardNumber(){

        return $this->email;
    }

    public function getCardDate1(){

        return $this->card_date1;
    }

    public function getCardDate2(){

        return $this->card_date2;
    }

    public function getCardNumberback(){

        return$this->card_numberback;
    }

    public function getPaymentType(){

        return $this->payment_type;
    }

    public function getTransport(){

        return $this->transport;
    }

    public function getTypeTransport(){

        return $this->type_transport;
    }

    public function getRemember(){

        return $this->remember;
    }


    public function setID( $value ){

        $this->id = trim( $value );
    }

    public function setName( $value ) {

        $this->name = trim( $value );
    }

    public function setPrice( $value ) {

        $this->price = trim( $value );
    }

    public function setVolume( $value ) {

        $this->volume = trim( $value );
    }

    public function setGoodsLink( $value ) {

        $this->goods_link = trim( $value );
    }

    public function setListGoods( $value ){

        $this->list_goods = $value;
    }

    public function setUniqID( $value ){

        $this->uniq_id = $value;
    }

    public function setActive( $value ){

        if( $value == 1 ){
            $this->active = 'checked';
        } else {
            $this->active = '';
        }

    }

    public function setGoodsName( $value ){

        $this->goods_name = $value;
    }
}