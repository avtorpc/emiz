<?php


namespace app\models;


use yii\base\Model;

class SpeedBasketForm extends Model
{

    public $name = null;
    public $phone = null;
    public $check = null;


    public function rules(){
        return [
            [[ 'phone' ], 'required', 'message' => 'Поле Телефон не может быть пустым'],
            [[ 'name' ], 'required', 'message' => 'Поле Имя не может быть пустым'],
            [[ 'check' ], 'required', 'message' => 'Нужно согласие на использование ваших данных'],
            [['name', 'phone', 'check'], 'trim']
        ];
    }



    public function getName(){

        return $this->name;
    }

    public function getPhone(){

        return $this->phone;
    }

    public function getCheck(){

        return $this->check;
    }

    public function validateCheck(){

        if( $this->getCheck() == 'on' ){

            return true;
        } else {

            return false;
        }
    }

}