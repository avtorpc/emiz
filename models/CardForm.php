<?php

namespace app\models;

use app\controllers\PartnersController;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "card".
 *
 * @property int $card_id ID записи в таблице платежной карты
 * @property string|null $card_type Тип карты: VISA или MASTER
 * @property string|null $card_name Имя пользователя каты
 * @property string $card_num Номер платежной карты
 * @property string $data_mes Месяц окончания действия
 * @property string $data_god Год окончания действия
 * @property string $card_oborot Номер с оборота
 * @property int|null $active Основная карта
 *
 * @property LinkUsersCard $linkUsersCard
 * @property Users[] $users
 */
class CardForm extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'card';
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['card_num', 'data_mes', 'data_god', 'card_oborot'], 'required'],
            [['card_type'], 'string', 'max' => 6],
            [['card_oborot'], 'string', 'max' => 3],
            [['data_mes', 'data_god'], 'string', 'max' => 2],
            [['card_name', 'card_num'], 'string', 'max' => 128],
            ['card_type', 'myRule'],
            [['card_type', 'card_name', 'data_mes', 'data_god', 'card_oborot', 'card_type'], 'trim'],
            [['active', 'card_id'], 'integer'],
        ];
    }
    /**
     * Дополнительное правило для карт
     * 
     * @param type $attr
     */
    public function myRule($attr){
        if(!in_array($this->$attr, ['VISA', 'MASTER'])){
            $this->addError($attr, 'Введите VISA или MASTER');
        }
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'card_id' => 'ID записи',
            'card_type' => 'VISA или MASTER',
            'card_name' => 'Имя каты',
            'card_num' => 'Номер карты',
            'data_mes' => 'Месяц окончания действия',
            'data_god' => 'Год окончания действия',
            'card_oborot' => 'Номер с оборота',
            'active' => 'Текущая карта',
        ];
    }
    /**
     * Получить активную карту по ID пользователя
     * 
     * @param type $id
     * @return type
     */
    public function getActiveCard($id){
        return Yii::$app->db->createCommand("SELECT `c`.`card_id`,
                                                    `c`.`card_type`,
                                                    `c`.`card_name`,
                                                    `c`.`card_num`,
                                                    `c`.`data_mes`AS`card_data_mes`,
                                                    `c`.`data_god`AS`card_data_god`,
                                                    `c`.`card_oborot`,
                                                    `c`.`active`,
                                                    `u`.`id`AS`users_id`,
                                                    `u`.`nickname`AS`users_nickname`,
                                                    `ui`.`id`AS`userinfo_id`
                                                FROM `card` `c`
                                                LEFT JOIN `link_users_card` `luc` ON(`luc`.`card_id`=`c`.`card_id`)
                                                LEFT JOIN `users` `u` ON(`u`.`id`=`luc`.`users_id`)
                                                LEFT JOIN `link_users_users_info` `lui`ON(`lui`.`id_users`=`u`.`id`)
                                                LEFT JOIN `user_info` `ui`ON(`ui`.`id`=`lui`.`id_users_info`)
                                                WHERE `c`.`active`=1 AND `luc`.`users_id`=:id")->bindValues( [
                                                                                ':id' => $id
                                                                            ] )->queryOne();

    }

    /**
     * Gets query for [[LinkUsersCards]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLinkUsersCards()
    {
        return $this->hasMany(LinkUsersCard::className(), ['card_id' => 'card_id']);
    }

    /**
     * Gets query for [[Users]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(Users::className(), ['id' => 'users_id'])->viaTable('link_users_card', ['card_id' => 'card_id']);
    }

}
