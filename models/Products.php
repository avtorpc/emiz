<?php
/**
 * Created by PhpStorm.
 * User: avtorpc
 * Date: 2020-05-06
 * Time: 23:02
 */

namespace app\models;


use Yii;
use yii\base\Model;

class Products extends Model {

    private $prod = null;
    private $good = null;
    private $good_tree = null;
    private $product_tree = null;

    public function getProd( $uniq_id ){

        $this->prod = Yii::$app->db->createCommand("SELECT  PROD.id AS prod_id, 
                                                     PROD.name AS prod_name,  
                                                     PROD.price AS prod_price,
                                                     PROD.volume AS prod_volume,
                                                     PROD.uniq_id AS prod_uniq_id,
                                                     PROD.img AS prod_img,
                                                     PROD.h AS prod_h,
                                                     PROD.w AS prod_w,
                                                     PROD.n AS prod_n,
                                                     PROD.active AS prod_active,
                                                     GOOD.name AS good_name,
                                                     GOOD.uniq_id AS good_uniq_id
                                                     FROM `products` AS PROD 
                                             LEFT JOIN `link_goods_products` AS LGP ON PROD.id = LGP.id_products 
                                             LEFT JOIN `goods` AS GOOD ON LGP.id_goods = GOOD.id WHERE GOOD.uniq_id= '$uniq_id' AND GOOD.active = 1 AND PROD.active = 1  ORDER BY PROD.volume ASC" )->queryAll();

        
        
        return $this->prod;
        
    }


    public function getGood( $uniq_id ) {

        $sql = "SELECT GOOD.id AS good_id,
                       GOOD.name AS good_name,
                       GOOD.short_desc AS good_short_desc,
                       GOOD.long_desc AS good_long_desc,
                       GOOD.notes AS good_notes,
                       GOOD.uniq_id AS good_uniq_id,
                       GOOD.sostav AS good_sostav
                FROM `goods` AS GOOD WHERE GOOD.uniq_id = '$uniq_id' AND GOOD.active = 1";

        $this->good = Yii::$app->db->createCommand($sql )->queryOne();

        if( !is_null( $this->good )) {
            $this->good['good_notes'] = explode( '|', $this->good['good_notes'] );
        }

        return $this->good;
    }


    public function getProdTree(){

        $vr = Yii::$app->db->createCommand ("SELECT PROD.uniq_id AS prod_uniq_id,
                                                        PROD.price AS prod_price,
                                                        GOOD.uniq_id AS good_uniq_id
                                                FROM `products` AS PROD 
                                                LEFT JOIN `link_goods_products` AS LGP ON PROD.id = LGP.id_products 
                                                LEFT JOIN `goods` AS GOOD ON LGP.id_goods = GOOD.id WHERE  GOOD.active = 1 AND PROD.active = 1" )->queryAll();


        foreach ( $vr as $key=>$value ) {
            $this->product_tree[$value['good_uniq_id']][$value['prod_uniq_id']]= $value['prod_price'] ;
        }

        return $this->product_tree;
    }

    public function getGoodTree(){

        $vr = Yii::$app->db->createCommand ("SELECT GOOD.name AS good_name,
                                                        GOOD.uniq_id AS good_uniq_id
                                                FROM `goods` AS GOOD  WHERE  GOOD.active = 1 " )->queryAll();

        foreach( $vr as $key=>$value ){
            $this->good_tree[$value['good_uniq_id']] = $value['good_name'];
        }

        return $this->good_tree;
    }

}