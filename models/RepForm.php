<?php
namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Фильтр dealer/reports
 *
 * @author kkk
 */
class RepForm extends Model{

    //Для фильтра (default)
    public $status = 0;//все
    public $tovar = 0;//все
    public $group = 1;//ДЕНЬ
    private $group_sql = " DATE(`b`.`date_in`) ";
    private $where = " WHERE `b`.`dealer_id`=:id ";//default
    private $params = [];//default [':user_id' => $id]
    public $list_items=[];
    public $d_min = "";
    public $d_max = "";

    //Дополнительные данные таблиц
    public $buf_dmin = "";
    public $buf_dmax = "";

    /**
     * {@inheritdoc}
     */
    public function rules(){
        return [
            [['status', 'group', 'tovar'], 'integer'],
            [['d_max', 'd_min'], 'trim'],
        ];
    }
    /**
     * Иницилизация модели
     */
    public function inits($id, $basket_table ){

        //1. Фильтры
        $this->params = [':id' => $id];//default

        if($this->group){
            if($this->group == 1){
                $this->group_sql = " DATE(`b`.`date_in`) ";
            }elseif($this->group == 2){
                $this->group_sql = " CONCAT(YEAR(`b`.`date_in`),'-', LPAD(MONTH(`b`.`date_in`), 2, 0)) ";
            }elseif($this->group == 3){
                $this->group_sql = " YEAR(`b`.`date_in`) ";
            }
        }

        if($this->status){
            if($this->status == 1){//в обработке
                $this->where .= " AND (`b`.date_close IS NULL) ";
            }elseif($this->status == 2){//выполнен
                $this->where .= " AND (`b`.date_close IS NOT NULL) ";
            }
        }

        $this->tovar = intval($this->tovar);
        if($this->tovar){
            $this->where .= " AND `i`.`prod_id`=:prod_id ";
            $this->params = $this->params + [ 'prod_id' => $this->tovar ];
        }


        if($this->d_min and $this->d_max){
            $this->where .= " AND (DATE(`b`.date_in) BETWEEN :min AND :max) ";
            $this->params = $this->params + [ 'min' => \DateTime::createFromFormat('d-m-Y', strval($this->d_min))->format('Y-m-d') ];
            $this->params = $this->params + [ 'max' => \DateTime::createFromFormat('d-m-Y', strval($this->d_max))->format('Y-m-d') ];
        }elseif($this->d_min){
            $this->where .= " AND (DATE(`b`.date_in) >= :min) ";
            $this->params = $this->params + [ 'min' => \DateTime::createFromFormat('d-m-Y', strval($this->d_min))->format('Y-m-d') ];
        }elseif($this->d_max){
            $this->where .= " AND (DATE(`b`.date_in) <= :max) ";
            $this->params = $this->params + [ 'max' => \DateTime::createFromFormat('d-m-Y', strval($this->d_max))->format('Y-m-d') ];
        }



        $this->list_items=$this->listTovar($id, $basket_table);

        $this->buf_dmin = $this->d_min;
        $this->buf_dmax = $this->d_max;
        $this->getMinMaxDate();

    }
    /**
     * Пункты OPTION группиировки
     *
     * @return type
     */
    public function getOptionGroups(){
        return [
            1 => 'за день',
            2 => 'за месяц',
            3 => 'за год',
        ];
    }
    /**
     * Пункты OPTION статуса
     *
     * @return type
     */
    public function getOptionStatuses(){
        return [
            0 => '-все-',
            1 => 'в обработке',
            2 => 'выполнен',
        ];
    }
    /**
     * Получить максмальные и минимальные числа
     *
     * @return type
     */
    private function getMinMaxDate(){
        
        $dd = [];
        if($this->list_items){
            $dd = array_column($this->list_items, 'dmin');
            array_multisort($dd);
            $this->d_min = \DateTime::createFromFormat('Y-m-d', array_shift($dd))->format('d-m-Y');
        }else{
            $this->d_min = date('d-m-Y');
        }
        
        if($this->list_items){
            $dd = [];
            $dd = array_column($this->list_items, 'dmax');
            array_multisort($dd);
            $this->d_max = \DateTime::createFromFormat('Y-m-d', array_pop($dd))->format('d-m-Y');
        }else{
            $this->d_min = date('d-m-Y');
        }
        
        unset($dd);
    }
    /**
     * Пункты OPTION продуктов
     *
     * @return type
     */
    public function getOptionProduct(){
        $sql="(SELECT 0 AS`prods_id`, '-все-' AS`prods_name`, '' AS`prods_volume`)
            UNION ALL
            (SELECT `p`.`id`AS`prods_id`, 
                `p`.`name` AS `prods_name`,  
                `p`.`volume` AS `prods_volume`
            FROM `products` `p`)";

        $arr=Yii::$app->db->createCommand($sql)->queryAll();
        $tvr=[];
        foreach($arr as $k=>$v){
            $tvr[$v['prods_id']]=$v;
        }

        return $tvr;
    }
    /**
     * Список купленных товаров
     *
     * @return type
     */
    private function listTovar($id, $basket_table ){
        $sql="SELECT {$this->group_sql} AS`d_in`,
		`i`.`prod_id`,
		MAX(`i`.`name`)AS`tovar`,
		MAX(`i`.`volume`)AS`vol`,
		SUM(`i`.`unit_price` * `i`.`cnt`)AS`price`,
		SUM(`i`.`cnt`)AS cnt,
		COUNT(DISTINCT `lbi`.`id_basket`)AS`bsk`,
		MAX(DATE(`b`.`date_in`))AS`dmax`,
		MIN(DATE(`b`.`date_in`))AS`dmin`
	FROM `link_basket_basket_items` `lbi`
        RIGHT JOIN " . $basket_table . " `b` ON(`b`.`id`=`lbi`.`id_basket`)
        LEFT JOIN `basket_items` `i` ON(`i`.`id`=`lbi`.`id_basket_item`)
        {$this->where}
        GROUP BY {$this->group_sql}, `i`.`prod_id`
        ORDER BY {$this->group_sql} DESC";


        return Yii::$app->db->createCommand($sql)->bindValues($this->params)->queryAll();
    }
    /**
     * Проверяет даты на (начало <= окончание)
     *
     * @return boolean
     */
    public function checkDate(){
        if($this->d_min and $this->d_max){
            if(\DateTime::createFromFormat('d-m-Y', strval($this->d_min))->format('Y-m-d') <=
               \DateTime::createFromFormat('d-m-Y', strval($this->d_max))->format('Y-m-d')){
                return true;
               }else{
                   return false;
               }

        }else{
            return true;
        }
    }
    /**
     * Проверяет вводимую дату начала на наличие заказов.
     *
     * @return boolean
     */
    public function checkDateMin(){
        if($this->buf_dmin != ""){
            if($this->buf_dmin != $this->d_min){
                return false;
            }
        }else{
            return true;
        }
        return true;
    }
    /**
     * Проверяет вводимую дату окончания на наличие заказов.
     *
     * @return boolean
     */
    public function checkDateMax(){
        if($this->buf_dmax != ""){
            if($this->buf_dmax != $this->d_max){
                return false;
            }
        }else{
            return true;
        }
        return true;
    }
}
