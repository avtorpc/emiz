<?php

namespace app\models;

use app\controllers\PartnersController;
use Yii;
use yii\base\Model;

/**
 * Description of Codep
 *
 * @author kkk
 */
class CodepForm extends Model {
    
    public $c1 = 0;
    public $c2 = 0;
    public $c3 = 0;
    public $c4 = 0;
    public $c5 = 0;
    public $c6 = 0;
    
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['c1', 'c2', 'c3', 'c4', 'c5', 'c6'], 'string', 'max' => 1, 'message' => 'Поле должно содержать по 1 цифрре'],
        ];
    }
    /**
     * Получить склеенный код
     */
    public function getCode(){
        return intval($this->c1.$this->c2.$this->c3.$this->c4.$this->c5.$this->c6);
    }


}
