<?php

namespace app\models;

use app\controllers\PartnersController;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "user_info".
 *
 * @property int $id ID записи в таблице пользователя
 * @property string|null $name Имя пользователя
 * @property string|null $famuly Фамилия пользователя
 * @property string|null $otchestvo Отчество пользователя
 * @property string|null $birth День рождения
 * @property string|null $phone Телефон
 * @property string|null $email Электронный адрес
 * @property string $id_telegramm ID чата в телеграмм
 * @property string|null $organization
 *
 * @property LinkUsersUsersInfo[] $linkUsersUsersInfos
 */
class UserinfoForm extends ActiveRecord{
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_info';
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['birth'], 'default', 'value' => null],//2001-10-31
            [['name', 'famuly', 'otchestvo', 'organization'], 'string', 'max' => 256],
            [['phone', 'email'], 'string', 'max' => 128],
            [['phone', 'email', 'name', 'famuly', 'otchestvo', 'organization', 'id_telegramm'], 'trim'],
            [['id'], 'integer'],
            ['email', 'email'],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID записи в таблице пользователя',
            'name' => 'Имя пользователя',
            'famuly' => 'Фамилия пользователя',
            'otchestvo' => 'Отчество пользователя',
            'birth' => 'День рождения',
            'phone' => 'Телефон',
            'email' => 'Электронный адрес',
            'id_telegramm' => 'ID чата в телеграмм',
            'organization' => 'Organization',
        ];
    }
    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param string $email the target email address
     * @return bool whether the model passes validation
     */
    public function sendEmailChangePass($email, $code)
    {
        
        Yii::$app->mailer->compose()
            ->setTo($this->email)
            ->setFrom([$email => $email])
            ->setSubject("EMIZ - Запрос изменения пароля.")
            ->setTextBody("Код подтверждения: " . $code)
            ->send();

        return true;
        
    }
    /**
     * Gets query for [[LinkUsersUsersInfos]].
     *
     * @return \yii\db\ActiveQuery|LinkUsersUsersInfoQuery
     */
    public function getLinkUsersUsersInfos()
    {
        return $this->hasMany(LinkUsersUsersInfo::className(), ['id_users_info' => 'id']);
    }
    /**
     * Изменить личные данные со страницы admin/profil
     * 
     * @return string
     * @throws \Exception
     */
    public function updPersonal($id){
        
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try{
            
            $rez = Yii::$app->db->createCommand("UPDATE user_info `i`
                LEFT JOIN link_users_users_info `lui` ON(`lui`.id_users_info = `i`.id)
                SET 
                  `i`.name = :name,
                  `i`.famuly = :famuly,
                  `i`.otchestvo = :otchestvo,
                  `i`.birth = :birth,
                  `i`.phone = :phone,
                  `i`.email = :email,
                  `i`.id_telegramm = :id_telegramm
                WHERE `lui`.id_users = :user_id")->bindValues([
                    ':user_id' => (int)intval($id),
                    ':name' => $this->name,
                    ':famuly' => $this->famuly,
                    ':otchestvo' => $this->otchestvo,
                    ':birth' => \DateTime::createFromFormat('d-m-Y', strval($this->birth))->format('Y-m-d'),
                    ':phone' => $this->phone,
                    ':email' => $this->email,
                    ':id_telegramm' => $this->id_telegramm,
                ])->execute();

            $transaction->commit();
            Yii::$app->session->setFlash('msg', "Личные данные успешно изменены");
        }catch(\Exception $e) {
            Yii::$app->session->setFlash('msg', "Системная ошибка! Не сохранено! Обратитесть к администратору.");
            $transaction->rollBack();
            Yii::$app->session->setFlash('personal', 0);
        }
        
    }
}
