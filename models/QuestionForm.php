<?php
/**
 * Created by PhpStorm.
 * User: avtorpc
 * Date: 2020-04-08
 * Time: 18:38
 */

namespace app\models;

use Yii;
use yii\base\Model;

class QuestionForm extends Model {

    public $name = null;
    public $phone = null;
    public $contact = null;
    public $question = null;
    public $check = null;


    public function rules()
    {
        return [
            [[ 'question' ], 'required', 'when' => function( $model ) {
                return $model->getControllerName();
            } ,'message' => Yii::t('app/partners', '_TEXT_22_')],
            [[ 'phone' ], 'required', 'message' => Yii::t('app/partners', '_TEXT_23_')],
         //   [[ 'contact' ], 'required', 'message' => 'Поле Контакт не может быть пустым'],
            [[ 'name' ], 'required', 'message' => Yii::t('app/partners', '_TEXT_24_')],
            [[ 'check' ], 'required', 'message' => Yii::t('app/partners', '_TEXT_25_')],
            [[ 'name', 'contact', 'question', 'check', 'phone' ], 'trim']
        ];
    }

    public function getName(){

        return $this->name;
    }

    public function getPhone(){

        return $this->phone;
    }
    
    public function getContact(){

        return $this->contact;
    }

    public function getQuestion(){

        return $this->question;
    }

    public function getCheck(){

        return $this->check;
    }

    public function validateCheck(){

        if( $this->getCheck() == 'on' ){

            return true;
        } else {

            return false;
        }
    }

    public function getControllerName(){
        if( Yii::$app->controller->id != 'contact') {

            return true;
        } else {

            return false;
        }

    }
}