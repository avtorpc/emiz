<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Filter admin/orders
 *
 * @author kkk
 */
class ShowBasketItemForm extends Model{
    
    //Для фильтра (default)
    public $sort = 1;
    public $sort_sql = "`b`.`date_in` DESC";
    public $city="-все-";
    public $type="-все-";
    public $where = "";
    public $params = [];
    
    //Дополнительные данные таблиц
    public $page = 1;//1 - dealer, 2 - client //for JS, Какой список сделать при открытии страниц
    public $excel = 0;
    public $summ_d = 0;
    public $summ_c = 0;
    public $item_d = 0;
    public $item_c = 0;
    public $basket_arr = [];
    public $item_arr = [];
    public $menu_city=[];
    public $option_paytype=[];
    public $path_csv="tmp_csv";
    public $name_csv="";
    
    
    /**
     * {@inheritdoc}
     */
    public function rules(){
        return [
            [['sort', 'page', 'excel'], 'integer', 'message' => 'Не верное условие сортировки'],
            [['city', 'type'], 'string', 'message' => 'Не верное условие сортировки'],
            [['city'], 'trim'],
        ];
    }
    /**
     * Иницилизация
     */
    public function getBasketItem($id){
        
        //сортировка
        if($this->sort == 1){$this->sort_sql = "`b`.`date_in` DESC";}
        elseif($this->sort == 2){$this->sort_sql = "`b`.`date_in` ASC";}
        elseif($this->sort == 3){$this->sort_sql = "`summ_basket` DESC";}
        elseif($this->sort == 4){$this->sort_sql = "`summ_basket` ASC";}
        
        //1. Фильтр "город" и "тип оплаты"
        $pp = [];$pp[] = [ ':dealer_id' => $id ];
        $ww = [];$ww[] = " `b`.`dealer_id`=:dealer_id ";
        if($this->city != '-все-'){
            $ww[] = " `b`.`city`=:city ";
            $pp[] = [ ':city' => $this->city ];
        }
        
        if($this->type != '-все-'){
            $ww[] = " `b`.`pay_type`=:type ";
            
            foreach($this->getOptionPaytype($id) as $k=>$v){
                if($v['paytype']==$this->type){
                    $pp[] = [ ':type' => $v['pay_type'] ];
                    break;
                }
            }
            
            
        }
        
        
        if(count($ww) == 1){
            $this->where=" WHERE ".$ww[0]." ";
            $this->params = $pp[0];
        }elseif(count($ww) > 1){
            $this->where=" WHERE ";
            foreach($ww as $k=>$v){
                $this->where .= " {$v} AND ";
                $this->params = $this->params + $pp[$k];
            }
            
            $this->where = mb_substr($this->where, 0, -4);

        }else{
            $this->where="";
        }
        
        

        
        
        $this->baskets();
        $this->items($id);
        $this->menu_city = $this->getOptionCity($id);
        $this->option_paytype = $this->getOptionPaytype($id);
        
        foreach($this->basket_arr as $k=>$v){
            if(is_null($v['basket_user_id'])){
                $this->summ_c+=($v['summ_basket'] + $v['basket_transport']);
                $this->item_c++;
            }else{
                $this->summ_d += ($v['summ_basket'] + $v['basket_transport']);
                $this->item_d++;
            }
        }
        
        //EXCEL
        //Сброс $this->excel=0 производиться в представлении (НЕ ЗДЕСЬ!!!)
        //Чек производиться в контроллере
        if($this->excel){
            $this->getFileNameCsv();
            $this->getExcel($id);
        }
        
    }
    /**
     * Получить массив списка корзин
     * 
     * @return type
     */   
    private function baskets(){
        
        $sql="SELECT `b`.`id`AS`basket_id`,
            `b`.`name`AS`basket_name`,
            `b`.`surname`AS`basket_surname`,
            `b`.`middle_name`AS`basket_middle_name`,
            `b`.`index`AS`basket_index`,
            `b`.`city`AS`basket_city`,
            `b`.`address`AS`basket_address`,
            `b`.`home`AS`basket_home`,
            `b`.`corpus`AS`basket_corpus`,
            `b`.`flat`AS`basket_flat`,
            `b`.`phone`AS`basket_phone`,
            `b`.`email`AS`basket_email`,
            `b`.`transport`AS`basket_transport`,
            `b`.`pay_type`AS`basket_pay_type`,
            `b`.`card_number`AS`basket_card_number`,
            `b`.`card_date1`AS`basket_card_date1`,
            `b`.`card_date2`AS`basket_card_date2`,
            `b`.`card_numberback`AS`basket_card_numberback`,
            `b`.`date_in`AS`basket_date_in`,
            `b`.`date_close`AS`basket_date_close`,
            `b`.`user_id`AS`basket_user_id`,
            `b`.`remember`AS`basket_remember`,
            `u`.`nickname`AS`users_nickname`,
            `i`.`id`AS`uinfo_id`,
            (
                SELECT SUM((`ii`.`unit_price` * `ii`.`cnt`))
                FROM link_basket_basket_items `bb`
                LEFT JOIN `basket_items` `ii` ON(`ii`.`id`=`bb`.`id_basket_item`)
                WHERE `bb`.`id_basket`=`b`.`id`
            )
            AS`summ_basket`
        FROM `basket` `b`
        LEFT JOIN `users` `u` ON(`b`.`user_id`=`u`.`id`)
        LEFT JOIN `link_users_users_info` `lui`ON(`lui`.`id_users`=`u`.`id`)
        LEFT JOIN `user_info` `i`ON(`i`.`id`=`lui`.`id_users_info`)
        {$this->where}
        ORDER BY {$this->sort_sql}";
        
        if($this->where == ""){
            $this->basket_arr = Yii::$app->db->createCommand($sql)->queryAll();
        }else{
            $this->basket_arr = Yii::$app->db->createCommand($sql)->bindValues($this->params)->queryAll();
        }
    }
    /**
     * Получить массив списка товаров карзины
     * 
     * @return type
     */
    private function items($id){
        $sql="SELECT `b`.`id`AS`basket_id`,
                `b`.`name`AS`basket_name`,
                `b`.`surname`AS`basket_surname`,
                `b`.`middle_name`AS`basket_middle_name`,
                `b`.`user_id`AS`basket_user_id`,
                `u`.`nickname`AS`users_nickname`,
                `b`.`phone`AS`basket_phone`,
                `b`.`date_in`AS`basket_date_in`,
                `i`.`name`AS`basketitems_name`,
                `i`.`volume`AS`basketitems_volume`,
                `i`.`unit_price`AS`basketitems_unitprice`,
                `i`.`cnt`AS`basketitems_cnt`,
                `i`.`uniq_id`AS`basketitems_uniq_id`,
                `p`.`id`AS`prod_id`,
                `g`.`id`AS`good_id`,
                `g`.`name`AS`good_name`,
                `g`.`uniq_id`AS`good_uniq_id`
            FROM `link_basket_basket_items` `lbi`
            LEFT JOIN `basket` `b` ON(`b`.`id`=`lbi`.`id_basket`)
            LEFT JOIN `users` `u` ON(`b`.`user_id`=`u`.`id`)
            LEFT JOIN `basket_items` `i` ON(`i`.`id`=`lbi`.`id_basket_item`)
            LEFT JOIN `products` `p` ON(`p`.`uniq_id`=`i`.`uniq_id`)
            LEFT JOIN `link_goods_products` `gp` ON(`gp`.`id_products`=`p`.`id`)
            LEFT JOIN `goods` `g` ON(`g`.`id`=`gp`.`id_goods`)
            WHERE `b`.`dealer_id`=:dealer_id";
        
        $this->item_arr = Yii::$app->db->createCommand($sql)->bindValues([ ':dealer_id' => $id ])->queryAll();
    }
    /**
     * Пункты OPTION сортировки
     * 
     * @return type
     */
    public function getOptionSort(){//getMenuSort
        return [
            1 => 'по дате &#9650;',
            2 => 'по дате &#9660;',
            3 => 'по сумме &#9650;',
            4 => 'по сумме &#9660;',
        ];
    }
    /**
     * Пункты OPTION городов
     * 
     * <b>На выходе массив масивов (результат метода queryAll() )</b>
     * <ul>
     *  <li>[row] => 65</li>
     *  <li>[city] => Москва</li>
     *  <li>[myord] => 2</li>
     *  <li>[user] => D</li>
     * </ul>
     * 
     * @return type
     */
    public function getOptionCity($id){
        
        $sql="(SELECT COUNT(*)AS`row`, '-все-' AS `city`, 1 AS myord, 'D' AS `user` FROM basket `b` WHERE `b`.`dealer_id`=:dealer_id AND `b`.user_id IS NOT NULL)
        UNION ALL
        (SELECT COUNT(*)AS`row`, `b`.city, 2 AS myord, 'D' AS `user` FROM basket `b` WHERE `b`.`dealer_id`=:dealer_id AND `b`.user_id IS NOT NULL GROUP BY `b`.city)
        UNION ALL
        (SELECT COUNT(*)AS`row`, '-все-' AS `city`, 3 AS myord, 'C' AS `user` FROM basket `b` WHERE `b`.`dealer_id`=:dealer_id AND `b`.user_id IS NULL)
        UNION ALL
        (SELECT COUNT(*)AS`row`, `b`.city, 4 AS myord, 'C' AS `user` FROM basket `b` WHERE `b`.`dealer_id`=:dealer_id AND `b`.user_id IS NULL GROUP BY `b`.city)
        ORDER BY myord, city";
        
        return Yii::$app->db->createCommand($sql)->bindValues([ ':dealer_id' => $id ])->queryAll();

    }
    /**
     * Пункты OPTION тип оплаты
     * 
     * <b>На выходе массив масивов (результат метода queryAll() )</b>
     * <ul>
     *  <li>[row] => 3</li>
     *  <li>[pay_type] => card-pay</li>
     *  <li>[myord] => 2</li>
     *  <li>[utype] => D</li>
     *  <li>[paytype] => на сайте</li>
     * </ul>
     * @return type
     */
    public function getOptionPaytype($id){
        
        
        $sql="(SELECT COUNT(*)AS`row`, NULL AS`pay_type`, 1 AS myord, 'D' AS `utype`, '-все-' AS `paytype` FROM basket `b` WHERE `b`.`dealer_id`=:dealer_id AND `b`.user_id IS NOT NULL)
            UNION ALL
        (SELECT COUNT(*)AS`row`, b.`pay_type`, 2 AS myord, 'D' AS `utype`,
        IF(MAX(o.paytype) IS NULL, MAX(b.pay_type), MAX(o.paytype))AS`paytype`
        FROM basket b
        LEFT JOIN pay_type o ON(o.pay_type=b.pay_type)
        WHERE `b`.`dealer_id`=:dealer_id AND `b`.user_id IS NOT NULL
        GROUP BY b.pay_type)
            UNION ALL
        (SELECT COUNT(*)AS`row`, NULL AS`pay_type`, 3 AS myord, 'C' AS `utype`, '-все-' AS `paytype` FROM basket `b` WHERE `b`.`dealer_id`=:dealer_id AND `b`.user_id IS NULL)
            UNION ALL
        (SELECT COUNT(*)AS`row`, b.`pay_type`, 4 AS myord, 'C' AS `utype`,
        IF(MAX(o.paytype) IS NULL, MAX(b.pay_type), MAX(o.paytype))AS`paytype`
        FROM basket b
        LEFT JOIN pay_type o ON(o.pay_type=b.pay_type)
        WHERE `b`.`dealer_id`=:dealer_id AND `b`.user_id IS NULL
        GROUP BY b.pay_type)";
        
        return Yii::$app->db->createCommand($sql)->bindValues([ ':dealer_id' => $id ])->queryAll();

    }
    /**
     * Групировка по заказам
     * 
     * <b>На выходе массив масивов (результат метода queryAll() )</b>
     * <ul>
     *  <li>[city] => Москва</li>
     *  <li>[pay_type] => на сайте</li>
     *  <li>[tvr] => 1000</li>
     *  <li>[bsk] => 100</li>
     *  <li>[summ] => 1000000</li>
     * </ul>
     * 
     * @return type
     */
    public function getGroupOrders(){
        $sql="SELECT `b`.city, IF(MAX(o.paytype) IS NULL, MAX(b.pay_type), MAX(o.paytype))AS`pay_type`, COUNT(DISTINCT `lbi`.id_basket)AS`bsk`, SUM(`i`.cnt)AS`tvr`, 
        (SUM((`i`.unit_price * `i`.cnt))+ SUM(DISTINCT `b`.transport) )AS`summ`
        FROM link_basket_basket_items `lbi`
        LEFT JOIN basket `b` ON(`b`.id=`lbi`.id_basket)
        LEFT JOIN basket_items `i` ON(`i`.id=`lbi`.id_basket_item)
        LEFT JOIN pay_type o ON(`o`.pay_type=`b`.pay_type)
        GROUP BY `b`.city, `b`.pay_type
        ORDER BY `b`.city, `b`.pay_type";
        return Yii::$app->db->createCommand($sql)->queryAll();
    }
    /**
     * Экспорт EXCEL
     */
    private function getExcel($id){
        
        $fp = fopen("../{$this->path_csv}/user_id_{$id}.csv", 'w');
        fputs($fp, chr(0xEF) . chr(0xBB) . chr(0xBF));// BOM
        foreach($this->basket_arr as $k=>$v){
            
            if($this->page == 1){//dealer
                if($v['basket_user_id']){
                    fputcsv($fp, $v, ';');
                }
            }elseif($this->page == 2){//client
                if(!$v['basket_user_id']){
                    fputcsv($fp, $v, ';');
                }
            }            
        }
        fclose($fp);
    }
    /**
     * Уникальное имя файла для EXCEL
     */
    private function getFileNameCsv(){
        if($this->page == 1){
            $this->name_csv = "emiz_dealers_".date("Ymd_His");
        }else{
            $this->name_csv = "emiz_clients_".date("Ymd_His");
        }
    }
}