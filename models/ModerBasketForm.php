<?php
/**
 * Created by PhpStorm.
 * User: avtorpc
 * Date: 2020-04-10
 * Time: 20:01
 */

namespace app\models;

use Yii;
use yii\base\Model;

class ModerBasketForm extends Model {

    public $basket_id = null;
    public $basket_name = null;
    public $basket_surname = null;
    public $basket_middle_name = null;
    public $basket_index = null;
    public $basket_city = null;
    public $basket_address = null;
    public $basket_home = null;
    public $basket_corpus = null;
    public $basket_flat = null;
    public $basket_phone = null;
    public $basket_email = null;
    public $basket_transport = null;
    public $basket_type_transport = null;
    public $basket_sum_transport = null;
    public $basket_pay_type = null;
    public $basket_card_number = null;
    public $basket_card_date1 = null;
    public $basket_card_date2 = null;
    public $basket_card_numberback = null;
    public $basket_date_in = null;
    public $basket_date_close = null;
    public $basket_user_id = null;
    public $basket_remember = null;
    public $basket_active = null;
    public $list_items = null;
    public $final_sum = null;
    public $final_cnt= null;
    public $goods_link = null;

    public function rules()
    {
        return [
            // username and password are both required
            [['active'], 'required', 'message' => 'Поле не может быть пустым'],
            [[  'basket_id',
                'basket_name',
                'basket_surname',
                'basket_middle_name',
                'basket_index',
                'basket_city',
                'basket_address',
                'basket_home',
                'basket_corpus',
                'basket_flat',
                'basket_phone',
                'basket_email',
                'basket_transport',
                'basket_type_transport',
                'basket_sum_transport',
                'basket_pay_type',
                'basket_card_number',
                'basket_card_date1',
                'basket_card_date2',
                'basket_card_numberback',
                'basket_date_in',
                'basket_date_close',
                'basket_user_id',
                'basket_remember',
                'goods_link',
                'basket_active'], 'trim']
        ];
    }

    public function getID(){

        return $this->basket_id;
    }

    public function getName(){

        return $this->basket_name;
    }

    public function getSurname(){

        return $this->basket_surname;
    }

    public function getMiddleName(){

        return $this->basket_middle_name;
    }

    public function getIndex(){

        return $this->basket_index;
    }

    public function getCity(){

        return $this->basket_city;
    }

    public function getAddress(){

        return $this->basket_address;
    }

    public function getHome(){

        return $this->basket_home;
    }

    public function getCorpus(){

        return $this->basket_corpus;
    }

    public function getFlat(){

        return $this->basket_flat;
    }

    public function getPhone(){

        return $this->basket_phone;
    }

    public function getEmail(){

        return $this->basket_email;
    }

    public function getTransport(){

        return $this->basket_transport;
    }

    public function getTypeTransport(){

        return $this->basket_type_transport;
    }

    public function getSumTransport(){

        return $this->basket_sum_transport;
    }

    public function getPayType(){

        return $this->basket_pay_type;
    }

    public function getCardNumber(){

        return $this->basket_card_number;
    }

    public function getCardDate1(){

        return $this->basket_card_date1;
    }

    public function getCardDate2(){

        return $this->basket_card_date2;
    }

    public function getCardNumberback(){

        return $this->basket_card_numberback;
    }

    public function getDateIn(){

        return $this->basket_date_in;
    }

    public function getDateClose(){

        return $this->basket_date_close;
    }

    public function getUserId(){

        return $this->basket_user_id;
    }

    public function getRemember(){

        return $this->basket_remember;
    }

    public function getActive(){

        if( $this->basket_active == 'checked' OR $this->basket_active == 'on') {

            return 1;
        } else {

            return 0;
        }
    }

    public function getActiveForForm(){

        if( $this->basket_active == 1 OR $this->basket_active == 'checked' OR $this->basket_active == 'on' ) {

            return 'checked';
        } else {

            return '';
        }
    }

    public function getTransportForForm(){
        if( (int) $this->getTransport() == '' OR is_null( $this->getTransport() ) ) {

            return '';
        } else {

            return 'checked';
        }
    }

    public function getPayTypeForForm(){
        if( $this->getPayType() =='' OR is_null( $this->getPayType() ) ) {

            return '';
        } else {

            return 'checked';
        }
    }

    public function getFullName(){

        $s = '';
        if( $this->getName() != '' AND !is_null( $this->getName() ) ){
            $s = $this->getName();
        }
        if( $this->getMiddleName() != '' AND !is_null( $this->getMiddleName() ) ){
            $s = $s . ' ' . $this->getMiddleName();
        }
        if( $this->getSurname() != '' AND !is_null( $this->getSurname() ) ){
            $s = $s . ' ' . $this->getSurname();
        }

        return $s;
    }

    public function getGoodsLink (){

        return $this->goods_link;
    }


    public function setDATA( $arr ){
        $vars = get_class_vars( get_class( $this ) );
        foreach( $arr as $key=>$value ){
            if( array_key_exists( $key, $vars )) {

                    $this->{$key} = $value ;
            }
        }
    }


    public function setActive( $value ){

        if( $value == 1 ){
            $this->basket_active = 'checked';
        } else {
            $this->basket_active = '';
        }

    }

    public function validatePayType( ){
        if ( $this->basket_pay_type == 'on' ){

            return 1;
        } else {

            return 0;
        }
    }

    public function validateTypeTransport(){
        if ( $this->basket_transport == 'on' ){

            return 1;
        } else {

            return 0;
        }
    }

    public function saveBasket( $id, $item_name, $item_count, $item_price ){
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {

            $params = [
                ':name' => $this->getName(),
                ':surname' => $this->getSurname(),
                ':middle_name' => $this->getMiddleName(),
                ':index' => $this->getIndex(),
                ':city' => $this->getCity(),
                ':address' => $this->getAddress(),
                ':home' => $this->getHome(),
                ':corpus' => $this->getCorpus(),
                ':flat' => $this->getFlat(),
                ':phone' => $this->getPhone(),
                ':email' => $this->getEmail(),
                ':transport' => (($this->validateTypeTransport()) ? $this->getSumTransport() : '0.00'),
                ':type_transport' => (($this->validateTypeTransport()) ? $this->getTypeTransport() : null ),
                ':pay_type' => $this->getPayType(),
                ':card_number' => $this->getCardNumber(),
                ':card_date1' => $this->getCardDate1(),
                ':card_date2' => $this->getCardDate2(),
                ':card_numberback' => $this->getCardNumberback(),
                ':date_in' => $this->getDateIn(),
                ':date_close' => ( ( $this->getDateClose() == '' )? null: $this->getDateClose() ) ,
                ':active' => $this->getActive(),
                ':id'=> (int)$id
            ];

            $rez = Yii::$app->db->createCommand(' UPDATE `basket` SET `name`=:name,
                                                                          `surname`=:surname,
                                                                          `middle_name`=:middle_name,
                                                                          `index`=:index,
                                                                          `city`=:city,
                                                                          `address`=:address,
                                                                          `home`=:home,
                                                                          `corpus`=:corpus,
                                                                          `flat`=:flat,
                                                                          `phone`=:phone,
                                                                          `email`=:email,
                                                                          `transport`=:transport,
                                                                          `type_transport`=:type_transport,
                                                                          `pay_type`=:pay_type,
                                                                          `card_number`=:card_number,
                                                                          `card_date1`=:card_date1,
                                                                          `card_date2`=:card_date2,
                                                                          `card_numberback`=:card_numberback,
                                                                          `date_in`=:date_in,
                                                                          `date_close`=:date_close,
                                                                          `active`=:active WHERE id=:id')->bindValues($params)->execute();

            $rez = Yii::$app->db->createCommand( 'DELETE FROM `basket_items` 
                                                      WHERE id IN ( SELECT id_basket_item 
                                                                    FROM `link_basket_basket_items` AS LBBI WHERE LBBI.id_basket=:id  
                                                                    ) 
                                                ' )->bindValues( [':id'=>$id ] )->execute();

            $select_prod = $db->createCommand('SELECT * FROM `products` WHERE id=:id' );
            $insert_item_to_basket = $db->createCommand('INSERT INTO `basket_items`( `name`, `cnt`, `unit_price`, `volume`, `prod_id`, `uniq_id`) VALUES ( :name, :cnt, :unit_price, :volume, :prod_id, :uniq_id)' );
            $insert_to_link_table = $db->createCommand('INSERT INTO `link_basket_basket_items`( `id_basket`, `id_basket_item`) VALUES ( :id_basket, :id_basket_item )' );

            foreach( $item_name as $key=> $value ) {
                $rez =  $select_prod->bindValues([':id' => $value] )->queryOne();

                $insert_item_to_basket ->bindValues( [
                        ':name' => $rez['name'],
                        ':cnt' => (int) $item_count[$key],
                        ':unit_price' => $item_price[$key],
                        ':volume' => $rez['volume'],
                        ':prod_id' => $rez['id'],
                        ':uniq_id' => $rez['uniq_id']
                    ] )->execute();
                $rez = $db->createCommand('INSERT INTO `link_basket_basket_items`( `id_basket`, `id_basket_item`) VALUES ( :id_basket, :id_basket_item )' )
                    ->bindValues( [
                        ':id_basket' => $id,
                        ':id_basket_item' => $db->getLastInsertID()
                    ] )->execute();
            }

            $transaction->commit();

        } catch(\Exception $e) {
            $transaction->rollBack();
            //echo "<pre>"; print_r(  $e->getMessage() ); die;
            throw $e;
        }
    }



    public function getBasket( $id, $basket_table){
        $params = [
            'id' => $id
        ];
        $rez = Yii::$app->db->createCommand('
                      SELECT 
                       BSK.id AS basket_id, 
                       BSK.name AS basket_name, 
                       BSK.surname AS basket_surname, 
                       BSK.middle_name AS basket_middle_name, 
                       BSK.index AS basket_index, 
                       BSK.city AS basket_city, 
                       BSK.address AS basket_address, 
                       BSK.home AS basket_home, 
                       BSK.corpus AS basket_corpus, 
                       BSK.flat AS basket_flat, 
                       BSK.phone AS basket_phone, 
                       BSK.email AS basket_email, 
                       BSK.transport AS basket_transport, 
                       BSK.type_transport AS basket_type_transport,
                       BSK.pay_type AS basket_pay_type, 
                       BSK.card_number AS basket_card_number, 
                       BSK.card_date1 AS basket_card_date1, 
                       BSK.card_date2 AS basket_card_date2, 
                       BSK.card_numberback AS basket_card_numberback, 
                       BSK.date_in AS basket_date_in, 
                       BSK.date_close AS basket_date_close, 
                       BSK.user_id AS basket_user_id, 
                       BSK.remember AS basket_remember,
                       BSK.active AS basket_active,
                       BSI.id AS item_id,
                       BSI.name AS item_name, 
                       BSI.cnt AS item_cnt,  
                       BSI.unit_price AS item_unit_price, 
                       BSI.volume AS item_volume, 
                       BSI.uniq_id AS item_uniq_id,
                       BSI.prod_id AS item_prod_id,
                       GDS.id AS item_good_id,
                       GDS.uniq_id AS item_good_uniq_id
                       FROM ' . $basket_table . ' AS BSK
                      LEFT JOIN link_basket_basket_items AS LBI ON LBI.id_basket = BSK.id
                      LEFT JOIN basket_items AS BSI ON BSI.id = LBI.id_basket_item
                      LEFT JOIN link_goods_products AS LGP ON  LGP.id_products = BSI.prod_id
                      LEFT JOIN goods AS GDS ON GDS.id = LGP.id_goods
                      WHERE BSK.id=:id' )->bindValues( $params )->queryAll();

        if( count( $rez ) == 0 ) {

            return false;
        } else {

            return $rez;
        }

    }

}