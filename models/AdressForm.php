<?php

namespace app\models;

use Yii;
use app\controllers\PartnersController;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "adress".
 *
 * @property int $adress_id ID записи адеса
 * @property string|null $region
 * @property string $city Город
 * @property string $street Улица
 * @property string $house Дом
 * @property string $korpus Корпус
 * @property string $apartament Квартира
 * @property string $pochta_index Почтовый индекс
 * @property int|null $active Основной адрес
 *
 * @property LinkUsersAdress $linkUsersAdress
 * @property Users[] $users
 * @property LinkUsersAdrkont $linkUsersAdrkont
 * @property Users[] $users0
 */
class AdressForm extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'adress';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['city', 'street', 'house', 'apartament', 'pochta_index'], 'required'],
            [['active', 'adress_id'], 'integer'],
            [['region'], 'string', 'max' => 128],
            [['city', 'street'], 'string', 'max' => 256],
            [['house', 'korpus', 'apartament'], 'string', 'max' => 50],
            [['pochta_index'], 'string', 'max' => 10],
            [['city', 'street', 'house', 'korpus', 'apartament', 'pochta_index'], 'trim'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'adress_id' => 'ID записи адеса',
            'region' => 'Region',
            'city' => 'Город',
            'street' => 'Улица',
            'house' => 'Дом',
            'korpus' => 'Корпус',
            'apartament' => 'Квартира',
            'pochta_index' => 'Почтовый индекс',
            'active' => 'Основной адрес',
        ];
    }

    /**
     * Gets query for [[LinkUsersAdresses]].
     *
     * @return \yii\db\ActiveQuery|LinkUsersAdressQuery
     */
    public function getLinkUsersAdresses()
    {
        return $this->hasMany(LinkUsersAdress::className(), ['adress_id' => 'adress_id']);
    }

    /**
     * Gets query for [[Users]].
     *
     * @return \yii\db\ActiveQuery|UsersQuery
     */
    public function getUsers()
    {
        return $this->hasMany(Users::className(), ['id' => 'users_id'])->viaTable('link_users_adress', ['adress_id' => 'adress_id']);
    }

}
