<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $path;
    public $rememberMe = true;

    private $_user = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            'trimming' => [['username', 'password', 'path' ], 'trim']
        ];
    }

    public function getLogin(){

        return trim( $this->username );
    }


    public function getPassword(){

        return trim( $this->password );
    }

    public function getPath(){

        return $this->path;
    }

}
