<?php
namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Description of SortByDateForm
 *
 * @author kkk
 */
class SortByDealer extends Model{
    
    public $sort = 4;
    
    /**
     * {@inheritdoc}
     */
    public function rules(){
        return [
            ['sort', 'integer', 'message' => 'Не верное условие сортировки'],
        ];
    }
    
    public function getMenuSort(){
        return [
            1 => 'по сумме',
            2 => 'по кол-ву товара',
            3 => 'по кол-ву заказов',
            4 => 'по дате регистрации',
        ];
    }

}
