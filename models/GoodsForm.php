<?php
/**
 * Created by PhpStorm.
 * User: avtorpc
 * Date: 2020-03-23
 * Time: 00:05
 */

namespace app\models;

use app\controllers\PartnersController;
use Yii;
use yii\base\Model;


class GoodsForm extends Model {

    public $id = null;
    public $name = null;
    public $short_desc = null;
    public $long_desc = null;
    public $notes = null;
    public $sostav = null;
    public $uniq_id = null;
    public $active = null;

    public function rules()
    {
        return [
            // username and password are both required
            [[ 'name', 'short_desc', 'long_desc', 'notes', 'sostav', 'uniq_id' ], 'required', 'message' => 'Поле не может быть пустым'],
            'trimming' => [[ 'id', 'name', 'short_desc', 'long_desc', 'notes', 'sostav', 'uniq_id', 'active' ], 'trim']
        ];
    }


    public function getID(){

        return $this->id;
    }


    public function getName(){

        return $this->name;
    }

    public function getShortDesc(){

        return $this->short_desc;
    }

    public function getLongDesc(){

        return $this->long_desc;
    }

    public function getNotes(){

        return $this->notes;
    }

    public function getSostav(){

        return $this->sostav;
    }

    public function getActive(){

        if( $this->active == 'checked' OR $this->active == 'on') {

            return 1;
        } else {

            return 0;
        }
    }

    public function getActiveForForm(){

        if( $this->active == 1 OR $this->active == 'checked' OR $this->active == 'on' ) {

            return 'checked';
        } else {

            return '';
        }
    }

    public function getUniqID(){

        return $this->uniq_id;
    }




    public function setID( $value ){

        $this->id = trim( $value );
    }

    public function setName( $value ) {

        $this->name = trim( $value );
    }

    public function setShortDesc( $value ) {

        $this->short_desc = trim( $value );
    }

    public function setLongDesc( $value ) {

        $this->long_desc = trim( $value );
    }

    public function setNotes( $value ) {

        $this->notes = trim( $value );
    }

    public function setSostav( $value ){

        $this->sostav = trim( $value );
    }

    public function setUniqID( $value ){

        $this->uniq_id = $value;
    }

    public function setActive( $value ){

        if( $value == 1 ){
            $this->active = 'checked';
        } else {
            $this->active = '';
        }

    }
}