<?php

namespace app\models;

use app\controllers\PartnersController;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "hashs".
 *
 * @property int $hashs_id ID записи в таблице хешей
 * @property string $hashs Сгенерированный хеш
 * @property int $users_id ID пользователя
 * @property string $date_out Дата окончания (2 часа)
 */
class Hashs extends ActiveRecord{
    
    public $textBody="";
    public $subject="";
    public $user_email="";
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hashs';
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hashs', 'users_id', 'date_out'], 'required'],
            [['users_id'], 'integer'],
            [['date_out'], 'safe'],
            [['hashs'], 'string', 'max' => 256],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'hashs_id' => 'ID записи в таблице хешей',
            'hashs' => 'Сгенерированный хеш',
            'users_id' => 'ID пользователя',
            'date_out' => 'Дата окончания (2 часа)',
        ];
    }
    /**
     * Иницилизация для добавления ID чата в TELEGRAM
     * 
     * @param type $id
     */
    public function initTelegram($id){
        //Подготовка
        $this->hashs=rand(1000, 9999);
        $this->users_id=intval($id);
        $this->clearOutHash();
        $this->clearHashsByUserId();
        $this->insHash();
        
        //Отослать письмо с кодом
        $this->subject="EMIZ - Запрос получения ID telegram.";
        $this->textBody="Отправте в чат код: " . $this->hashs;
        $this->user_email=$this->getUserEmail($id);
        $this->sendEmail();
        Yii::$app->session->setFlash('msg', "На почту {$this->user_email} было отправлено письмо с кодом подтверждения");
        Yii::$app->session->setFlash('personal', 0);

    }
    
    public function addIdTelegram($id){
        //Подготовка
        Yii::$app->telegram->botUsername= Yii::$app->params['telegram_bot'];
        $this->hashs = $this->getHashByUser($id);
        $this->users_id = intval($id);
        
        //telegram
        $url=Yii::$app->telegram->apiUrl.Yii::$app->telegram->botToken.'/getUpdates';//echo $url."<hr>";die;
        $response = json_decode(file_get_contents($url), JSON_OBJECT_AS_ARRAY);
        
        if($response["ok"]){
            foreach($response["result"] as $update){
                //echo $update["message"]["from"]["id"]." - ".$update["message"]["text"]."<br>";
                if($update["message"]["text"] == $this->hashs){
                    $this->updHashsTelegram($update["message"]["from"]["id"]);
                    Yii::$app->telegram->sendMessage([
                        'chat_id' => $update["message"]["from"]["id"],
                        'text' => 'Вы зарегестрированы!',
                    ]);
                    $this->clearHashsByUserId();
                    break;
                }
            }
        }else{
            Yii::$app->session->setFlash('msg', 'Системная ошибка! Нет ответа от telegram.');
        }
        
        
        //294349100
        /*
        $lastupdate=280820121;
        $params=['offset'=>280820121+1];
        $url=$url.'?'.http_build_query($params);
        

        $chat_id=463836112;
        $url2=Yii::$app->telegram->apiUrl.Yii::$app->telegram->botToken.'/sendMessage';
        $params=['chat_id'=>$chat_id, 'text'=>'I em bot'];
        $url2=$url2.'?'.http_build_query($params);
        echo $url2."<hr>";

        $chat_id=456353715;
        $url2=Yii::$app->telegram->apiUrl.Yii::$app->telegram->botToken.'/sendMessage';
        $params=['chat_id'=>$chat_id, 'text'=>'I em bot'];
        $url2=$url2.'?'.http_build_query($params);
        echo $url2."<hr>";

        $response= json_decode('{"ok":true,"result":[{"update_id":280820116,
"message":{"message_id":6,"from":{"id":463836112,"is_bot":false,"first_name":"\u041a\u043e\u043d\u0441\u0442\u0430\u043d\u0442\u0438\u043d","last_name":"\u042f\u043a\u043e\u0432\u043b\u0435\u0432","language_code":"ru"},"chat":{"id":463836112,"first_name":"\u041a\u043e\u043d\u0441\u0442\u0430\u043d\u0442\u0438\u043d","last_name":"\u042f\u043a\u043e\u0432\u043b\u0435\u0432","type":"private"},"date":1587547181,"text":"rewr"}},{"update_id":280820117,
"message":{"message_id":7,"from":{"id":463836112,"is_bot":false,"first_name":"\u041a\u043e\u043d\u0441\u0442\u0430\u043d\u0442\u0438\u043d","last_name":"\u042f\u043a\u043e\u0432\u043b\u0435\u0432","language_code":"ru"},"chat":{"id":463836112,"first_name":"\u041a\u043e\u043d\u0441\u0442\u0430\u043d\u0442\u0438\u043d","last_name":"\u042f\u043a\u043e\u0432\u043b\u0435\u0432","type":"private"},"date":1587548361,"text":"fsdfsdfds"}},{"update_id":280820118,
"message":{"message_id":8,"from":{"id":463836112,"is_bot":false,"first_name":"\u041a\u043e\u043d\u0441\u0442\u0430\u043d\u0442\u0438\u043d","last_name":"\u042f\u043a\u043e\u0432\u043b\u0435\u0432","language_code":"ru"},"chat":{"id":463836112,"first_name":"\u041a\u043e\u043d\u0441\u0442\u0430\u043d\u0442\u0438\u043d","last_name":"\u042f\u043a\u043e\u0432\u043b\u0435\u0432","type":"private"},"date":1587548363,"text":"dsfdsfsdfds"}},{"update_id":280820119,
"message":{"message_id":9,"from":{"id":463836112,"is_bot":false,"first_name":"\u041a\u043e\u043d\u0441\u0442\u0430\u043d\u0442\u0438\u043d","last_name":"\u042f\u043a\u043e\u0432\u043b\u0435\u0432","language_code":"ru"},"chat":{"id":463836112,"first_name":"\u041a\u043e\u043d\u0441\u0442\u0430\u043d\u0442\u0438\u043d","last_name":"\u042f\u043a\u043e\u0432\u043b\u0435\u0432","type":"private"},"date":1587548365,"text":"sdfsdfdsfdsfsdfsdf"}},{"update_id":280820120,
"message":{"message_id":10,"from":{"id":456353715,"is_bot":false,"first_name":"\u041d\u0430\u0441\u0442\u0430\u0441\u0438\u044f","last_name":"\u042f\u043a\u043e\u0432\u043b\u0435\u0432\u0430","username":"NastasichBlack","language_code":"ru"},"chat":{"id":456353715,"first_name":"\u041d\u0430\u0441\u0442\u0430\u0441\u0438\u044f","last_name":"\u042f\u043a\u043e\u0432\u043b\u0435\u0432\u0430","username":"NastasichBlack","type":"private"},"date":1587548663,"text":"/start","entities":[{"offset":0,"length":6,"type":"bot_command"}]}},{"update_id":280820121,
"message":{"message_id":11,"from":{"id":456353715,"is_bot":false,"first_name":"\u041d\u0430\u0441\u0442\u0430\u0441\u0438\u044f","last_name":"\u042f\u043a\u043e\u0432\u043b\u0435\u0432\u0430","username":"NastasichBlack","language_code":"ru"},"chat":{"id":456353715,"first_name":"\u041d\u0430\u0441\u0442\u0430\u0441\u0438\u044f","last_name":"\u042f\u043a\u043e\u0432\u043b\u0435\u0432\u0430","username":"NastasichBlack","type":"private"},"date":1587548672,"text":"Got"}}]}
');



        if($response->ok){
            foreach($response->result as $update){
                echo $update->message->text."<br>";
            }
        }
        echo"<pre>"; var_dump($response);

        die;
        */

        /*
        Yii::$app->telegram->sendMessage([
	'chat_id' => 294349100,
	'text' => 'Denn Kruds',
        ]);*/

    }
    /**
     * Иницилизация для работы "Забыл пароль"
     */
    public function initPass(){
        
        // ...
        
        
        //Отослать письмо с кодом
        $this->subject="EMIZ - Запрос изменения пароля.";
        $this->textBody="";

    }
    /**
     * Чистка просроченныйх хешей
     */
    public function clearOutHash(){
        Yii::$app->db->createCommand("DELETE FROM hashs WHERE (NOW() > date_out)")->execute();
    }
    /**
     * Удаление ХЕШЕЙ по пользователю
     */
    public function clearHashsByUserId(){
        Yii::$app->db->createCommand("DELETE FROM hashs WHERE users_id=:users_id")->bindValues([
                    ':users_id' => (int)$this->users_id,
                ])->execute();
    }
    /**
     * Добавить ХЕШ
     * 
     * @throws \Exception
     */
    public function insHash(){
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try{

            $rez = Yii::$app->db->createCommand(
                "INSERT INTO hashs (hashs, users_id, date_out)VALUES "
                . "(:hashs, :users_id, DATE_ADD(NOW(), INTERVAL 2 HOUR))"
                    )->bindValues([
                    ':hashs' => (int)$this->hashs,
                    ':users_id' => (int)$this->users_id,
                ])->execute();
            $transaction->commit();

        }catch(\Exception $e) {
            Yii::$app->session->setFlash('msg', 'Системная ошибка! Обратитесть к администратору.');
            $transaction->rollBack();
            throw $e;
            //Yii::$app->response->redirect( '/login/' );
        }

    }
    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param string $email the target email address
     * @return bool whether the model passes validation
     */
    public function sendEmail(){
        
        $admin_email=Yii::$app->params['adminEmail'];
        
        Yii::$app->mailer->compose()
            ->setTo($this->user_email)
            ->setFrom([$admin_email => $admin_email])
            ->setSubject($this->subject)
            ->setTextBody($this->textBody)
            ->send();

        return true;
        
    }
    /**
     * Получить почтовый адрес
     * 
     * @param type $id
     * @return type
     */
    public function getUserEmail($id){
                
        $UsersForm = new UsersForm();
        $arr=$UsersForm->getCurrUser($id);
        return $arr['info_email'];
        
    }
    /**
     * Получить пользователя по ХЭШ (для формы забыл пароль)
     * 
     * @return type
     */
    public function getStatus(){
        return Yii::$app->db->createCommand("SELECT users_id FROM hashs WHERE hashs=:hashs AND NOW() < date_out")->bindValues( [
            ':hashs' => $this->h
        ] )->queryScalar();
    }
    /**
     * Получить ХЭШ по пользователю (для получения ID telegram)
     * 
     * @return type
     */
    public function getHashByUser($id){
        return Yii::$app->db->createCommand("SELECT hashs FROM hashs WHERE users_id=:users_id AND NOW() < date_out")->bindValues( [
            ':users_id' => (int)$id
        ] )->queryScalar();
    }
    
    public function updHashsTelegram($id_telegram){
        $id_telegram=strval($id_telegram);
        
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try{
            $rez = Yii::$app->db->createCommand(
                "UPDATE user_info `i`
                LEFT JOIN link_users_users_info `lui` ON(`lui`.id_users_info = `i`.id)
                SET id_telegramm=:id_telegramm
                WHERE `lui`.id_users = :user_id")->bindValues([
                ':user_id' => (int)$this->users_id,
                ':id_telegramm' => $id_telegram,
            ])->execute();
            $transaction->commit();

            Yii::$app->session->setFlash('msg', 'ID телеграмм успешно установлен: '.$id_telegram);

        }catch(\Exception $e) {
            Yii::$app->session->setFlash('msg', 'Системная ошибка! Обратитесть к администратору.');
            $transaction->rollBack();
        }
        
        
        
    }
}
