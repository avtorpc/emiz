<?php
namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Description of AuthrestForm
 *
 * @author kkk
 */
class AuthrestForm extends Model{
    public $h;
    public $password_repeat;
    public $password;
    
    public function rules(){
        return [
            [['h'], 'trim'],
            [['password_repeat'], 'string', 'min' => 6],
            [['password'], 'string', 'min' => 6],
            ['password', 'compare']
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'h' => 'Хеш',
            'password_repeat' => 'Еще пароль',
            'password' => 'Пароль',
        ];
    }

    public function getStatus(){
        return Yii::$app->db->createCommand("SELECT users_id FROM hashs WHERE hashs=:hashs AND NOW() < date_out")->bindValues( [
            ':hashs' => $this->h
        ] )->queryScalar();
      
    }
    

}
