function preloader() {
  $(() => {
    setInterval(() => {
      let p = $(".preloader-wrapper", 0);
      p.css("opacity");
      setInterval(() => p.remove(), parseInt(p.css("--duration")) * 1000);
    }, 2000);
  });
}

preloader();

/// первая страница которая scroll
AOS.init({
  disable: function() {
    var maxWidth = 992;
    return window.innerWidth < maxWidth;
  }
});

window.addEventListener("load", AOS.refresh);

$(document).ready(function() {
  $("#home-owl").owlCarousel({
    items: 1,
    loop: true,
    dots: false,
    animateIn: "fadeIn",
    animateOut: "fadeOut",
    mouseDrag: false,
    pullDrag: false,
    autoHeight: true,
    touchDrag: false
  });

  //!infobox

  $(".infobox1")
    .on("mouseover", () => {
      $(".bio .contain-info-box").addClass("contain-info-box-active1");
    })
    .on("mouseout", () => {
      $(".bio .contain-info-box").removeClass("contain-info-box-active1");
    })
    .on("click", () => {
      $(".bio .contain-info-box").toggleClass(
        "contain-info-box-active-important1"
      );
    });

  $(".infobox2")
    .on("mouseover", () => {
      $(".polifenols .contain-info-box").addClass("contain-info-box-active2");
    })
    .on("mouseout", () => {
      $(".polifenols .contain-info-box").removeClass(
        "contain-info-box-active2"
      );
    })
    .on("click", () => {
      $(".polifenols .contain-info-box").toggleClass(
        "contain-info-box-active-important2"
      );
    });

  //!smooth go to #link

  $("a.go").click(function(e) {
    e.preventDefault();
    elementClick = $(this).attr("href");
    destination = $(elementClick).offset().top;
    $("body,html").animate({ scrollTop: destination }, 1000);
  });

  // ÐŸÐ°Ñ€Ð°Ð»Ð»Ð°ÐºÑ Ð¾Ñ‚ Ð´Ð²Ð¸Ð¶ÐµÐ½Ð¸Ñ Ð¼Ñ‹ÑˆÐ¸
  if ($(window).width() > 960) {
    $("body").parallax({
      elements: [
        {
          selector: ".circle",
          properties: {
            x: {
              right: {
                initial: 100,
                multiplier: 0.04,
                unit: "px",
                invert: false
              }
            },
            y: {
              top: {
                initial: -400,
                multiplier: 0.04,
                unit: "px",
                invert: true
              }
            }
          }
        }
      ]
    });
  }

  if ($(window).width() > 960) {
    $("body").parallax({
      elements: [
        {
          selector: ".circlenew",
          properties: {
            x: {
              right: {
                initial: 100,
                multiplier: 0.04,
                unit: "px",
                invert: false
              }
            },
            y: {
              top: {
                initial: -420,
                multiplier: 0.04,
                unit: "px",
                invert: true
              }
            }
          }
        }
      ]
    });
  }

  if ($(window).width() > 960) {
    $("body").parallax({
      elements: [
        {
          selector: ".circle2",
          properties: {
            x: {
              right: {
                initial: 250,
                multiplier: 0.04,
                unit: "px",
                invert: false
              }
            },
            y: {
              top: {
                initial: -370,
                multiplier: 0.04,
                unit: "px",
                invert: true
              }
            }
          }
        }
      ]
    });
  }

  if ($(window).width() > 960) {
    $("body").parallax({
      elements: [
        {
          selector: ".circle3",
          properties: {
            x: {
              right: {
                initial: 130,
                multiplier: 0.04,
                unit: "px",
                invert: false
              }
            },
            y: {
              top: {
                initial: -560,
                multiplier: 0.04,
                unit: "px",
                invert: true
              }
            }
          }
        }
      ]
    });
  }

  if ($(window).width() > 960) {
    $("body").parallax({
      elements: [
        {
          selector: ".circle4",
          properties: {
            x: {
              right: {
                initial: 160,
                multiplier: 0.04,
                unit: "px",
                invert: false
              }
            },
            y: {
              top: {
                initial: -550,
                multiplier: 0.04,
                unit: "px",
                invert: true
              }
            }
          }
        }
      ]
    });
  }

  if ($(window).width() > 960) {
    $("body").parallax({
      elements: [
        {
          selector: ".circle5",
          properties: {
            x: {
              right: {
                initial: 50,
                multiplier: 0.04,
                unit: "px",
                invert: false
              }
            },
            y: {
              top: {
                initial: -510,
                multiplier: 0.04,
                unit: "px",
                invert: true
              }
            }
          }
        }
      ]
    });
  }

  //! sticky class add on move

  $(window).scroll(function() {
    if ($(this).scrollTop() > 1) {
      $(".fixed-nav").addClass("sticky");
    } else {
      $(".fixed-nav").removeClass("sticky");
    }
  });

  $(window).scroll(function() {
    if ($(this).scrollTop() > 600) {
      $(".small-nav").addClass("sticky");
    } else {
      $(".small-nav").removeClass("sticky");
    }
  });

  $("#small-main-navigaiton").onePageNav({
    currentClass: "active",
    changeHash: false,
    scrollSpeed: 750,
    scrollThreshold: 0.5,
    filter: "",
    easing: "swing"
  });

  $(".faq-navi__list").delegate("li", "click", function() {
    $(this)
      .addClass("bactive")
      .siblings()
      .removeClass("bactive");
  });

  // FAQ opening

  var faqsSections = $(".faq-group"),
    faqTrigger = $(".trigger"),
    faqsContainer = $(".faq-items"),
    faqsCategoriesContainer = $(".categories"),
    faqsCategories = faqsCategoriesContainer.find("a"),
    closeFaqsContainer = $(".cd-close-panel");

  faqTrigger.on("click", function(event) {
    event.preventDefault();
    $(this)
      .next(".faq-content")
      .slideToggle(200)
      .end()
      .parent("li")
      .toggleClass("content-visible");
  });

  $("#contacts-slid").owlCarousel({
    items: 1,
    loop: true,
    animateIn: "fadeIn",
    animateOut: "fadeOut",
    mouseDrag: false,
    pullDrag: false,
    dots: true,
    dotsData: true,
    dotsContainer: ".test-dot-container"
  });

  $(".contacts-slid__next").click(function() {
    $("#contacts-slid").trigger("next.owl.carousel");
  });

  $(" .faq-footer__link, .contacts-info__btn").on("click", () => {
    $("#examplebuy2").arcticmodal({
      overlay: {
        css: {
          backgroundColor: "#290D3F",
          // backgroundImage: 'url(images/overlay.png)',
          // backgroundRepeat: 'repeat',
          // backgroundPosition: '50% 0',
          opacity: 0.8
        }
      }
    });
  });

  $(" .bay-from-form").on("click", (e) => {
    e.preventDefault();
    $("#examplebuy2").arcticmodal({
      overlay: {
        css: {
          backgroundColor: "#290D3F",
          // backgroundImage: 'url(images/overlay.png)',
          // backgroundRepeat: 'repeat',
          // backgroundPosition: '50% 0',
          opacity: 0.8
        }
      }
    });
  });

  $(".partners__btn_second").on("click", () => {
    $("#becomepartner").arcticmodal({
      overlay: {
        css: {
          backgroundColor: "#290D3F",
          // backgroundImage: 'url(images/overlay.png)',
          // backgroundRepeat: 'repeat',
          // backgroundPosition: '50% 0',
          opacity: 0.8
        }
      }
    });
  });

  $(".partners__btn_first").on("click", () => {
    $("#exampleModal").arcticmodal({
      overlay: {
        css: {
          backgroundColor: "#290D3F",
          // backgroundImage: 'url(images/overlay.png)',
          // backgroundRepeat: 'repeat',
          // backgroundPosition: '50% 0',
          opacity: 0.8
        }
      }
    });
  });

  $(
    ".products-header #product-168, .products-header #product-171, .products-header #product-195, .products-header #product-198"
  ).addClass("thisInactive");

  // $("#custom-option1").on("click", () => {
  //   $(
  //     "#products-hero-wrap-buy-box-list__item1, #product-165, #product-192"
  //   ).removeClass("thisInactive");
  //   $(
  //     "#products-hero-wrap-buy-box-list__item2, #products-hero-wrap-buy-box-list__item3, #product-168, #product-171, #product-195, #product-198"
  //   ).addClass("thisInactive");
  // });

  // $("#custom-option2").on("click", () => {
  //   $(
  //     "#products-hero-wrap-buy-box-list__item2, #product-168, #product-195"
  //   ).removeClass("thisInactive");
  //   $(
  //     "#products-hero-wrap-buy-box-list__item1, #products-hero-wrap-buy-box-list__item3, #product-165, #product-171, #product-192, #product-198"
  //   ).addClass("thisInactive");
  // });

  // $("#custom-option3").on("click", () => {
  //   $(
  //     "#products-hero-wrap-buy-box-list__item3, #product-171, #product-198"
  //   ).removeClass("thisInactive");
  //   $(
  //     "#products-hero-wrap-buy-box-list__item1, #products-hero-wrap-buy-box-list__item2, #product-168, #product-165, #product-192, #product-195"
  //   ).addClass("thisInactive");
  // });

  $(".owl-inner__link")
    .on("mouseenter", () => {
      $(".supershadow").addClass("opain");
    })
    .on("mouseleave", () => {
      $(".supershadow").removeClass("opain");
    });

  // $(".custom-select__trigger, .custom-select__trigger2").on("click", () => {
  //   $(".custom-select__trigger, .custom-select__trigger2").addClass(
  //     "opatrigger"
  //   );
  // });
  // $(".custom-options ").on("click", () => {
  //      console.log( $( this ).data( 'img') );
  // });
});

// init controller
var controller = new ScrollMagic.Controller();

$(function() {
  // wait for document ready
  // build scene
  var scene = new ScrollMagic.Scene({ triggerElement: "#pin1", duration: 1300 })
    .setPin("#floating-wrapper")

    .addTo(controller);
});

// accordion filter script

filterSelection("all");
function filterSelection(c) {
  var x, i;
  x = document.getElementsByClassName("filterDiv");
  if (c == "all") c = "";
  for (i = 0; i < x.length; i++) {
    w3RemoveClass(x[i], "show");
    if (x[i].className.indexOf(c) > -1) w3AddClass(x[i], "show");
  }
}

function w3AddClass(element, name) {
  var i, arr1, arr2;
  arr1 = element.className.split(" ");
  arr2 = name.split(" ");
  for (i = 0; i < arr2.length; i++) {
    if (arr1.indexOf(arr2[i]) == -1) {
      element.className += " " + arr2[i];
    }
  }
}

function w3RemoveClass(element, name) {
  var i, arr1, arr2;
  arr1 = element.className.split(" ");
  arr2 = name.split(" ");
  for (i = 0; i < arr2.length; i++) {
    while (arr1.indexOf(arr2[i]) > -1) {
      arr1.splice(arr1.indexOf(arr2[i]), 1);
    }
  }
  element.className = arr1.join(" ");
}

function remove_class_from_mobile_menu(){
  $( '.burger').removeClass( 'burger__background_one');
  $( '.burger').removeClass( 'burger__background_three');
  $( '.burger').removeClass( 'burger__background_two');
}

function change_color_in_mobile_nav ( slide_number ){

  remove_class_from_mobile_menu();

  if( slide_number == 'slide_one'){
    $( '#mob-nav-logo-1').css( { fill: "#7d5a9d" } );
    $( '#mob-nav-logo-2').css( { fill: "#7d5a9d" } );
    $( '.mob-nav' ).css( "border-bottom-color",  "#7d5a9d" );

    $( '.burger').css( "background", "#7d5a9d" );

    $( '.burger').before(function(){
      $(this).addClass("burger__background_one");
    });
    $( '.burger').after(function(){
      $(this).addClass("burger__background_one");
    });
  }
  if( slide_number == 'slide_two'){
    $( '#mob-nav-logo-1').css( { fill: "#5900ac" } );
    $( '#mob-nav-logo-2').css( { fill: "#5900ac" } );
    $( '.mob-nav' ).css( "border-bottom-color",  "#5900ac" );
    $( '.burger').css( "background", "#5900ac" );

    $( '.burger').before(function(){
      $(this).addClass("burger__background_two");
    });
    $( '.burger').after(function(){
      $(this).addClass("burger__background_two");
    });
  }
  if( slide_number == 'slide_three') {
    $('#mob-nav-logo-1').css({fill: "#2fa749"});
    $('#mob-nav-logo-2').css({fill: "#2fa749"});
    $('.mob-nav').css("border-bottom-color", "#2fa749");

    $( '.burger').css( "background", "#2fa749" );
    $('.burger').before(function () {
      $(this).addClass("burger__background_three");
    });
    $('.burger').after(function () {
      $(this).addClass("burger__background_three");
    });
  }
}

let burger = document.getElementById("burger"),
  nav = document.getElementById("main-nav");

burger.addEventListener("click", function(e) {
  this.classList.toggle("is-open");
  nav.classList.toggle("is-open");
});

const players = Plyr.setup(".js-player");

// Expose player so it can be used from the console
window.player = player;
