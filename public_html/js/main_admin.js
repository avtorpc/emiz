$(document).ready(function() {
  $("#account-dist-1").owlCarousel({
    items: 1,
    loop: true,
    dots: false,
    animateIn: "fadeIn",
    animateOut: "fadeOut",
    mouseDrag: false,
    pullDrag: false
  });

  $(".account-dist-main-floatin-wrap__next").click(function() {
    $("#account-dist-1").trigger("next.owl.carousel");
  });
  // Go to the previous item
  $(".account-dist-main-floatin-wrap__prev").click(function() {
    // With optional speed parameter
    // Parameters has to be in square bracket '[]'
    $("#account-dist-1").trigger("prev.owl.carousel");
  });

  $(".diss-prof-wrap-card__add").on("click", () => {
    $(".diss-prof-wrap-card").addClass("cardedit");
    $(".diss-prof-card-add").addClass("addadd");
  });
  $(".diss-prof-card-add-nav__cancel, .diss-prof-card-add-nav__add").on(
    "click",
    () => {
      $(".diss-prof-wrap-card").removeClass("cardedit");
      $(".diss-prof-card-add").removeClass("addadd");
    }
  );
  $(".diss-prof-cotnacts-navigation__btn").on("click", () => {
    $(".diss-add-address-delivery").addClass("add");
  });
  $(
    ".diss-add-address-delivery-nav__cancel, .diss-add-address-delivery-nav__add"
  ).on("click", () => {
    $(".diss-add-address-delivery").removeClass("add");
  });
  $(".diss-prof-personal__link").on("click", () => {
    $(".diss-prof-contacts-edit").addClass("add");
  });
  $(".diss-prof-contacts-edit__btn").on("click", () => {
    $(".diss-prof-contacts-edit").removeClass("add");
  });
  $(".diss-prof-addressies__btn").on("click", () => {
    $(".diss-add-address-delivery__extra").addClass("add");
  });
  $(
    ".diss-add-address-delivery-nav__cancel_cancel, .diss-add-address-delivery-nav__add_delete, .diss-add-address-delivery-nav__add_add"
  ).on("click", () => {
    $(".diss-add-address-delivery__extra").removeClass("add");
  });
  //when the button is clicked
  $("#showMenu").click(function() {
    //apply toggleable classes
    $("#nav").toggleClass("show");
    $("#showMenu").toggleClass("moveButton");
  });

  $("#wrapper").click(function() {
    $("#nav").removeClass("show");
    $("#showMenu").removeClass("moveButton");
  });

  $("#admin-profile__link2").on("click", () => {
    $(".admin-profile-change").addClass("add");
  });
  $("#admin-profile__link1").on("click", () => {
    $(".admin-profile-personal").addClass("add");
  });
/*
  $(".admin-profile-change .admin-profile-change__submit").on("click", () => {
    $(".admin-profile-change").removeClass("add");
    $(".admin-profile-submit").addClass("add");
  });

  $(".admin-profile-submit .admin-profile-change__submit").on("click", () => {
    $(".admin-profile-submit").removeClass("add");
  });
  */
  $(".admin-diss-top__add, .admin-diss-inner-info-box__link").on(
    "click",
    () => {
      $(".admin-diss-over").addClass("add");
    }
  );
  $(".admin-diss-form__navigation a").on("click", () => {
    $(".admin-diss-over").removeClass("add");
  });
});

let burger = document.getElementById("burger"),
  nav = document.getElementById("main-nav");

burger.addEventListener("click", function(e) {
  this.classList.toggle("is-open");
  nav.classList.toggle("is-open");
});
