<?php

namespace app\controllers;

use Yii;
use app\models\Products;

/**
 * Description of ProductsController
 *
 * @author kkk
 */

class ProductsController extends WebAuthController {
    
    
    use trait_TranslateProductsByName;
    
    private $m = null;
    private $data_title = [];
    private $good_tree = null;


    public function init(){
                    
          $this->data_title['path']="products";
          $this->data_title['body_class']="products-controller page-template page-template-products page-template-products-php page page-id-10 page-parent class-name theme-Emiz woocommerce-js woocommerce-active";

          $this->m = new Products();
          $this->good_tree = $this->m->getGoodTree();
    }

    public function actionPremium( $lang=null ){

        Yii::$app->response->redirect( '/products/' . $lang . '/' );
    }

    public function actionIndex( $lang=null ){

        $good_name = 'emiz_premium';
        
        $this->view->registerMetaTag(['name'=>'keywords', 'content'=>Yii::t( 'app', '_CONTENT_PRODUCTS_' )]);
        $this->view->registerMetaTag(['name'=>'description', 'content'=>Yii::t( 'app', '_DESCRIPTION_PRODUCTS_' )]);
        $this->view->title=Yii::t( 'app', '_TITLE_PRODUCTS_' );

        $g = $this->m->getGood( $good_name);
        
        return $this->render('index.twig', [
            'data_title'=>$this->data_title,
            'good_name' => $good_name,
            'good' => $this->m->getGood( $good_name),
            'prod' => $this->m->getProd( $good_name),
            'default_selected_prod' => 'emiz_premium_15',
            'prod_tree' => $this->m->getProdTree(),
            'good_tree' => $this->good_tree,
            'tranlate_name_in_good_tree' => $this->translate_for_name_prod( $this->good_tree ),
            'prevlink' => '/products/pasta/' . $lang . '/',
            'nextlink' => '/products/tavricheskij/' . $lang . '/',
            'url_premium' => '/products/' . $lang . '/',
            'url_tavr' => '/products/tavricheskij/' . $lang . '/',
            'url_pasta' => '/products/pasta/' . $lang . '/',
            'good_short_desc' => Yii::t( 'app/' . $good_name, mb_strtoupper( '_' . $g['good_uniq_id'] . '_good_short_desc_' ) ),
            'good_sostav' => Yii::t( 'app/' . $good_name, mb_strtoupper( '_' . $g['good_uniq_id'] . '_good_sostav_' ) ),
            'good_note_1' => Yii::t( 'app/' . $good_name, mb_strtoupper( '_' . $g['good_uniq_id'] . '_good_note_1_' ) ),
            'good_note_2' => Yii::t( 'app/' . $good_name, mb_strtoupper( '_' . $g['good_uniq_id'] . '_good_note_2_' ) ),
            'good_note_3' => Yii::t( 'app/' . $good_name, mb_strtoupper( '_' . $g['good_uniq_id'] . '_good_note_3_' ) ),
            'good_long_desc' => Yii::t( 'app/' . $good_name, mb_strtoupper( '_' . $g['good_uniq_id'] . '_good_long_desc_' ) ),
            'good_name' => Yii::t( 'app/' . $good_name, '_NAME_' ),
            'good_name' => $good_name
        ]);
    }


    public function actionTavricheskij( $lang=null ){
        $good_name = 'emiz_tavr';

        $this->view->registerMetaTag(['name'=>'keywords', 'content'=>Yii::t( 'app', '_CONTENT_PRODUCTS_' )]);
        $this->view->registerMetaTag(['name'=>'description', 'content'=>Yii::t( 'app', '_DESCRIPTION_PRODUCTS_' )]);
        $this->view->title=Yii::t( 'app', '_TITLE_PRODUCTS_' );

        $g = $this->m->getGood( $good_name);
        
        return $this->render('index.twig', [
            'data_title'=>$this->data_title,
            'good_name' => $good_name,
            'good' => $this->m->getGood( $good_name),
            'prod' => $this->m->getProd( $good_name),
            'default_selected_prod' => 'emiz_tavr_05',
            'prod_tree' => $this->m->getProdTree(),
            'good_tree' => $this->good_tree,
            'tranlate_name_in_good_tree' => $this->translate_for_name_prod( $this->good_tree ),
            'prevlink' => '/products/' . $lang . '/',
            'nextlink' => '/products/pasta/' . $lang . '/',
            'url_premium' => '/products/' . $lang . '/',
            'url_tavr' => '/products/tavricheskij/' . $lang . '/',
            'url_pasta' => '/products/pasta/' . $lang . '/',
            'good_short_desc' => Yii::t( 'app/' . $good_name, mb_strtoupper( '_' . $g['good_uniq_id'] . '_good_short_desc_' ) ),
            'good_sostav' => Yii::t( 'app/' . $good_name, mb_strtoupper( '_' . $g['good_uniq_id'] . '_good_sostav_' ) ),
            'good_note_1' => Yii::t( 'app/' . $good_name, mb_strtoupper( '_' . $g['good_uniq_id'] . '_good_note_1_' ) ),
            'good_note_2' => Yii::t( 'app/' . $good_name, mb_strtoupper( '_' . $g['good_uniq_id'] . '_good_note_2_' ) ),
            'good_note_3' => Yii::t( 'app/' . $good_name, mb_strtoupper( '_' . $g['good_uniq_id'] . '_good_note_3_' ) ),
            'good_long_desc' => Yii::t( 'app/' . $good_name, mb_strtoupper( '_' . $g['good_uniq_id'] . '_good_long_desc_' ) ),
            'good_name_zagolovok' => Yii::t( 'app/' . $good_name, '_NAME_' ),
            'good_name' => $good_name
        ]);
    }

    public function actionPasta( $lang=null ){

        $good_name = 'pasta';

        $this->view->registerMetaTag(['name'=>'keywords', 'content'=>Yii::t( 'app', '_CONTENT_PRODUCTS_' )]);
        $this->view->registerMetaTag(['name'=>'description', 'content'=>Yii::t( 'app', '_DESCRIPTION_PRODUCTS_' )]);
        $this->view->title=Yii::t( 'app', '_TITLE_PRODUCTS_' );
        
        $g = $this->m->getGood( $good_name);
        
        return $this->render('pasta.twig', [
            'data_title'=>$this->data_title,
            'good_name' => $good_name,
            'good' => $this->m->getGood( $good_name),
            'prod' => $this->m->getProd( $good_name),
            'default_selected_prod' => 'pasta_035',
            'prod_tree' => $this->m->getProdTree(),
            'good_tree' => $this->good_tree,
            'tranlate_name_in_good_tree' => $this->translate_for_name_prod( $this->good_tree ),
            'prevlink' => '/products/tavricheskij/' . $lang . '/',
            'nextlink' => '/products/' . $lang . '/',
            'url_premium' => '/products/' . $lang . '/',
            'url_tavr' => '/products/tavricheskij/' . $lang . '/',
            'url_pasta' => '/products/pasta/' . $lang . '/',
            'good_short_desc' => Yii::t( 'app/pasta', mb_strtoupper( '_' . $g['good_uniq_id'] . '_good_short_desc_' ) ),
            'good_sostav' => Yii::t( 'app/pasta', mb_strtoupper( '_' . $g['good_uniq_id'] . '_good_sostav_' ) ),
            'good_note_1' => Yii::t( 'app/pasta', mb_strtoupper( '_' . $g['good_uniq_id'] . '_good_note_1_' ) ),
            'good_note_2' => Yii::t( 'app/pasta', mb_strtoupper( '_' . $g['good_uniq_id'] . '_good_note_2_' ) ),
            'good_note_3' => Yii::t( 'app/pasta', mb_strtoupper( '_' . $g['good_uniq_id'] . '_good_note_3_' ) ),
            'good_long_desc' => Yii::t( 'app/pasta', mb_strtoupper( '_' . $g['good_uniq_id'] . '_good_long_desc_' ) ),
            'good_name_zagolovok' => Yii::t( 'app/' . $good_name, '_NAME_' ),
            'good_name' => $good_name
        ]);
    }

}
