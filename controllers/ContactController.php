<?php
namespace app\controllers;

use Yii;
use app\models\QuestionForm;
/**
 *
 */
class ContactController extends WebAuthController {


    private $action_url_question = '/contact/';


    public function actionIndex( $lang=null )
    {

        $model = new QuestionForm();
        $err = null;
        $success = null;

        if ( Yii::$app->request->isPost) {

            if ( $model->load( Yii::$app->request->post() ) && $model->validate() ) {

                    $params = [
                        ':name' => $model->getName(),
                        ':contact' => $model->getPhone()
                    ];

                $rez = Yii::$app->db->createCommand('INSERT INTO `question_contact`( `name`, `contact` ) 
                                                             VALUES (:name, :contact )')->bindValues( $params )->execute();
                
                 $admin_email=Yii::$app->params['adminEmail'];
                Yii::$app->mailer->compose()
                        ->setTo( Yii::$app->params['sales'] )
                        ->setFrom( [$admin_email => $admin_email] )
                        ->setSubject( 'Звонок' )
                        ->setTextBody( "Вам поступил запрос на обратный звонок от " . $model->getName() . ", номер телефона: " . $model->getPhone()  )
                        ->send();


                    $success = [ 'success' => [ 0 => Yii::t( 'app/contacts', '_TEXT_17_')] ];

            }else {

                if ( $model->hasErrors() ) {
                    $err = $model->getErrors();
                }
            }
        }

        $this->view->registerMetaTag(['name'=>'keywords', 'content'=>Yii::t( 'app/contacts', '_TEXT_14_')]);
        $this->view->registerMetaTag(['name'=>'description', 'content'=>Yii::t( 'app/contacts', '_TEXT_15_')]);
        $this->view->title = Yii::t( 'app/contacts', '_TEXT_16_');

        $data_title=[];
        $data_title['path']="contact";
        $data_title['body_class']="page-template page-template-contacts page-template-contacts-php page page-id-22 class-name theme-Emiz woocommerce-js woocommerce-active";
        
        return $this->render('index.twig', [
            'MES_ERR' => $err,
            'MES_SUCCESS' => $success,
            'model' => $model,
            'action_url_question' => $this->action_url_question . $lang . '/',
            'data_title' => $data_title,
        ]);
    }

}
