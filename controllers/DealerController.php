<?php

namespace app\controllers;

use Yii;
use app\models\CardForm;
use app\models\AdressForm;
use app\models\UsersForm;
use app\models\UserinfoForm;
use app\models\SortByDateForm;
use app\models\RepForm;
use app\models\ProdDealer;
use app\models\Products;

/**
 * Dealer Controller
 *
 * @author kkk
 */
class DealerController extends WebAuthController {


    use trait_CreateViewBasket;
    use trait_AddGeoSXtoBASE;


    private $basket_table = 'basket';


    public function actionIndex( $id )
    {

        $this->init_();
        $user = new UsersForm;
        $products = new ProdDealer;
        $m = new Products();//Для корзины
       

        $baskets = Yii::$app->db->createCommand("SELECT `b`.`id`AS`basket_id`,
                                                    `b`.`name`AS`basket_name`,
                                                    `b`.`surname`AS`basket_surname`,
                                                    `b`.`middle_name`AS`basket_middle_name`,
                                                    `b`.`index`AS`basket_index`,
                                                    `b`.`city`AS`basket_city`,
                                                    `b`.`address`AS`basket_address`,
                                                    `b`.`home`AS`basket_home`,
                                                    `b`.`corpus`AS`basket_corpus`,
                                                    `b`.`flat`AS`basket_flat`,
                                                    `b`.`phone`AS`basket_phone`,
                                                    `b`.`email`AS`basket_email`,
                                                    `b`.`transport`AS`basket_transport`,
                                                    `b`.`pay_type`AS`basket_pay_type`,
                                                    `b`.`card_number`AS`basket_card_number`,
                                                    `b`.`card_date1`AS`basket_card_date1`,
                                                    `b`.`card_date2`AS`basket_card_date2`,
                                                    `b`.`card_numberback`AS`basket_card_numberback`,
                                                    `b`.`date_in`AS`basket_date_in`,
                                                    `b`.`date_close`AS`basket_date_close`,
                                                    `b`.`user_id`AS`basket_user_id`,
                                                    `b`.`remember`AS`basket_remember`,
                                                    `u`.`nickname`AS`users_nickname`,
                                                    `i`.`id`AS`uinfo_id`,
                                                    ((
                                                        SELECT SUM((`ii`.`unit_price` * `ii`.`cnt`))
                                                        FROM link_basket_basket_items `bb`
                                                        LEFT JOIN `basket_items` `ii` ON(`ii`.`id`=`bb`.`id_basket_item`)
                                                        WHERE `bb`.`id_basket`=`b`.`id`
                                                    ) + `b`.`transport`)
                                                    AS`summ_basket`
                                                FROM " . $this->basket_table . " AS  `b`
                                                LEFT JOIN `users` `u` ON(`b`.`user_id`=`u`.`id`)
                                                LEFT JOIN `link_users_users_info` `lui`ON(`lui`.`id_users`=`u`.`id`)
                                                LEFT JOIN `user_info` `i`ON(`i`.`id`=`lui`.`id_users_info`)
                                                WHERE `b`.`dealer_id`=:dealer_id")->bindValues( [ ':dealer_id' => $id] )->queryAll();
       

        $items = Yii::$app->db->createCommand("SELECT `b`.`id`AS`basket_id`,
                                                    `b`.`name`AS`basket_name`,
                                                    `b`.`surname`AS`basket_surname`,
                                                    `b`.`middle_name`AS`basket_middle_name`,
                                                    `b`.`user_id`AS`basket_user_id`,
                                                    `u`.`nickname`AS`users_nickname`,
                                                    `b`.`phone`AS`basket_phone`,
                                                    `b`.`date_in`AS`basket_date_in`,
                                                    `i`.`name`AS`basketitems_name`,
                                                    `i`.`volume`AS`basketitems_volume`,
                                                    `i`.`unit_price`AS`basketitems_unitprice`,
                                                    `i`.`cnt`AS`basketitems_cnt`,
                                                    `i`.`uniq_id`AS`basketitems_uniq_id`,
                                                    `p`.`id`AS`prod_id`,
                                                    `g`.`id`AS`good_id`,
                                                    `g`.`name`AS`good_name`,
                                                    `g`.`uniq_id`AS`good_uniq_id`
                                                FROM  " . $this->basket_table . " AS `b`
                                                LEFT JOIN `link_basket_basket_items` `lbi` ON(`b`.`id`=`lbi`.`id_basket`)
                                                LEFT JOIN `users` `u` ON(`b`.`user_id`=`u`.`id`)
                                                LEFT JOIN `basket_items` `i` ON(`i`.`id`=`lbi`.`id_basket_item`)
                                                LEFT JOIN `products` `p` ON(`p`.`uniq_id`=`i`.`uniq_id`)
                                                LEFT JOIN `link_goods_products` `gp` ON(`gp`.`id_products`=`p`.`id`)
                                                LEFT JOIN `goods` `g` ON(`g`.`id`=`gp`.`id_goods`)
                                                WHERE `b`.`user_id`=:user_id")->bindValues( [':user_id' => $id] )->queryAll();

        $allSummBasket=0;
        foreach($items as $k=>$v){
            $allSummBasket+=$v['basketitems_unitprice'];
        }

        $basket_info=[];
        $basket_info['basket_summ']=$allSummBasket;


        $this->view->registerMetaTag(['name'=>'keywords', 'content'=>'Emiz']);
        $this->view->registerMetaTag(['name'=>'description', 'content'=>'Эликсир молодости и здоровья']);
        $this->view->title="Главная";

        return $this->render('index.twig', [
            'user'=>$user->getCurrUser($id),
            'baskets'=>$baskets,
            'items'=>$items,
            'basket_info'=>$basket_info,
            'products'=>$products->getAllProd(),
            'prod_tree' => $m->getProdTree(),
        ]);
    }


    /**
     * Профиль
     *
     * @return type
     */
    public function actionProfil( $id )
    {

        $user = new UsersForm;

        $cards = Yii::$app->db->createCommand("SELECT `c`.`card_id`,
                                                    `c`.`card_type`,
                                                    `c`.`card_name`,
                                                    `c`.`card_num`,
                                                    `c`.`data_mes`AS`card_data_mes`,
                                                    `c`.`data_god`AS`card_data_god`,
                                                    `c`.`card_oborot`,
                                                    `c`.`active`,
                                                    `u`.`id`AS`users_id`,
                                                    `u`.`nickname`AS`users_nickname`,
                                                    `ui`.`id`AS`userinfo_id`
                                                    FROM `card` `c`
                                                    LEFT JOIN `link_users_card` `luc` ON(`luc`.`card_id`=`c`.`card_id`)
                                                    LEFT JOIN `users` `u` ON(`u`.`id`=`luc`.`users_id`)
                                                    LEFT JOIN `link_users_users_info` `lui`ON(`lui`.`id_users`=`u`.`id`)
                                                    LEFT JOIN `user_info` `ui`ON(`ui`.`id`=`lui`.`id_users_info`)
                                                    WHERE `luc`.`users_id`=:id")->bindValues( [
            ':id' => $id
        ] )->queryAll();

        $adress = Yii::$app->db->createCommand("SELECT `a`.`adress_id`,
                                                        `a`.`region`,
                                                        `a`.`city`,
                                                        `a`.`street`,
                                                        `a`.`house`,
                                                        `a`.`korpus`,
                                                        `a`.`apartament`,
                                                        `a`.`pochta_index`,
                                                        `a`.`active`,
                                                        `u`.`id`AS`users_id`,
                                                        `u`.`nickname`AS`users_nickname`,
                                                        `i`.`id`AS`userinfo_id`,
                                                        `i`.`name`AS`userinfo_name`,
                                                        `i`.`famuly`AS`userinfo_famuly`,
                                                        `i`.`otchestvo`AS`userinfo_otchestvo`,
                                                        `i`.`phone`AS`userinfo_phone`,
                                                        `i`.`email`AS`userinfo_email`
                                                    FROM `adress` `a`
                                                    LEFT JOIN `link_users_adress` `lua` ON(`lua`.`adress_id`=`a`.`adress_id`)
                                                    LEFT JOIN `users` `u` ON(`u`.`id`=`lua`.`users_id`)
                                                    LEFT JOIN `link_users_users_info` `lui`ON(`lui`.`id_users`=`u`.`id`)
                                                    LEFT JOIN `user_info` `i`ON(`i`.`id`=`lui`.`id_users_info`)
                                                    WHERE `lua`.`users_id`=:id")->bindValues( [
            ':id' => $id
        ] )->queryAll();

        $adrkont = Yii::$app->db->createCommand("SELECT `a`.`adress_id`AS`adrkont_id`,
                                                        `a`.`region`,
                                                        `a`.`city`,
                                                        `a`.`street`,
                                                        `a`.`house`,
                                                        `a`.`korpus`,
                                                        `a`.`apartament`,
                                                        `a`.`pochta_index`,
                                                        `a`.`active`,
                                                        `u`.`id`AS`users_id`,
                                                        `u`.`nickname`AS`users_nickname`,
                                                        `i`.`id`AS`userinfo_id`,
                                                        `i`.`name`AS`userinfo_name`,
                                                        `i`.`famuly`AS`userinfo_famuly`,
                                                        `i`.`otchestvo`AS`userinfo_otchestvo`,
                                                        `i`.`phone`AS`userinfo_phone`,
                                                        `i`.`email`AS`userinfo_email`
                                                    FROM `link_users_adrkont` `luk`
                                                    LEFT JOIN `adress` `a` ON(`luk`.`adrkonts_id`=`a`.`adress_id`)
                                                    LEFT JOIN `users` `u` ON(`u`.`id`=`luk`.`users_id`)
                                                    LEFT JOIN `link_users_users_info` `lui`ON(`lui`.`id_users`=`u`.`id`)
                                                    LEFT JOIN `user_info` `i`ON(`i`.`id`=`lui`.`id_users_info`)
                                                    WHERE `luk`.`users_id`=:id")->bindValues( [
            ':id' => $id
        ] )->queryAll();

        $this->view->registerMetaTag(['name'=>'keywords', 'content'=>'Emiz']);
        $this->view->registerMetaTag(['name'=>'description', 'content'=>'Эликсир молодости и здоровья']);
        $this->view->title="Профиль";

        return $this->render('profil.twig', [
            'user'=>$user->getCurrUser($id),
            'cards'=>$cards,
            'adress'=>$adress,
            'adrkont'=>$adrkont,
        ]);
    }
    /**
     * Заказы
     *
     * @return type
     */
    public function actionOrders( $id )
    {

        $this->init_();

        $msg="";
        $model = new SortByDateForm;

        if ( Yii::$app->request->isPost) {
            if ( $model->load( Yii::$app->request->post() ) && $model->validate() ) {

            }else{
                if ( $model->hasErrors() ) {
                    foreach($model->getErrors() as $k=>$v){
                        foreach($v as $vv){
                            $msg .= "$vv";
                        }
                    }
                }
            }
        }

        if($model->ord == 'DESC'){
            $ord = "DESC";
        }else{
            $ord = "ASC";
        }

        $user = new UsersForm;

        $baskets = Yii::$app->db->createCommand("SELECT `b`.`id`AS`basket_id`,
                                                `b`.`name`AS`basket_name`,
                                                `b`.`surname`AS`basket_surname`,
                                                `b`.`middle_name`AS`basket_middle_name`,
                                                `b`.`index`AS`basket_index`,
                                                `b`.`city`AS`basket_city`,
                                                `b`.`address`AS`basket_address`,
                                                `b`.`home`AS`basket_home`,
                                                `b`.`corpus`AS`basket_corpus`,
                                                `b`.`flat`AS`basket_flat`,
                                                `b`.`phone`AS`basket_phone`,
                                                `b`.`email`AS`basket_email`,
                                                `b`.`transport`AS`basket_transport`,
                                                `b`.`pay_type`AS`basket_pay_type`,
                                                `b`.`card_number`AS`basket_card_number`,
                                                `b`.`card_date1`AS`basket_card_date1`,
                                                `b`.`card_date2`AS`basket_card_date2`,
                                                `b`.`card_numberback`AS`basket_card_numberback`,
                                                `b`.`date_in`AS`basket_date_in`,
                                                `b`.`date_close`AS`basket_date_close`,
                                                `b`.`user_id`AS`basket_user_id`,
                                                `b`.`remember`AS`basket_remember`,
                                                `u`.`nickname`AS`users_nickname`,
                                                `i`.`id`AS`uinfo_id`,
                                                TIMESTAMPDIFF(HOUR, `b`.`date_in`, NOW())AS `basket_hour`,
                                                (
                                                    SELECT SUM((`ii`.`unit_price` * `ii`.`cnt`))
                                                    FROM link_basket_basket_items `bb`
                                                    LEFT JOIN `basket_items` `ii` ON(`ii`.`id`=`bb`.`id_basket_item`)
                                                    WHERE `bb`.`id_basket`=`b`.`id`
                                                )
                                                AS`summ_basket`
                                            FROM " . $this->basket_table . " AS `b`
                                            LEFT JOIN `users` `u` ON(`b`.`user_id`=`u`.`id`)
                                            LEFT JOIN `link_users_users_info` `lui`ON(`lui`.`id_users`=`u`.`id`)
                                            LEFT JOIN `user_info` `i`ON(`i`.`id`=`lui`.`id_users_info`)
                                            WHERE `b`.`dealer_id`=:dealer_id
                                            ORDER BY `b`.`date_in` {$ord}")->bindValues( [':dealer_id' => $id] )->queryAll();

        //Общая сумма с доставкой
        $allSummBasket=0;
        foreach($baskets as $k=>$v){
            $allSummBasket+=($v['summ_basket'] + $v['basket_transport']);
        }
        $basket_info=[];
        $basket_info['basket_summ']=$allSummBasket;


        $items = Yii::$app->db->createCommand("SELECT `b`.`id`AS`basket_id`,
                                                `b`.`name`AS`basket_name`,
                                                `b`.`surname`AS`basket_surname`,
                                                `b`.`middle_name`AS`basket_middle_name`,
                                                `b`.`user_id`AS`basket_user_id`,
                                                `u`.`nickname`AS`users_nickname`,
                                                `b`.`phone`AS`basket_phone`,
                                                `b`.`date_in`AS`basket_date_in`,
                                                `i`.`name`AS`basketitems_name`,
                                                `i`.`volume`AS`basketitems_volume`,
                                                `i`.`unit_price`AS`basketitems_unitprice`,
                                                `i`.`cnt`AS`basketitems_cnt`,
                                                `i`.`uniq_id`AS`basketitems_uniq_id`,
                                                `p`.`id`AS`prod_id`,
                                                `g`.`id`AS`good_id`,
                                                `g`.`name`AS`good_name`,
                                                `g`.`uniq_id`AS`good_uniq_id`
                                            FROM   " . $this->basket_table . " AS  `b`
                                            LEFT JOIN `link_basket_basket_items` `lbi`  ON(`b`.`id`=`lbi`.`id_basket`)
                                            LEFT JOIN `users` `u` ON(`b`.`user_id`=`u`.`id`)
                                            LEFT JOIN `basket_items` `i` ON(`i`.`id`=`lbi`.`id_basket_item`)
                                            LEFT JOIN `products` `p` ON(`p`.`uniq_id`=`i`.`uniq_id`)
                                            LEFT JOIN `link_goods_products` `gp` ON(`gp`.`id_products`=`p`.`id`)
                                            LEFT JOIN `goods` `g` ON(`g`.`id`=`gp`.`id_goods`)
                                            WHERE `b`.`dealer_id`=:dealer_id")->bindValues( [':dealer_id' => $id] )->queryAll();


        $this->view->registerMetaTag(['name'=>'keywords', 'content'=>'Emiz']);
        $this->view->registerMetaTag(['name'=>'description', 'content'=>'Эликсир молодости и здоровья']);
        $this->view->title="Заказы";

        return $this->render('orders.twig', [
            'user'=>$user->getCurrUser($id),
            'baskets'=>$baskets,
            'items'=>$items,
            'basket_info'=>$basket_info,
            'model'=>$model,
            'msg'=>$msg,
        ]);
    }
    /**
     * Отчеты
     *
     * @return type
     */
    public function actionReports( $id )
    {

        $this->init_(); ///$this->basket_table

        $user = new UsersForm;
        $RepForm = new RepForm;

        if ( $RepForm->load( Yii::$app->request->post() ) && $RepForm->validate() ){
            if(!$RepForm->checkDate()){
                $this->session->setFlash('msg', 'Дата начала должна быть меньше даты окончания');
            }
        }else {
            if ( $RepForm->hasErrors() ) {
                $this->session->setFlash('msg', 'Ошибка приходящих данных!');
                $this->session->setFlash('msg_arr', $RepForm->getErrors());
            }
        }

        $RepForm->inits($id, $this->basket_table );

        if(!$RepForm->checkDateMin()){//запускать только после $RepForm->inits($id)
            $this->session->setFlash('msg_arr', ["checkDateMin"=>["На дату начала {$RepForm->buf_dmin} заказов нет, автоустановка на {$RepForm->d_min}"]]);
        }
        if(!$RepForm->checkDateMax()){//запускать только после $RepForm->inits($id)
            if($this->session->hasFlash('msg_arr')){
                $this->session->setFlash('msg_arr', $this->session->getFlash('msg_arr') + ["checkDateMax"=>["На дату окончания {$RepForm->buf_dmax} заказов нет, автоустановка на {$RepForm->d_max}"]]);
            }else{
                $this->session->setFlash('msg_arr', ["checkDateMax"=>["На дату окончания {$RepForm->buf_dmax} заказов нет, автоустановка на {$RepForm->d_max}"]]);
            }
        }

        $this->view->registerMetaTag(['name'=>'keywords', 'content'=>'Emiz']);
        $this->view->registerMetaTag(['name'=>'description', 'content'=>'Эликсир молодости и здоровья']);
        $this->view->title="Отчеты";

        return $this->render('reports.twig', [
            'user'=>$user->getCurrUser($id),
            'RepForm'=>$RepForm,
        ]);
    }
    /**
     * Инструкции
     *
     * @return type
     */
    public function actionHelp( $id )
    {

        $user = new UsersForm;

        $this->view->registerMetaTag(['name'=>'keywords', 'content'=>'Emiz']);
        $this->view->registerMetaTag(['name'=>'description', 'content'=>'Эликсир молодости и здоровья']);
        $this->view->title="Инструкции";

        return $this->render('help.twig', [
            'user'=>$user->getCurrUser($id),
        ]);
    }
    /**
     * Удалить платежную карту
     *
     * @return type
     * @throws NotFoundHttpException
     */
    public function actionDelcard( $id )
    {

        $msg=[];
        $rez=0;
        $model = new CardForm;

        if ( $model->load( Yii::$app->request->post() ) && $model->validate() ){

            $db = Yii::$app->db;
            $transaction = $db->beginTransaction();

            try{

                //CONSTRAINT LUC_CARD_FK FOREIGN KEY (card_id) REFERENCES card(card_id) ON DELETE CASCADE
                $rez = Yii::$app->db->createCommand("DELETE FROM card WHERE card_id=:id; ")->bindValues([
                    ':id' => $model->card_id
                ])->execute();

                $transaction->commit();
                //Если удалено 0 запиисей
                if( $rez === 0 ){
                    $msg['msg'] = 'Карта не найдена! Обратитесть к администратору.';
                    $msg['status'] = 0;
                }else{
                    $msg['msg'] = 'Карта успешно удалена';
                    $msg['status'] = 1;
                }
            }catch(\Exception $e) {

                $msg['msg'] = 'Системная ошибка! Обратитесть к администратору.';
                $msg['status'] = 0;

                $transaction->rollBack();
                throw $e;
            }
        }else {
            if ( $model->hasErrors() ) {
                $msg['msg'] = 'Ошибка приходящих данных!';
                $msg['status'] = 0;
                $msg['err'] = $model->getErrors();
            }
        }

        return $this->render('delcard.twig', [
            'msg' => json_encode($msg),
        ]);
    }
    /**
     * Добавить платежную карту
     *
     * @return type
     * @throws \Exception
     */
    public function actionAddcard( $id )
    {

        $model = new CardForm;
        $rez=0;
        $msg=[];

        if ( $model->load( Yii::$app->request->post() ) && $model->validate() ){

            $db = Yii::$app->db;
            $transaction = $db->beginTransaction();
            try{
                $rez = Yii::$app->db->createCommand(
                    "INSERT INTO card (card_type, card_name, card_num, data_mes, data_god, card_oborot)"
                    . "VALUES(:type, :name, :num, :mes, :god, :obr);"
                    . "SET @last=LAST_INSERT_ID();"
                    . "INSERT INTO link_users_card VALUES(:uid, @last);")->bindValues([
                    ':type' => $model->card_type,
                    ':name' => $model->card_name,
                    ':num' => $model->card_num,
                    ':mes' => $model->data_mes,
                    ':god' => $model->data_god,
                    ':obr' => $model->card_oborot,
                    ':uid' => $this->session['AUTH_DATA']['ID'],
                ])->execute();
                $transaction->commit();

                $msg['msg'] = 'Карта успешно добавлена';
                $msg['status'] = 1;

            }catch(\Exception $e) {

                $msg['msg'] = 'Системная ошибка! Обратитесть к администратору.';
                $msg['status'] = 0;

                $transaction->rollBack();
                throw $e;
            }

        }else {
            if ( $model->hasErrors() ) {
                $msg['msg'] = 'Ошибка приходящих данных!';
                $msg['status'] = 0;
                $msg['err'] = $model->getErrors();
            }
        }

        return $this->render('addcard.twig',
            [
                'msg' => json_encode($msg),
            ]);
    }
    /**
     * Добавить адрес доставки
     *
     * @return type
     * @throws \Exception
     */
    public function actionAddadr( $id )
    {

        $model = new AdressForm;
        $rez=0;
        $msg=[];

        if ( $model->load( Yii::$app->request->post() ) && $model->validate() ){

            $db = Yii::$app->db;
            $transaction = $db->beginTransaction();
            try{

                $rez = Yii::$app->db->createCommand("
                    INSERT INTO adress (city, street, house, korpus, apartament, pochta_index) "
                    . "VALUES(:city, :street, :house, :korpus, :apartament, :pochta_index); "
                    . "SET @last=LAST_INSERT_ID(); "
                    . "INSERT INTO link_users_adress VALUES(:uid, @last); ")->bindValues([
                    ':city' => $model->city,
                    ':street' => $model->street,
                    ':house' => $model->house,
                    ':korpus' => $model->korpus,
                    ':apartament' => $model->apartament,
                    ':pochta_index' => $model->pochta_index,
                    ':uid' => $id,
                ])->execute();
                $transaction->commit();

                $msg['msg'] = 'Адрес успешно добавлен';
                $msg['status'] = 1;

            }catch(\Exception $e) {
                $msg['msg'] = 'Системная ошибка! Обратитесть к администратору.';
                $msg['status'] = 0;
                $transaction->rollBack();
                throw $e;
            }

        }else {
            if ( $model->hasErrors() ) {
                $msg['msg'] = 'Ошибка приходящих данных!';
                $msg['status'] = 0;
                $msg['err'] = $model->getErrors();
            }
        }

        return $this->render('addadr.twig', [
            'msg' => json_encode($msg),
        ]);

    }
    /**
     * Обновить адрес доставки
     *
     * @return type
     * @throws \Exception
     */
    public function actionUpdadr( $id )
    {
        $model = new AdressForm;
        $rez=0;
        $msg=[];

        if ( $model->load( Yii::$app->request->post() ) && $model->validate() ){

            $db = Yii::$app->db;
            $transaction = $db->beginTransaction();
            try{

                $rez = Yii::$app->db->createCommand("UPDATE adress SET city=:city, street=:street, house=:house, korpus=:korpus, apartament=:apartament, pochta_index=:pochta_index"
                    . " WHERE adress_id=:adress_id")->bindValues([
                    ':adress_id' => $model->adress_id,
                    ':city' => $model->city,
                    ':street' => $model->street,
                    ':house' => $model->house,
                    ':korpus' => $model->korpus,
                    ':apartament' => $model->apartament,
                    ':pochta_index' => $model->pochta_index,
                ])->execute();

                $transaction->commit();
                $msg['msg'] = 'Адрес успешно обновлен';
                $msg['status'] = 1;

            }catch(\Exception $e) {
                $msg['msg'] = 'Системная ошибка! Обратитесть к администратору.';
                $msg['status'] = 0;
                $transaction->rollBack();
                throw $e;
            }
        }else {
            if ( $model->hasErrors() ) {
                $msg['msg'] = 'Ошибка приходящих данных!';
                $msg['status'] = 0;
                $msg['err'] = $model->getErrors();
            }
        }

        return $this->render('updadr.twig',
            [
                'msg' => json_encode($msg),
            ]);

    }
    /**
     * Удалить адрес доставки
     *
     * @return type
     * @throws \Exception
     */
    public function actionDeladr( $id )
    {

        $model = new AdressForm;
        $rez=0;
        $msg=[];

        if ( $model->load( Yii::$app->request->post() ) && $model->validate() ){

            $db = Yii::$app->db;
            $transaction = $db->beginTransaction();
            try{

                //CONSTRAINT `LUA_ADR_FK` FOREIGN KEY (`adress_id`) REFERENCES `adress` (`adress_id`) ON DELETE CASCADE
                $rez = Yii::$app->db->createCommand("DELETE FROM adress WHERE adress_id=:adress_id; ")->bindValues([
                    ':adress_id' => $model->adress_id,
                ])->execute();

                $transaction->commit();
                //Если удалено 0 запиисей
                if( $rez === 0 ){
                    $msg['msg'] = 'Адес не найден! Обратитесть к администратору.';
                    $msg['status'] = 0;
                }else{
                    $msg['msg'] = 'Адрес успешно удален';
                    $msg['status'] = 1;
                }

            }catch(\Exception $e) {
                $transaction->rollBack();
                throw $e;
            }
        }else {
            if ( $model->hasErrors() ) {
                $msg['msg'] = 'Системная ошибка! Обратитесть к администратору.';
                $msg['status'] = 0;
                $msg['err'] = $model->getErrors();
            }
        }

        return $this->render('deladr.twig', [
            'msg' => json_encode($msg),
        ]);

    }
    /**
     * Установить активную карту
     *
     * @return type
     * @throws \Exception
     */
    public function actionActivecard( $id )
    {
        $model = new CardForm;
        $rez=0;
        $msg=[];

        if ( $model->load( Yii::$app->request->post() ) && $model->validate() ){

            $db = Yii::$app->db;
            $transaction = $db->beginTransaction();
            try{

                $rez = Yii::$app->db->createCommand(
                    "UPDATE card c LEFT JOIN link_users_card luc ON(luc.card_id=c.card_id) "
                    . "SET c.active=NULL "
                    . "WHERE luc.users_id=:users_id AND c.active=1; "
                    . "UPDATE card c LEFT JOIN link_users_card luc ON(luc.card_id=c.card_id) "
                    . "SET c.active=1 "
                    . "WHERE luc.users_id=:users_id AND c.card_id=:card_id; "

                )->bindValues([
                    ':card_id' => (int)$model->card_id,
                    ':users_id' => $this->session['AUTH_DATA']['ID'],
                ])->execute();
                $transaction->commit();

                $msg['msg'] = 'Карта установлена как основная';
                $msg['status'] = 1;
                //}
            }catch(\Exception $e) {

                $msg['msg'] = 'Системная ошибка! Обратитесть к администратору.';
                $msg['status'] = 0;

                $transaction->rollBack();
                throw $e;
            }
        }else {
            if ( $model->hasErrors() ) {
                $msg['msg'] = 'Ошибка приходящих данных!';
                $msg['status'] = 0;
                $msg['err'] = $model->getErrors();
            }
        }

        return $this->render('activecard.twig', [
            'msg' => json_encode($msg),
        ]);
    }
    /**
     * Установить активный адрес
     *
     * @return type
     * @throws \Exception
     */
    public function actionActiveadress( $id )
    {
        $model = new AdressForm;
        $rez=0;
        $msg=[];

        if ( $model->load( Yii::$app->request->post() ) && $model->validate()){

            $db = Yii::$app->db;
            $transaction = $db->beginTransaction();
            try{

                $rez = Yii::$app->db->createCommand(
                    "UPDATE adress a LEFT JOIN link_users_adress lua ON(lua.adress_id=a.adress_id) "
                    . "SET a.active=NULL "
                    . "WHERE lua.users_id=:users_id AND a.active=1; "
                    . "UPDATE adress a LEFT JOIN link_users_adress lua ON(lua.adress_id=a.adress_id) "
                    . "SET a.active=1 "
                    . "WHERE lua.users_id=:users_id AND a.adress_id=:adress_id; ")->bindValues([
                    ':adress_id' => $model->adress_id,
                    ':users_id' => $this->session['AUTH_DATA']['ID'],
                ])->execute();
                $transaction->commit();

                $msg['msg'] = 'Адрес установлен как основной';
                $msg['status'] = 1;

            }catch(\Exception $e) {

                $msg['msg'] = 'Системная ошибка! Обратитесть к администратору.';
                $msg['status'] = 0;
                $transaction->rollBack();
                throw $e;
            }
        }else {
            if ( $model->hasErrors() ) {
                $msg['msg'] = 'Ошибка приходящих данных!';
                $msg['status'] = 0;
                $msg['err'] = $model->getErrors();
            }
        }

        return $this->render('activecard.twig',
            [
                'msg' => json_encode($msg),
            ]);
    }
    /**
     * Обновить контактный адрес
     *
     * @return type
     * @throws \Exception
     */
    public function actionUpdkont( $id )
    {
        $model = new AdressForm;
        $model_i = new UserinfoForm;
        $rez=0;
        $msg=[];

        if ( $model->load( Yii::$app->request->post() ) && $model->validate() ){
            if ( $model_i->load( Yii::$app->request->post() ) && $model_i->validate() ){

                $db = Yii::$app->db;
                $transaction = $db->beginTransaction();
                try{

                    $rez = Yii::$app->db->createCommand("UPDATE adress SET city=:city, street=:street, house=:house, korpus=:korpus, apartament=:apartament, pochta_index=:pochta_index "
                        . "WHERE adress_id=:adress_id; "
                        . "SELECT id_users_info INTO @idui FROM link_users_users_info WHERE id_users=:uid; "
                        . "UPDATE user_info SET phone=:phone, email=:email WHERE id=@idui; ")->bindValues([
                        ':adress_id' => $model->adress_id,
                        ':city' => $model->city,
                        ':street' => $model->street,
                        ':house' => $model->house,
                        ':korpus' => $model->korpus,
                        ':apartament' => $model->apartament,
                        ':pochta_index' => $model->pochta_index,
                        ':phone' => $model_i->phone,
                        ':email' => $model_i->email,
                        ':uid' => $this->session['AUTH_DATA']['ID'],
                    ])->execute();

                    $this->updateGeoSxCity( $db, $model, $this->session['AUTH_DATA']['ID']);

                    $transaction->commit();
                    $msg['msg'] = 'Контактный адрес успешно обновлен';
                    $msg['status'] = 1;

                }catch(\Exception $e) {
                    $msg['msg'] = 'Системная ошибка! Обратитесть к администратору.';
                    $msg['status'] = 0;
                    $transaction->rollBack();
                    throw $e;
                }

            }else {
                if ( $model->hasErrors() ) {
                    $msg['msg'] = 'Ошибка приходящих данных userinfo!';
                    $msg['status'] = 0;
                    $msg['err'] = $model->getErrors();
                }
            }
        }else {
            if ( $model->hasErrors() ) {
                $msg['msg'] = 'Ошибка приходящих данных адреса!';
                $msg['status'] = 0;
                $msg['err'] = $model->getErrors();
            }
        }


        return $this->render('updkont.twig',
            [
                'msg' => json_encode($msg),
            ]);

    }
    /**
     * Добавить контактный адрес
     *
     * @return type
     * @throws \Exception
     */
    public function actionAddkont( $id )
    {
        $model = new AdressForm;
        $model_i = new UserinfoForm;
        $rez=0;
        $msg=[];

        if ( $model->load( Yii::$app->request->post() ) && $model->validate() ){
            if ( $model_i->load( Yii::$app->request->post() ) && $model_i->validate() ){

                $db = Yii::$app->db;
                $transaction = $db->beginTransaction();
                try{

                    $rez = Yii::$app->db->createCommand(
                        "INSERT INTO adress (city, street, house, korpus, apartament, pochta_index) "
                        . "VALUES(:city, :street, :house, :korpus, :apartament, :pochta_index); "
                        . "SET @last=LAST_INSERT_ID(); "
                        . "INSERT INTO link_users_adress VALUES(:uid, @last); "
                        . "INSERT INTO link_users_adrkont VALUES(:uid, @last); "
                        . "SELECT id_users_info INTO @idui FROM link_users_users_info WHERE id_users=:uid; "
                        . "UPDATE user_info SET phone=:phone, email=:email WHERE id=@idui; "
                    )->bindValues([
                        ':city' => $model->city,
                        ':street' => $model->street,
                        ':house' => $model->house,
                        ':korpus' => $model->korpus,
                        ':apartament' => $model->apartament,
                        ':pochta_index' => $model->pochta_index,
                        ':phone' => $model_i->phone,
                        ':email' => $model_i->email,
                        ':uid' => $this->session['AUTH_DATA']['ID'],
                    ])->execute();

                    $transaction->commit();
                    $msg['msg'] = 'Контактный адрес успешно добавлен';
                    $msg['status'] = 1;

                }catch(\Exception $e) {
                    $msg['msg'] = 'Системная ошибка! Обратитесть к администратору.';
                    $msg['status'] = 0;
                    $transaction->rollBack();
                    throw $e;
                }

            }else {
                if ( $model->hasErrors() ) {
                    $msg['msg'] = 'Ошибка приходящих данных userinfo!';
                    $msg['status'] = 0;
                    $msg['err'] = $model->getErrors();
                }
            }
        }else {
            if ( $model->hasErrors() ) {
                $msg['msg'] = 'Ошибка приходящих данных адреса!';
                $msg['status'] = 0;
                $msg['err'] = $model->getErrors();
            }
        }


        return $this->render('addkont.twig',
            [
                'msg' => json_encode($msg),
            ]);

    }

}
