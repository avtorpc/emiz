<?php

namespace app\controllers;

use Yii;
use app\models\QuestionForm;
/**
 * Description of FaqController
 *
 * @author kkk
 */
class FaqController extends WebAuthController {


    private $action_url_question = '/faq/';

    public function actionIndex( $lang=null ){

        $model = new QuestionForm();
        $err = null;
        $success = null;
        
        $this->view->registerMetaTag(['name'=>'keywords', 'content'=>Yii::t( 'app/faq', '_TEXT_17_' )]);
        $this->view->registerMetaTag(['name'=>'description', 'content'=>Yii::t( 'app/faq', '_TEXT_18_' )]);
        $this->view->title="FAQ";

        if ( Yii::$app->request->isPost) {

            if ( $model->load( Yii::$app->request->post() ) && $model->validate() ) {
             
                if( $model->validateCheck() ){
                    $params = [
                        ':name' => $model->getName(),
                        ':contact' => $model->getContact(),
                        ':question' => $model->getQuestion()
                    ];

                    $rez = Yii::$app->db->createCommand('INSERT INTO `question`( `name`, `contact`, `question`) 
                                                             VALUES (:name, :contact, :question )')->bindValues( $params )->execute();

                    $admin_email=Yii::$app->params['adminEmail'];

                     $admin_email=Yii::$app->params['adminEmail'];
                    Yii::$app->mailer->compose()
                        ->setTo( Yii::$app->params['faq'] )
                        ->setFrom([$admin_email => $admin_email])
                        ->setSubject("Вам поступил вопрос с сайта emiz.ru:" )
                        ->setTextBody(
                                "Имя: " . $model->getName() . "\r\n" .
                                "Вопрос: " . $model->getQuestion() . "\r\n" .
                                "Почта: " . $model->getContact() . "\r\n"
                                )
                        ->send();

                    $success = [ 'success' => [ 0 =>Yii::t( 'app/faq', '_TEXT_15_' )] ];
                } else {

                    $success = [ 'check' => [ 0 =>Yii::t( 'app/faq', '_TEXT_16_' )] ];
                }

            }else {

                if ( $model->hasErrors() ) {
                    $err = $model->getErrors();
                }
            }
        }

        $data_title=[];
        $data_title['path']="FAQ";
        $data_title['body_class']="faq-cotroller page-template page-template-faq page-template-faq-php page page-id-18 class-name theme-Emiz woocommerce-js woocommerce-active";

        return $this->render('index.twig', [
            'MES_ERR' => $err,
            'MES_SUCCESS' => $success,
            'model' => $model,
            'action_url_question' => $this->action_url_question . $lang . '/',
            'data_title' => $data_title,
        ]);

    }

}
