<?php
namespace app\controllers;


use Yii;


trait trait_TranslateProductsByName {

    public function translate_for_name_prod( $arr ) {
        $a = [];
        
        foreach( $arr as $key => $value ){
            switch ($key){
                case 'emiz_premium': $a[$key] = Yii::t( 'app', '_EMIZ_PREMIUM_' ); break;
                case 'emiz_tavr': $a[$key] = Yii::t( 'app', '_EMIZ_TAVRICHESKIY_' ); break;
                case 'pasta': $a[$key] = Yii::t( 'app', '_PASTA_VINOGRADNAY_' ); break;
            }
        }
        
        return $a;
    }
    
                
    public function translate_for_name_prod_for_cart( $arr ){
        $a = [];
  
        foreach( $arr as $key => $value ){
            foreach( $value as $k => $val ){
                if( $k == 'good_uniq_id') {
                    switch ($val){
                        case 'emiz_premium': $a[$val] = Yii::t( 'app', '_EMIZ_PREMIUM_' ); break;
                        case 'emiz_tavr': $a[$val] = Yii::t( 'app', '_EMIZ_TAVRICHESKIY_' ); break;
                        case 'pasta': $a[$val] = Yii::t( 'app', '_PASTA_VINOGRADNAY_' ); break;
                    }
                }
            }
        }
        
        return $a;
    }
}