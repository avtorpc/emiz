<?php
/**
 * Created by PhpStorm.
 * User: avtorpc
 * Date: 2020-03-25
 * Time: 18:43
 */

namespace app\controllers;

use Yii;
use app\models\ProdForm;


class ProdController extends WebAuthController{

    public function actionIndex(){

        $model = new ProdForm();
        $rez = Yii::$app->db->createCommand('SELECT  PROD.id AS prod_id, 
                                                     PROD.name AS prod_name,  
                                                     PROD.price AS prod_price,
                                                     PROD.volume AS prod_volume,
                                                     PROD.uniq_id AS prod_uniq_id,
                                                     PROD.active AS prod_active,
                                                     GOOD.name AS good_name
                                                     FROM `products` AS PROD 
                                             LEFT JOIN `link_goods_products` AS LGP ON PROD.id = LGP.id_products 
                                             LEFT JOIN `goods` AS GOOD ON LGP.id_goods = GOOD.id ORDER BY PROD.id ASC' )->queryAll();
        return $this->render('index.twig',
            [
                'ADMIN_NAME' => $this->session['AUTH_DATA'] ['HOME_PAGE'],
                'DATA' => $rez,
                'MES_ERR' => $this->session->getFlash('err_register'),
                'model' => $model,
                'action_url' => '/prod/edit/'
            ]);
    }


    public function actionEdit( $id = null ){

        $model = new ProdForm;
        $err =  false;
        $success = false;
        $csrf_token_name =  Yii::$app->getRequest()->csrfParam;
        $csrf_token_value = Yii::$app->getRequest()->getCsrfToken();

        $rez = Yii::$app->db->createCommand('SELECT * FROM `goods`' )->queryAll();

        $model->setListGoods( $rez );

        if( is_null( $id ) ) {

            $this->session->setFlash('err_register', 'Позиция товара  не найдена');

            Yii::$app->response->redirect( '/prod/index/' );
        } else {

            $params = [ 'id' => $id ];
            $rez = Yii::$app->db->createCommand('SELECT  PROD.id AS prod_id, 
                                                     PROD.name AS prod_name,  
                                                     PROD.price AS prod_price,
                                                     PROD.volume AS prod_volume,
                                                     PROD.uniq_id AS prod_uniq_id,
                                                     PROD.h AS prod_h,
                                                     PROD.w AS prod_w,
                                                     PROD.n AS prod_n,
                                                     PROD.active AS prod_active,
                                                     GOOD.id AS good_id,
                                                     GOOD.name AS good_name
                                                     FROM `products` AS PROD 
                                                     LEFT JOIN `link_goods_products` AS LGP ON PROD.id = LGP.id_products 
                                                     LEFT JOIN `goods` AS GOOD ON LGP.id_goods = GOOD.id WHERE PROD.id=:id' )->bindValues( $params )->queryOne();

            if ( Yii::$app->request->isPost) {

                if ( $model->load( Yii::$app->request->post() ) && $model->validate() ) {

                    $db = Yii::$app->db;

                    $transaction = $db->beginTransaction();

                    try {

                    $params = $params = $this->getParams( $model, $id );

                     Yii::$app->db->createCommand('UPDATE `products` SET `name`=:name,`price`=:price, `volume`=:volume, `uniq_id`=:uniq_id, `w`=:w, `h`=:h, `n`=:n, `active`=:active WHERE `id`=:id' )
                        ->bindValues( $params )->execute();

                        $rez = Yii::$app->db->createCommand('SELECT  LGP.id as link_goods_products_id
                                                     FROM `products` AS PROD 
                                                     LEFT JOIN `link_goods_products` AS LGP ON PROD.id = LGP.id_products 
                                                     LEFT JOIN `goods` AS GOOD ON LGP.id_goods = GOOD.id WHERE PROD.id=:id' )
                            ->bindValues( [
                            ':id' => ( int ) $id
                        ] )->queryOne();


                        $db->createCommand( 'UPDATE `link_goods_products` SET `id_goods`=:id_goods,`id_products`=:id_products WHERE `id`=:id' )
                            ->bindValues( [
                            ':id_goods' => $model->getGoodsLink(),
                            ':id_products' => ( int ) $id,
                            ':id' => $rez['link_goods_products_id']
                        ] )->execute();

                        $transaction->commit();

                        $success = [ 0 => 'Данные успешно обновлены'];

                    } catch(\Exception $e) {
                        $err = [ 0 => 'Системная ошибка! Обратитесть к администратору.'];
                        $transaction->rollBack();
                        throw $e;
                    }


                    } else {
                    if ( $model->hasErrors() ) {
                        $err = $model->getErrors();
                    }
                }
            } else {
                // Default
                $model->setID( $rez['prod_id'] );
                $model->setName( $rez['prod_name'] );
                $model->setPrice( $rez['prod_price'] );
                $model->setVolume( $rez['prod_volume'] );
                $model->setUniqID( $rez['prod_uniq_id'] );
                $model->setHeight( $rez['prod_h'] );
                $model->setWeigth( $rez['prod_w'] );
                $model->setNetto( $rez['prod_n'] );
                $model->setActive( $rez['prod_active'] );
                $model->setGoodsLink( $rez['good_id'] );
                $model->setGoodsName( $rez['good_name'] );
            }

        }

        return $this->render('add.twig',
            [
                'ADMIN_NAME' => $this->session['AUTH_DATA'] ['HOME_PAGE'],
                'DATA' => $model,
                'MES_ERR' => $err,
                'MES_SUCCESS' => $success,
                'model' => $model,
                'action_url' => '/prod/edit/'. $id . '/'
            ]);
    }

    public function actionAdd(){

        $model = new ProdForm;
        $err =  false;
        $success = false;
        $csrf_token_name =  Yii::$app->getRequest()->csrfParam;
        $csrf_token_value = Yii::$app->getRequest()->getCsrfToken();

        $rez = Yii::$app->db->createCommand('SELECT * FROM `goods`' )->queryAll();

        $model->setListGoods( $rez );

        if ( $model->load( Yii::$app->request->post() ) && $model->validate() ) {

            $db = Yii::$app->db;

            $transaction = $db->beginTransaction();

            try {

                $params = $this->getParams( $model, $id );

                $rez = $db->createCommand('INSERT INTO `products`( `name`, `price`, `volume`, `uniq_id`, `active`) 
                                                             VALUES (:name, :price, :volume, :uniq_id, :h, :w, :n, :active )')->bindValues( $params )->execute();

                $params =[];

                $params = [
                    ':id_goods' => $model->getGoodsLink(),
                    ':id_products' => $db->getLastInsertID()
                ];

                $rez = $db->createCommand( 'INSERT INTO `link_goods_products`( `id_goods`, `id_products`) VALUES (:id_goods, :id_products )' )->bindValues( $params )->execute();

                $transaction->commit();

            } catch(\Exception $e) {
                $transaction->rollBack();
                throw $e;
            }

            $success = [ 'success' => [ 0 =>'Данные успешно сохранены'] ];

        } else {

            if ( $model->hasErrors() ) {
                $err = $model->getErrors();
            }
        }

        return $this->render('add.twig',
            [
                'ADMIN_NAME' => $this->session['AUTH_DATA'] ['HOME_PAGE'],
                'MES_ERR' => $err,
                'MES_SUCCESS' => $success,
                'model' => $model,
                'action_url' => '/prod/add/'
            ]);
    }
    
    private function getParams( $model, $id ) {
       
        return [
                    ':name' => $model->getName(),
                    ':price' => $model->getPrice(),
                    ':volume' => $model->getVolume(),
                    ':uniq_id' => $model->getUniqID(),
                    ':h' => $model->getHeight(),
                    ':w' => $model->getWeigth(),
                    ':n' => $model->getNetto(),
                    ':active' => $model->getActive(),
                    ':id' => $id
                ];
    }

}