<?php


namespace app\controllers;


use Yii;

trait trait_CreateViewBasket {

    public function init_() {
        if( $this->session['AUTH_DATA']["ROLE"] == 'admin' OR $this->session['AUTH_DATA']["ROLE"] == 'dealer'){
            $this->basket_table = 'basket_' . $this->session['AUTH_DATA']["ROLE"] . '_' . $this->session['AUTH_DATA']["ID"];
            $rez = Yii::$app->db->createCommand( 'create or replace view ' . $this->basket_table . ' AS select * from `basket` WHERE `dealer_id` = ' .$this->session['AUTH_DATA']["ID"] )->execute();
        }
    }
}