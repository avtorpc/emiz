<?php
/**
 * Created by PhpStorm.
 * User: avtorpc
 * Date: 16.07.18
 * Time: 18:59
 */

namespace app\controllers;


use models\Models;
use yii\helpers\Url;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\commands\GET_LANG;

class WebAuthController extends Controller {

    use trait_Catch_Err_Controller;
    use trait_Translate;

    public $session = null;
    public $request = null;
    public $layout = false;
    
    public function beforeAction($action) {
        try {

            $this->session = Yii::$app->session;
            $l = new GET_LANG;
         
            $lang_list = $l->get_lang_list();
            
            $this->session->open();
          // print_r(Yii::$app->controller->id ); die;
                if( array_key_exists( Yii::$app->controller->id, Yii::$app->params['lang_control'])  ) {
                
                    if( $lang_list[$_GET['lang']] == $_COOKIE['lang'] AND !is_null($lang_list[$_GET['lang']]) ){

                        Yii::$app->language = $lang_list[$_GET['lang']];
                    } else {

                        if ( isset( $_COOKIE['lang'] ) ) {
                            Yii::$app->language = $_COOKIE['lang'];
                                if ( isset( $_GET['lang'] ) ){
                                   // echo "<pre>"; print_r ( substr( Yii::$app->request->pathInfo,0, -3 ) ); die;
                                    return Yii::$app->response->redirect('/' . substr( Yii::$app->request->pathInfo, 0, -3 ) .  array_search(Yii::$app->language, $lang_list ) . '/' );
                                } else {
                                    return Yii::$app->response->redirect('/' . Yii::$app->request->pathInfo .  array_search(Yii::$app->language, $lang_list ) . '/' );
                                }
                        } else {
                                setcookie("lang", Yii::$app->language, 0, '/');
                                if ( isset( $_GET['lang'] ) ){
                                    return Yii::$app->response->redirect('/' . substr( Yii::$app->request->pathInfo, 0, -3 ) .  array_search(Yii::$app->language, $lang_list ) . '/' );
                                } else {
                                    return Yii::$app->response->redirect('/' . Yii::$app->request->pathInfo .  array_search(Yii::$app->language, $lang_list ) . '/' );
                                }
                        }
                    }
                }
    

            if ( !isset( $this->session['AUTH_DATA'] ) ) {
                $this->session['AUTH_DATA'] = [ 'ROLE' => 'guest',  'HOME_PAGE' => '/' , 'ID' => null ];
            }

                $acl = Yii::$app->params['acl_list'];

                if ( isset( $acl[ $this->session['AUTH_DATA']['ROLE'] ] ) ) {

                    if ( isset( $acl[ $this->session['AUTH_DATA']['ROLE'] ][Yii::$app->controller->id] ) ) {

                        if ( in_array(Yii::$app->controller->action->id, $acl[ $this->session['AUTH_DATA']['ROLE'] ][Yii::$app->controller->id])) {

                            if( isset( Yii::$app->params['action_control_id'][$this->id][$action->id] ) ) {

                                  if( $this->session['AUTH_DATA']['ID'] === Yii::$app->request->get('id') ){

                                    return parent::beforeAction($action);
                                } else {

                                     die( '4' );
                                }

                            } else {

                                return parent::beforeAction($action);
                            }

                        } else {
                            $this->session->set("Message", 'Доступ к action закрыт');
                            $this->session->set("Code", 403);
                            Yii::$app->response->redirect(Url::to( '/login/'));
                        }
                    } else {
                        die( '2');
                        $this->session->set("Message", 'Доступ к controller закрыт');
                        $this->session->set("Code", 403);
                        Yii::$app->response->redirect(Url::to( '/site/fault/'));
                    }
                } else {
                    die( '3');
                    $this->session->set("Message", 'Роли пользователя не существует');
                    $this->session->set("Code", 403);
                    Yii::$app->response->redirect(Url::to( '/site/fault/'));
                }


        } catch (\Exception $ex) {

            return $this->err($ex);
        }

    }


    public function setLang() {

        if ($this->session->has('language')) {
            $lang = explode('-', $this->session->get('language'));
            if ($lang[0] == 'de') {
                $lang[0] = 'en';
            }

            Models::callProc("SET_LANG",
                [
                    "in_lang" => strtolower($lang[0])
                ],
                []
            );

        } else {
            throw new \Exception(Yii::t('app', 'Err lang'), 200);
        }

    }


    public function __destruct( ) {

    }

}