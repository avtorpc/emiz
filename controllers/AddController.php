<?php
/**
 * Created by PhpStorm.
 * User: avtorpc
 * Date: 2020-03-21
 * Time: 22:03
 */

namespace app\controllers;

use Yii;
use app\models\GoodsForm;

class AddController extends WebAuthController{

    public function actionIndex(){

        $model = new GoodsForm();
        $rez = Yii::$app->db->createCommand('SELECT * FROM `goods`' )->queryAll();

        return $this->render('index.twig',
            [
                'ADMIN_NAME' => $this->session['AUTH_DATA'] ['HOME_PAGE'],
                'DATA' => $rez,
                'MES_ERR' => $this->session->getFlash('err_register'),
                'model' => $model,
                'action_url' => '/add/edit/'
            ]);
    }


    public function actionEdit( $id = null ){

        $err =  false;
        $success = false;
        $csrf_token_name =  Yii::$app->getRequest()->csrfParam;
        $csrf_token_value = Yii::$app->getRequest()->getCsrfToken();

        if( is_null( $id ) ) {

            $this->session->setFlash('err_register', 'Позиция товара  не найдена');

            Yii::$app->response->redirect( '/admin/index/' );
        } else {

            $params = [ 'id' => $id ];
            $rez = Yii::$app->db->createCommand('SELECT * FROM `goods` WHERE id=:id' )->bindValues( $params )->queryOne();

            $model = new GoodsForm;
            if ( Yii::$app->request->isPost) {

                if ( $model->load( Yii::$app->request->post() ) && $model->validate() ) {
                    $params = [
                        ':name' => $model->getName(),
                        ':short_desc' => $model->getShortDesc(),
                        ':long_desc' => $model->getLongDesc(),
                        ':notes' => $model->getNotes(),
                        ':sostav' => $model->getSostav(),
                        ':uniq_id' => $model->getUniqID(),
                        'active' => $model->getActive(),
                        'id' => $id
                    ];

                    $rez = Yii::$app->db->createCommand('UPDATE `goods` SET `name`=:name,`short_desc`=:short_desc,`long_desc`=:long_desc,`notes`=:notes,`sostav`=:sostav, `uniq_id`=:uniq_id, `active`=:active WHERE `id`=:id' )
                                     ->bindValues( $params )->execute();

                    if( $rez ) {

                        $success = [ 0 => 'Данные успешно обновлены'];
                    } else {

                        $err = [ 0 => 'Системная ошибка! Обратитесть к администратору.'];
                    }

                } else {
                    if ( $model->hasErrors() ) {
                        $err = $model->getErrors();
                    }
            }
            } else {
                // Default
                $model->setID( $rez['id'] );
                $model->setLongDesc( $rez['long_desc'] );
                $model->setName( $rez['name'] );
                $model->setNotes( $rez['notes'] );
                $model->setShortDesc( $rez['short_desc'] );
                $model->setUniqID( $rez['uniq_id'] );
                $model->setSostav( $rez['sostav'] );
                $model->setActive( $rez['active'] );
            }

        }

        return $this->render('products.twig',
            [
                'ADMIN_NAME' => $this->session['AUTH_DATA'] ['HOME_PAGE'],
                'DATA' => $model,
                'MES_ERR' => $err,
                'MES_SUCCESS' => $success,
                'csrf_token_name' => $csrf_token_name,
                'csrf_token_value' => $csrf_token_value,
                'model' => $model,
                'action_url' => '/add/edit/'. $id . '/'
            ]);
    }


    public function actionProducts(){

        $model = new GoodsForm;
        $err =  false;
        $success = false;
        $csrf_token_name =  Yii::$app->getRequest()->csrfParam;
        $csrf_token_value = Yii::$app->getRequest()->getCsrfToken();

        if ( $model->load( Yii::$app->request->post() ) && $model->validate() ) {
            $params = [
                   ':name' => $model->getName(),
                   ':short_desc' => $model->getShortDesc(),
                   ':long_desc' => $model->getLongDesc(),
                   ':notes' => $model->getNotes(),
                   ':sostav' => $model->getSostav(),
                   ':uniq_id' => $model->getUniqID(),
                   'active' => $model->getActive()
            ];

            $rez = Yii::$app->db->createCommand('INSERT INTO `goods`(`name`, `short_desc`, `long_desc`, `notes`, `sostav`,  `uniq_id`, `active`)
                                                             VALUES (:name, :short_desc, :long_desc, :notes, :sostav, :uniq_id, :active )')->bindValues( $params )->execute();
            /* приходит ===NULL, по логике если вставлено 0 строк то ошибка */
            if( $rez ) {

                $success = [ 0 => 'Данные успешно сохранены'];
            } else {

                $err = [ 0 => 'Системная ошибка! Обратитесть к администратору.'];
            }
        } else {
            if ( $model->hasErrors() ) {
                $err = $model->getErrors();
            }
        }

        return $this->render('products.twig',
            [
                'ADMIN_NAME' => $this->session['AUTH_DATA'] ['HOME_PAGE'],
                'csrf_token_name' => $csrf_token_name,
                'csrf_token_value' => $csrf_token_value,
                'MES_ERR' => $err,
                'MES_SUCCESS' => $success,
                'model' => $model,
                'action_url' => '/add/products/'
            ]);
    }
}