<?php

namespace app\controllers;


use Yii;

trait trait_AddGeoSXtoBASE {

    public function detectCity( $db ){
        $geo_data_default = [ 'city' => [
            'id'=>'',
            'lat' => '',
            'lon' => '',
            'name_ru' => '',
            'name_en' => ''
        ],
            'region' => [
                'name_ru' => '',
                'iso' => '',
                'name_en' => ''
            ],
            'country' => [
                'name_ru' => '',
                'name_en' => '',
               'iso' => '',
            ]
        ];

        include(__DIR__ . "/geo/SxGeo.php");

        $SxGeo = new \SxGeo(__DIR__ . '/geo/SxGeoCity.dat');

        $vr = $SxGeo->getCityFull( $_SERVER['REMOTE_ADDR'] );

        if(!$vr){
            $geo_data=$geo_data_default;
        }
        else{
            $geo_data = array_replace_recursive($geo_data_default, $vr );
        } 
        
        $rez = $db->createCommand ( 'SELECT  `city_rus` FROM `geo_city` WHERE lat_city=:lat AND lon_city=:lon')->
        bindValues([ 
            ':lat'=>$geo_data['city']['lat'],
            ':lon'=>$geo_data['region']['lon']
            ] )->queryOne();

        if( !$rez ){

            $db->createCommand('INSERT INTO `geo_city`( `city_rus`, `name_city`, `lat_city`, `lon_city`, `name_region`, `iso_region`, `name_country`, `iso_country`) 
                                    VALUES (:city_rus, :name_city, :lat_city, :lon_city, :name_region, :iso_region, :name_country, :iso_country )')->
            bindValues( [
                ':city_rus' =>  mb_convert_encoding($geo_data['city']['name_ru'], "UTF-8", "CP1251"),
                ':name_city' =>  $geo_data['city']['name_en'],
                ':lat_city' =>  $geo_data['city']['lat'],
                ':lon_city' =>  $geo_data['city']['lon'],
                ':name_region' =>  $geo_data['region']['name_en'],
                ':iso_region' =>  $geo_data['region']['iso'],
                ':name_country' =>  $geo_data['country']['name_en'],
                ':iso_country' =>  $geo_data['country']['iso']
            ]) -> execute();
        }

        return $geo_data;
    }


    public function updateGeoSxCity( $db, $model, $dealer_id ){

        try{
            
                $transaction = $db->beginTransaction();
               
                $rez = Yii::$app->db->createCommand( 'SELECT * FROM `geo_city` WHERE city_rus=:city_rus OR name_city=:city_rus' )->bindValues( [':city_rus'=> $model->city])->queryOne();

                if( !$rez ) {
                    $str_sql = 'INSERT INTO `geo_city`( `city_rus`, `name_city`, `lat_city`, `lon_city`, `name_region`, `iso_region`, `name_country`, `iso_country`) 
                                            VALUES (:city_rus, :name_city, :lat_city, :lon_city, :name_region, :iso_region, :name_country, :iso_country )';

                    $db->createCommand($str_sql )->
                    bindValues( [
                        ':city_rus' =>  $model->city,
                        ':name_city' =>  null,
                        ':lat_city' => null,
                        ':lon_city' => null,
                        ':name_region' =>  null,
                        ':iso_region' => null,
                        ':name_country' => null,
                        ':iso_country' => null
                    ]) -> execute();

                    $id = $db->getLastInsertID();

               } else {
                   
                    $id = $rez['id'];
                }
                  $db->createCommand('DELETE FROM `link_user_geo_city` WHERE id_user=:id_user' )->bindValues([
                       'id_user'=>  (int)$this->session['AUTH_DATA']['ID'] 
                          ] )->execute();
                  
                    $db->createCommand('INSERT INTO `link_user_geo_city`( `id_user`, `id_city`) VALUES ( :id_user, :id_city)')
                        ->bindValues([
                            'id_user'=>  (int) $dealer_id,
                            'id_city' => (int) $id ])->execute();
                    
                     $transaction->commit();

        } catch (Exception $ex) {
                        $transaction->rollBack();
                        die( 'Err updateGeoSxCity');
        }

    }

}