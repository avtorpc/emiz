<?php
/**
 * Created by PhpStorm.
 * User: avtorpc
 * Date: 2020-04-08
 * Time: 22:33
 */

namespace app\controllers;

use Yii;
class ModerPartnerController extends WebAuthController {

    public function actionIndex() {
        $rez = Yii::$app->db->createCommand('
                     SELECT * FROM `parthers_add`  ORDER BY dt_add ASC')->queryAll();

        return $this->render('index.twig',
            [
                'DATA' => $rez
            ]);
    }

}