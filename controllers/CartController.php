<?php

namespace app\controllers;

use app\models\SpeedBasketForm;
use Yii;
use app\models\BasketForm;
use app\models\UsersForm;
use app\models\CardForm;
use app\models\Products;
use yii\helpers\Url;
use YandexCheckout\Client;

/**
 * Description of CartController
 *
 * @author kkk
 */
class CartController extends WebAuthController {

    use trait_AddGeoSXtoBASE;
    use trait_DetectCity;
     use trait_TranslateProductsByName;

    public $dealer_id = null;
    public $user_id = null;
    private $list_of_town = [];
    
    public function init(){
        $this->list_of_town = [
                    '_1_' => 'Москва',
                    '_2_' => 'Казань',
                    '_3_' => 'Уфа',
                    '_4_' => 'Самара',
                    '_5_' => 'Оренбург',
                    '_6_' => 'Ульяновск',
                    '_7_' => 'Пенза',
                    '_8_' => 'Набережные Челны',
                    '_9_' => 'Нижнекамск',
                    '_10_' => 'Альметьевск',
                    '_11_' => 'Елабуга',
                    '_12_' => 'Зеленодольск',
                    '_13_' => 'Нальчик',
                    '_14_' => 'Грозный',
                    '_15_' => 'Азнакаево',
                    '_16_' => 'Кукмор',
                    '_17_' => 'Арск',
                    '_18_' => 'Балтаси',
                    '_19_' => 'Чистополь'
        ];
        
    }

    public function actionIndex( $lang = null ){
        
        foreach ( $this->list_of_town as $key=>$value  ){
            $this->list_of_town[$key] = Yii::t( 'app/basket', $key );
        }
        
        $csrf_token_name = Yii::$app->getRequest()->csrfParam;
        $csrf_token_value = Yii::$app->getRequest()->getCsrfToken();

        $dealer = new UsersForm;
        $card = new CardForm;
        $m = new Products();

        if(isset($_GET['id_basket'])){
            $this->session->setFlash('success', 'Ваш заказ сохранен за номером - ' . $_GET['id_basket'] );
            setcookie("basket", "", time() - 3600, "/");
            return Yii::$app->response->redirect('/cart/');
        }

        $this->view->registerMetaTag(['name' => 'keywords', 'content' => 'Emiz']);
        $this->view->registerMetaTag(['name' => 'description', 'content' => 'Эликсир молодости и здоровья']);
        $this->view->title = "Корзина";

        $data_title = [];
        $data_title['path'] = "cart";
        $data_title['body_class'] = "cart-controller page-template page-template-cart-checkout page-template-cart-checkout-php page page-id-119 class-name theme-Emiz woocommerce-cart woocommerce-page woocommerce-js woocommerce-active";

        $premium = $m->getProd('emiz_premium');
        $tavr = $m->getProd('emiz_tavr');
        $pasta = $m->getProd('pasta');
     
        $prod_sizes = [];
        
        foreach ( $premium as $key=>$value ){
           $prod_sizes[$value['prod_uniq_id']]['h'] = $value['prod_h'];
           $prod_sizes[$value['prod_uniq_id']]['w'] = $value['prod_w'];
           $prod_sizes[$value['prod_uniq_id']]['n'] = $value['prod_n']; 
        }
        foreach ( $tavr as $key=>$value ){
           $prod_sizes[$value['prod_uniq_id']]['h'] = $value['prod_h'];
           $prod_sizes[$value['prod_uniq_id']]['w'] = $value['prod_w'];
           $prod_sizes[$value['prod_uniq_id']]['n'] = $value['prod_n']; 
        }
        foreach ( $pasta as $key=>$value ){
           $prod_sizes[$value['prod_uniq_id']]['h'] = $value['prod_h'];
           $prod_sizes[$value['prod_uniq_id']]['w'] = $value['prod_w'];
           $prod_sizes[$value['prod_uniq_id']]['n'] = $value['prod_n']; 
        }
      

return $this->render('index.twig', [
            'data_title' => $data_title,
            'premium' => $premium,
            'tavr' => $tavr,
            'pasta' => $pasta,
            'premium_translate' => $this->translate_for_name_prod_for_cart( $premium ),
            'tavr_translate' => $this->translate_for_name_prod_for_cart( $tavr ),
            'pasta_translate' => $this->translate_for_name_prod_for_cart( $pasta ),
            'dealer' => $dealer->getCurrDealer($this->session['AUTH_DATA']['ID']),
            'card' => $card->getActiveCard($this->session['AUTH_DATA']['ID']),
            'prod_tree' => $m->getProdTree(),
            'good_tree' => $m->getGoodTree(),
            'prod_sizes' => $prod_sizes,
            'csrf_token_name' => $csrf_token_name,
            'csrf_token_value' => $csrf_token_value,
            'MES_ERR' => $this->session->getFlash('err_register'),
            'MES_SUCCESS' => $this->session->getFlash('success'),
            'list_of_town' => $this->list_of_town,
            'url_for_basket' => '/cart/add/' . $lang .'/'
        ]);
    }

    public function actionAdd()
    {

        $client = new Client();
        $client->setAuth(Yii::$app->params['shopId'], Yii::$app->params['secretKey']);

        $model = new BasketForm;
        $err = false;
        $success = false;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $db = Yii::$app->db;

            $geo_data = $this->detectCity($db);

            if (isset($_COOKIE['basket'])) {
                $basket = json_decode($_COOKIE['basket'], true);
                $eq = ['pasta', 'emiz_tavr', 'emiz_premium'];
                $control_count = 0;
                foreach ($basket as $key => $value) {
                    if (!in_array($key, $eq)) {
                        $control_count = $control_count + $value;
                    }
                }
                if ($control_count > 0) {

                    try {

                        $transaction = $db->beginTransaction();

                        $this->select_user_dealer($db, $model, $geo_data);

                        $params = [
                            ':name' => $model->getName(),
                            ':surname' => $model->getSurname(),
                            ':middle_name' => $model->getMiddleName(),
                            ':index' => $model->getIndex(),
                            ':city' => $model->getCity(),
                            ':address' => $model->getAddress(),
                            ':home' => $model->getHome(),
                            ':corpus' => $model->getCorpus(),
                            ':flat' => $model->getFlat(),
                            ':phone' => $model->getPhone(),
                            ':email' => $model->getEmail(),
                            ':transport' => $model->getTransport(),
                            ':type_transport' => $model->getTypeTransport(),
                            ':pay_type' => $model->getPaymentType(),
                            ':card_number' => $model->getCardNumber(),
                            ':card_date1' => $model->getCardDate1(),
                            ':card_date2' => $model->getCardDate2(),
                            ':card_numberback' => $model->getCardNumberback(),
                            ':date_close' => null,
                            ':user_id' => $this->user_id,
                            ':dealer_id' => $this->dealer_id['id'],
                            ':remember' => $model->getRemember(),
                            ':yandex_id' => null,
                            ':active' => 1
                        ];

                        $rez = $db->createCommand('INSERT INTO `basket`( `name`, `surname`, `middle_name`, `index`, `city`, `address`, `home`, `corpus`, `flat`, `phone`, `email`, `transport`, `type_transport`, `pay_type`,
                                       `card_number`, `card_date1`, `card_date2`, `card_numberback`, `date_close`, `user_id`, `dealer_id`, `remember`, `yandex_id`, `active`)
                                                        VALUES ( :name, :surname, :middle_name, :index, :city, :address, :home, :corpus, :flat, :phone, :email, :transport, :type_transport,
                                                            :pay_type, :card_number, :card_date1, :card_date2, :card_numberback, :date_close, :user_id, :dealer_id, :remember, :yandex_id, :active )
                                                            ')->bindValues($params)->execute();

                        $id_basket = $db->getLastInsertID();

                        $select_prod = $db->createCommand('SELECT * FROM `products` WHERE uniq_id=:uniq_id');
                        $insert_item_to_basket = $db->createCommand('INSERT INTO `basket_items`( `name`, `cnt`, `unit_price`, `volume`, `prod_id`, `uniq_id`) VALUES ( :name, :cnt, :unit_price, :volume, :prod_id, :uniq_id)');
                        $insert_to_link_table = $db->createCommand('INSERT INTO `link_basket_basket_items`( `id_basket`, `id_basket_item`) VALUES ( :id_basket, :id_basket_item )');

                        $list_prod = $this->getProductsByUniq();
                        $str_prod = '';
                        $summ = 0;
                        
                        
                        foreach ($basket as $key => $value) {
                            if (!in_array($key, $eq)) {
                                $rez = $select_prod->bindValues([':uniq_id' => $key])->queryOne();

                                $insert_item_to_basket->bindValues([
                                    ':name' => $rez['name'],
                                    ':cnt' => (int)$value,
                                    ':unit_price' => $rez['price'],
                                    ':volume' => $rez['volume'],
                                    ':prod_id' => $rez['id'],
                                    ':uniq_id' => $rez['uniq_id']
                                ])->execute();

                                $insert_to_link_table->bindValues([
                                    ':id_basket' => $id_basket,
                                    ':id_basket_item' => $db->getLastInsertID()
                                ])->execute();
                                
                               
                                $str_prod = $str_prod . "\r\n" . $list_prod[$rez['uniq_id']]['name'] . ' x ' . (int)$value;
                                $summ = $summ + (int)$list_prod[$rez['uniq_id']]['price'] * $value;
                            }
                        }

                        $transaction->commit();

                        $admin_email=Yii::$app->params['adminEmail'];

                        $body_mail = "Вам поступил заказ № " .
                            $id_basket . "\r\n" .
                            'Имя: ' . $model->getName() . ' ' . $model->getMiddleName() . ' ' . $model->getSurname(). "\r\n" .
                            'Телефон: ' . $model->getPhone() . "\r\n" .
                            'Адрес: ' . $model->getAddress(). ' ' . $model->getCity(). ' ' . $model->getHome(). ' ' . $model->getCorpus() . ' ' . $model->getFlat() . "\r\n" .
                            'Позиции: ' . $str_prod . "\r\n" .
                            'Сумма заказа: ' . $summ .' руб.' ;


                        Yii::$app->mailer->compose()
                            ->setTo( Yii::$app->params['sales'] )
                            ->setFrom([$admin_email => $admin_email])
                            ->setSubject("EMIZ #" . $id_basket )
                            ->setTextBody( $body_mail )
                            ->send();

                        Yii::$app->telegram->botUsername = Yii::$app->params['telegram_bot'];

                        Yii::$app->telegram->sendMessage([
                            'chat_id' => Yii::$app->params['info_telegramm'],
                            'text' => $body_mail,
                        ]);

                        if ( $model->getPaymentType() == 'card-pay'){

                            $client = new Client();
                            $client->setAuth(Yii::$app->params['shopId'], Yii::$app->params['secretKey']);
                            $idempotenceKey = uniqid('', true);
                            $response = $client->createPayment(
                                array(
                                    'amount' => array(
                                        'value' => $summ,
                                        'currency' => 'RUB',
                                    ),
                                    'payment_method_data' => array(
                                        'type' => 'bank_card',
                                    ),
                                    'confirmation' => array(
                                        'type' => 'redirect',
                                        'return_url' => 'http://www.emiz.ru/cart/?id_basket=' . $id_basket ,
                                    ),
                                    'description' => 'Заказ №' . $id_basket,
                                ),
                                $idempotenceKey
                            );

                            //get confirmation url

                            $confirmationUrl = $response->getConfirmation()->getConfirmationUrl();

                            $rez = $db->createCommand( "UPDATE `basket` SET `yandex_id`=' . $response->id  . ' WHERE `id` = '". $id_basket ."'" )->execute();

                           // Yii::error( print_r($response->id , 1) , 'oracle_fail');

                            return   Yii::$app->response->redirect($confirmationUrl);

                        } else {

                            $this->session->setFlash('success', Yii::t('app/basket', '_ACCESS_ORDER_') . ' #' . $id_basket );

                            setcookie("basket", "", time() - 3600, "/");
                            return Yii::$app->response->redirect('/cart/');
                        }


                    } catch (\Exception $e) {
                        $transaction->rollBack();
                        Yii::error( print_r($e , 1) , 'oracle_fail');
                        $this->session->setFlash('err_register', Yii::t('app/basket', '_ERR_SAVE_BASKET_') );
                       return Yii::$app->response->redirect('/cart/');
                    }

                } else {
                    $this->session->setFlash('err_register', Yii::t('app/basket', '_NO_PROD_IN_BASKET_') );
                   return Yii::$app->response->redirect('/cart/');
                }

            } else {
                $this->session->setFlash('err_register', Yii::t('app/basket', '_NO_BASKET_') );
                return Yii::$app->response->redirect('/cart/');
            }
        }
    }


    public function actionAddBasketFromMainPage(){
        if (Yii::$app->request->isAjax) {

            $model = new SpeedBasketForm;
            $err = false;
            $success = false;

            if ($model->load(Yii::$app->request->post()) && $model->validate()) {

                $db = Yii::$app->db;

                $geo_data = $this->detectCity($db);

                $this->select_user_dealer($db, $model, $geo_data);

                try {

                    $transaction = $db->beginTransaction();
                    $params = [
                        ':name' => $model->getName(),
                        ':surname' => null,
                        ':middle_name' => null,
                        ':index' => null,
                        ':city' => $geo_data['city']['name_en'],
                        ':address' => null,
                        ':home' => null,
                        ':corpus' => null,
                        ':flat' => null,
                        ':phone' => $model->getPhone(),
                        ':email' => null,
                        ':transport' => null,
                        ':type_transport' => null,
                        ':pay_type' => null,
                        ':card_number' => null,
                        ':card_date1' => null,
                        ':card_date2' => null,
                        ':card_numberback' => null,
                        ':date_close' => null,
                        ':user_id' => $this->user_id,
                        'dealer_id' => $this->dealer_id['id'],
                        ':remember' => $model->getCheck(),
                        ':active' => 1
                    ];

                    $rez = $db->createCommand('INSERT INTO `basket`( `name`, `surname`, `middle_name`, `index`, `city`, `address`, `home`, `corpus`, `flat`, `phone`, `email`, `transport`, `type_transport`, `pay_type`,
                                       `card_number`, `card_date1`, `card_date2`, `card_numberback`, `date_close`, `user_id`, `dealer_id`, `remember`, `active`)
                                                        VALUES ( :name, :surname, :middle_name, :index, :city, :address, :home, :corpus, :flat, :phone, :email, :transport, :type_transport,
                                                            :pay_type, :card_number, :card_date1, :card_date2, :card_numberback, :date_close, :user_id, :dealer_id, :remember, :active )
                                                            ')->bindValues($params)->execute();

                    $id_basket = $db->getLastInsertID();

                    $transaction->commit();

                    $admin_email=Yii::$app->params['adminEmail'];

                    Yii::$app->mailer->compose()
                        ->setTo( Yii::$app->params['sales'] )
                        ->setFrom( [$admin_email => $admin_email] )
                        ->setSubject("EMIZ #" . $id_basket )
                        ->setTextBody("Вам поступил быстрый заказ № " . $id_basket . " от " . $model->getName() .", номер телефона: " . $model->getPhone() )
                        ->send();


                    $msg['msg'] = Yii::t('app/basket', '_ACCESS_ORDER_') . ' #' . $id_basket;
                    $msg['status'] = 1;

                } catch (\Exception $e) {
                    $transaction->rollBack();
                    $msg['msg'] = Yii::t('app/basket', '_ERR_SAVE_ORDER_') ;
                    $msg['status'] = 0;
                }
            } else {

                $msg['msg'] = Yii::t('app/basket', '_ALL_FIELDS_') ;
                $msg['status'] = 0;
            }


            return $this->render('/layouts/ajax_msg.twig', [
                'msg' => json_encode($msg),
            ]);
        }

    }
    
    
    public function getProductsByUniq(){

        $arr=[];$arr=Yii::$app->db->createCommand("SELECT CONCAT(name, ' ', volume, ' L')AS`name`, uniq_id, price FROM products")->queryAll();
        $rez=[];
        foreach($arr as $k=>$v){
            $rez[$v['uniq_id']]['name']=$v['name'];
            $rez[$v['uniq_id']]['price']=$v['price'];
        }
        
        return $rez;
    }
    
}
