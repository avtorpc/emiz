<?php
/**
 * Created by PhpStorm.
 * User: avtorpc
 * Date: 2020-04-06
 * Time: 20:08
 */

namespace app\controllers;

use Yii;
use yii\helpers\Url;
use app\models\ModerBasketForm;


class ModerGeoController extends WebAuthController {

    public function actionIndex(){

        $rez = Yii::$app->db->createCommand('
                      SELECT 
                       BSK.id AS basket_id, 
                       BSK.name AS basket_name, 
                       BSK.surname AS basket_surname, 
                       BSK.middle_name AS basket_middle_name, 
                       BSK.index AS basket_index, 
                       BSK.city AS basket_city, 
                       BSK.address AS basket_address, 
                       BSK.home AS basket_home, 
                       BSK.corpus AS basket_corpus, 
                       BSK.flat AS basket_flat, 
                       BSK.phone AS basket_phone, 
                       BSK.email AS basket_email, 
                       BSK.transport AS basket_transport, 
                       BSK.pay_type AS basket_pay_type, 
                       BSK.card_number AS basket_card_number, 
                       BSK.card_date1 AS basket_card_date1, 
                       BSK.card_date2 AS basket_card_date2, 
                       BSK.card_numberback AS basket_card_numberback, 
                       BSK.date_in AS basket_date_in, 
                       BSK.date_close AS basket_date_close, 
                       BSK.user_id AS basket_user_id, 
                       BSK.remember AS basket_remember,
                       BSI.id AS item_id,
                       BSI.name AS item_name, 
                       BSI.cnt AS item_cnt,  
                       BSI.unit_price AS item_unit_price, 
                       BSI.volume AS item_volume, 
                       BSI.uniq_id AS item_uniq_id
                       FROM basket AS BSK
                      LEFT JOIN link_basket_basket_items AS LBI ON LBI.id_basket = BSK.id
                      LEFT JOIN basket_items AS BSI ON BSI.id = LBI.id_basket_item
                      ORDER BY BSK.date_in ASC' )->queryAll();
        $baskets = [];
        $baskets_items = [];
        foreach( $rez as $key=>$value ){
            foreach ( $value as $k=>$val) {
                if(stristr($k, 'basket_') === false ) {
                    $baskets_items[$value['basket_id']][$value['item_id']][$k] = $val;
                } else {
                    $baskets[$value['basket_id']][$k] = $val;
                    $baskets_items[$value['basket_id']][$value['item_id']]['final_sum'] = $val;
                }
            }
        }


        // Считаем сумму заказа
        foreach( $baskets_items as $key=>$value ){
            $s = 0;
            $с = 0;
             foreach( $value as $k=>$val ){
                 $s = $s + $val['item_cnt'] * $val['item_unit_price'];
                 $с = $с + $val['item_cnt'];
             }
            $baskets[$key]['final_sum'] = $s;
            $baskets[$key]['final_cnt'] = $с;
        }

        return $this->render('index.twig',
            [
                'ADMIN_NAME' => $this->session['AUTH_DATA'] ['HOME_PAGE'],
                'BASKETS' => $baskets,
                'BASKETS_ITEMS' => $baskets_items,
                'MES_ERR' => $this->session->getFlash('err_register'),
                'action_url' => '/moder-basket/edit/'
            ]);

    }



    public function actionEdit( $id = null ){

        $model = new ModerBasketForm();

        if ( $model->load( Yii::$app->request->post() ) && $model->validate() ) {

/*
 *
 * item_name
 * item_sum
 * item_count
 * item_price
 * goods_link
 */
            $item_name = Yii::$app->request->post( 'item_name');
            $item_count = Yii::$app->request->post( 'item_count');
            $item_price = Yii::$app->request->post( 'item_price');
            $goods_link = Yii::$app->request->post( 'goods_link');

            if( count( $item_name) == 0 ) {
                $this->session->setFlash('err', 'Товары не указаны');
                Yii::$app->response->redirect( Url::to( '/moder-basket/edit/' . $id . '/' ) );
            }

            $model->saveBasket($id, $item_name, $item_count, $item_price, $goods_link );

            $this->session->setFlash('success', 'Данные успешно обновлены');
            Yii::$app->response->redirect( Url::to( '/moder-basket/edit/' . $id . '/' ) );

        } else {

                if ( $model->hasErrors() ) {
                    $this->session->setFlash('err', serialize( $model->getErrors() ));
                }

            $rez = $model->getBasket( $id );

            //    echo "<pre>"; print_r( $rez ); die;

            $basket = [];
            $basket_items = [];

            foreach( $rez as $key=>$value ){
                foreach ( $value as $k=>$val) {
                    if(stristr($k, 'basket_') === false ) {
                        $basket_items[$value['item_id']][$k] = $val;
                    } else {
                        $basket[$k] = $val;
                    }
                }
            }

            foreach (  $basket_items as $key=>$value  ){

                $basket_items[$key]['item_sum'] = $value['item_unit_price'] * $value['item_cnt'];
            }

            $s = 0;
            $с = 0;
            foreach( $basket_items as $key=>$val ){

                $s = $s + $val['item_cnt'] * $val['item_unit_price'];
                $с = $с + $val['item_cnt'];

            }

            $basket['final_sum'] = $s;
            $basket['final_cnt'] = $с;

            $model->setDATA( $basket );

         //  echo "<pre>"; print_r( $basket_items ); die;

            $list_of_goods = Yii::$app->db->createCommand('SELECT id, name FROM `goods`' )->queryAll();
            $list_of_products = Yii::$app->db->createCommand('SELECT id, name, volume FROM `products`' )->queryAll();

           // echo "<pre>"; print_r( $list_of_goods ); die;

            return $this->render('edit.twig',
                [
                    'BASKET' => $basket,
                    'BASKET_ITEMS' => $basket_items,
                    'MES_ERR' => unserialize( $this->session->getFlash('err') ),
                    'MES_SUCCESS' => $this->session->getFlash('success'),
                    'action_url' => '/moder-basket/edit/' . $id . '/',
                    'model' => $model,
                    'goods' => $list_of_goods,
                    'prod'  => $list_of_products
                ]);
        }

    }

}