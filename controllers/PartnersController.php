<?php

namespace app\controllers;

use Yii;
use app\models\QuestionForm;
use app\models\AddPartherForm;

class PartnersController extends WebAuthController {

    private $action_url_question = '/partners/';
    private $action_url_partners = '/partners/';
    private $model_Question = null;
    private $model_Add = null;
    private $err = null;
    private $success = null;

    /**
     * Страница Партнеры
     * 
     * @return type
     */
    public function actionIndex( $lang=null )
    {
        $this->view->registerMetaTag(['name'=>'keywords', 'content'=>Yii::t('app/partners', '_TEXT_26_')]);
        $this->view->registerMetaTag(['name'=>'description', 'content'=>Yii::t('app/partners', '_TEXT_27_')]);
        $this->view->title=Yii::t('app/partners', '_TEXT_28_');

        $data_title=[];
        $data_title['path']="partners";
        $data_title['body_class']="page-template page-template-partners page-template-partners-php page page-id-14 class-name theme-Emiz woocommerce-js woocommerce-active";
      
        $this->model_Question = new QuestionForm();
        $this->model_Add = new AddPartherForm();
        
        if( Yii::$app->request->isPost ){
            if ( Yii::$app->request->post('form-type') == 'actionQuestion' ) {
              $this->model_Question =  $this->actionQuestion ( $this->model_Question );
              
            }
            
            if ( Yii::$app->request->post('form-type') == 'actionAdd' ) {
                $this->model_Add =  $this->actionAdd ( $this->model_Add );
            }
        }

        return $this->render('index.twig', [
            'data_title'=>$data_title,
            'action_url_question' => $this->action_url_question . $lang . '/',
            'action_url_partners' => $this->action_url_partners . $lang . '/',
            'actionQuestion' => 'actionQuestion',
            'actionAdd' => 'actionAdd',
            'model_Question' => $this->model_Question,
            'model_Add' => $this->model_Add,
            'MES_ERR' => $this->err,
            'MES_SUCCESS' => $this->success,
        ]);
    }

    
    private function actionQuestion ( $model ){

        if ( $model->load( Yii::$app->request->post() ) && $model->validate() ) {

            if( $model->validateCheck() ){
                $params = [
                    ':name' => $model->getName(),
                    ':phone' => $model->getPhone(),
                    ':contact' => $model->getContact(),
                    ':question' => $model->getQuestion()
                ];

                $rez = Yii::$app->db->createCommand('INSERT INTO `question`( `name`, `phone`, `contact`, `question`) 
                                                             VALUES (:name, :phone, :contact, :question )')->bindValues( $params )->execute();

                $admin_email=Yii::$app->params['adminEmail'];

                      Yii::$app->mailer->compose()
                        ->setTo( Yii::$app->params['parthers'] )
                        ->setFrom([$admin_email => $admin_email])
                        ->setSubject("Вам поступил вопрос по сотрудничеству:" )
                        ->setTextBody(
                                "Имя: " . $model->getName() . "\r\n" .
                                "Вопрос: " . $model->getQuestion() . "\r\n" .
                                "Телефон: " . $model->getPhone() . "\r\n"
                                )
                        ->send();

                $this->success = [ 'success' => [ 0 =>Yii::t('app/partners', '_TEXT_14_')] ];
            } else {

                $this->success = [ 'check' => [ 0 =>Yii::t('app/partners', '_TEXT_15_')] ];
            }

        }else {

            if ( $model->hasErrors() ) {
                $this->err = $model->getErrors();
            }
        }
        
        return $model;
    }

    
    public function actionAdd( $model ){
 
         if ( $model->load( Yii::$app->request->post() ) && $model->validate() ) {

            if( $model->validateCheck() ){
                $params = [
                    ':name' => $model->getName(),
                    ':phone' => $model->getPhone(),
                    ':contact' => $model->getContact(),
                    ':question' => $model->getQuestion()
                ];

                $rez = Yii::$app->db->createCommand('INSERT INTO `parthers_add`( `name`, `phone`, `contact`, `question`) 
                                                             VALUES (:name, :phone, :contact, :question )')->bindValues( $params )->execute();

                $admin_email=Yii::$app->params['adminEmail'];

                 $admin_email=Yii::$app->params['adminEmail'];
                 
                Yii::$app->mailer->compose()
                        ->setTo( Yii::$app->params['parthers'] )
                        ->setFrom([$admin_email => $admin_email])
                        ->setSubject("Хочу стать партнером:" )
                        ->setTextBody(
                                "Имя: " . $model->getName() . "\r\n" .
                                "Предложение: " . $model->getQuestion() . "\r\n" .
                                "Телефон: " . $model->getPhone() . "\r\n"
                                )
                        ->send();

                $this->success = [ 'success' => [ 0 =>Yii::t('app/partners', '_TEXT_16_')] ];
            } else {

                $this->err = [ 'check' => [ 0 =>Yii::t('app/partners', '_TEXT_17_')] ];
            }

        }else {

            if ( $model->hasErrors() ) {
                $this->err = $model->getErrors();
            }
        }

        return $model;
    }
    
}
