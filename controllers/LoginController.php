<?php
/**
 * Created by PhpStorm.
 * User: avtorpc
 * Date: 18.03.20
 * Time: 13:39
 */

namespace app\controllers;

use Yii;
use app\models\LoginForm;
use app\models\UserinfoForm;
use app\models\AuthrestForm;
use app\models\UsersForm;
use yii\helpers\Url;
use app\models\Hashs;

class LoginController extends WebAuthController{
    /**
     * Вход на сайт
     *
     * @return type
     */
    public function actionIndex(){

       if ( isset( $this->session['AUTH_DATA']['ROLE']  ) AND  $this->session['AUTH_DATA']['ROLE'] != 'guest' ) {
               Yii::$app->response->redirect( $this->session['AUTH_DATA'] ['HOME_PAGE'] );
       }

       return $this->render('index.twig',[
       'Message' => $this->session->getFlash('err_register'),
       'form_url' => '/login/verification/'
        ]);
   }
   /**
    * Верификация пользователя
    *
    * @return type
    */
    public function actionVerification()
    {
        try{

            $model = new LoginForm;

            if ( $model->load( Yii::$app->request->post() ) && $model->validate() ) {

                $hash = password_hash($model->getPassword(), PASSWORD_DEFAULT);

                $params = [':login' => $model->getLogin()];

                $rez = Yii::$app->db->createCommand('
                        SELECT 
                        USR.id AS user_id, 
                        USR.login AS user_login, 
                        USR.password AS user_password, 
                        USR.role AS user_role, 
                        USR.home_page AS user_home_page, 
                        USR.nickname AS user_nickname,
                        USI.name AS user_name,
                        USI.famuly AS user_famuly,
                        USI.otchestvo AS user_otchestvo,
                        USI.phone  AS user_phone,
                        USI.email AS user_email,
                        USI.id_telegramm AS user_id_telegramm
                        FROM users AS USR
                        LEFT JOIN link_users_users_info AS LUI ON LUI.id_users = USR.id
                        LEFT JOIN user_info AS USI ON USI.id = LUI.	id_users_info
                        WHERE login=:login
                        ')->bindValues( $params )->queryOne();

                if( $rez ) {
                    if (password_verify($model->getPassword(), $rez['user_password'] ) ) {
                        $this->session['AUTH_DATA'] = [
                            'ROLE' => $rez['user_role'],
                            'HOME_PAGE' => $rez['user_home_page'] . $rez['user_id'] .'/',
                            'ID' => $rez['user_id'],
                            'NIK_NAME' => $rez['user_nickname'],
                            'NAME' => $rez['user_name'],
                            'FAMULY' => $rez['user_famuly'],
                            'SECOND_NAME' => $rez['user_otchestvo'],
                            'PHONE' => $rez['user_phone'],
                            'EMAIL' => $rez['user_email'],
                            'ID_TELEGRAM' => $rez['user_id_telegramm']
                        ];
                        Yii::$app->response->redirect( $this->session['AUTH_DATA']['HOME_PAGE'] );
                    } else {

                        $this->session->setFlash('err_register', 'Пользователь не найден!' );

                        Yii::$app->response->redirect( '/login/' );
                    }
                } else {

                    $this->session->setFlash('err_register', 'Пользователь не найден!' );

                    Yii::$app->response->redirect( '/login/' );
                }
            } else {

                Yii::$app->response->redirect( '/login/' );
            }
        } catch ( \Exception $ex ){

            return $this->err($ex);
        }
    }
    /**
     * Выход из сайта
     */
    public function actionLogout()
    {

        $this->session->close();

        $this->session->destroy();

        unset( $this->session['AUTH_DATA'] );

        Yii::$app->response->redirect( '/login/' );
    }



    /**
     * Форма забыл пароль
     */
    public function actionRestorep(){

        $userInfo = new UserinfoForm();

        if( $userInfo->load( Yii::$app->request->post() ) && $userInfo->validate() ){
        if(strval($userInfo->email)){

            $rez = Yii::$app->db->createCommand(
                    "SELECT `lui`.id_users FROM user_info `ui` "
                    . "LEFT JOIN link_users_users_info `lui`ON(`lui`.id_users_info=`ui`.id) "
                    . "WHERE `ui`.email=:email; "
                        )->bindValues([
                    ':email' => $userInfo->email,
            ])->queryScalar();

        if($rez){

            $hesh=sha1(password_hash(md5(date("Y-m-d H:i:s")."ha-ha-he-he-ho-ho"), PASSWORD_DEFAULT));

            $db = Yii::$app->db;
            $transaction = $db->beginTransaction();
            try{

                $rez = Yii::$app->db->createCommand(
                    "INSERT INTO hashs (hashs, users_id, date_out)VALUES "
                    . "('{$hesh}', {$rez}, DATE_ADD(NOW(), INTERVAL 2 HOUR)); "
                    . "DELETE FROM hashs WHERE (NOW() > date_out); "
                        )->execute();
                $transaction->commit();

            }catch(\Exception $e) {
                $this->session->setFlash('msg', 'Системная ошибка! Обратитесть к администратору.');
                $transaction->rollBack();
                throw $e;
                Yii::$app->response->redirect( '/login/' );
            }

            $admin_email=Yii::$app->params['adminEmail'];

            Yii::$app->mailer->compose()
            ->setTo($userInfo->email)
            ->setFrom([$admin_email => $admin_email])
            ->setSubject("EMIZ - Восстановление пароля.")
            ->setTextBody("Двух часовая ссылка для изменения пароля (если вы пароль не забывали не нажимайте на ссыку)- ".Url::home(true)."login/authrest/?AuthrestForm[h]={$hesh}")
            ->send();

            $this->session->setFlash('msg', "На почту {$userInfo->email} было отправлено письмо о восстановлении пароля");

        }else{
            $this->session->setFlash('msg', "Email {$userInfo->email} не существует!");
        }}else{
            $this->session->setFlash('msg', 'Поле email должно быть заполнено');
        }}else {
            if ( $userInfo->hasErrors() ) {
                $this->session->setFlash('msg', 'Ошибка приходящих данных!');
                $this->session->setFlash('msg_arr', $userInfo->getErrors());
            }
        }

        return $this->render('index.twig', [
                "flag_fogot_pass"=>1,
            ]);
    }
    /**
     * Переход по ссылке "Забыл пароль" из почты
     * Форма изменения пароля
     *
     * @return type
     */
    public function actionAuthrest(){

        $authrestForm = new AuthrestForm();//Приходящие данныее GET только $authrestForm->h


        if( $authrestForm->load( Yii::$app->request->get() ) && $authrestForm->validate() ){

            if($authrestForm->getStatus()){

            }else{
                $this->session->setFlash('msg', 'Срок истек!');
            }

        }else {
            if ( $authrestForm->hasErrors() ) {
                $this->session->setFlash('msg', 'Ошибка приходящих данных!');
                $this->session->setFlash('msg_arr', $authrestForm->getErrors());
            }
        }

        return $this->render('index.twig', [
                "AuthrestForm"=>$authrestForm,
            ]);

    }
    /**
     * Форма смены пароля (введите 2 пароля)
     *
     * @throws \Exception
     */
    public function actionChangep(){

        //echo"<pre>";print_r(Yii::$app->request->post());die;

        $authrestForm = new AuthrestForm();

        if( $authrestForm->load( Yii::$app->request->post() ) && $authrestForm->validate() ){
        if($authrestForm->password != '' and $authrestForm->password != ''){

            if($authrestForm->getStatus()){


                //echo "##: ".$authrestForm->getStatus()."<pre>";print_r($authrestForm);die;

                $usersForm = new UsersForm;
                $usersForm->password=$authrestForm->password;


                $db = Yii::$app->db;
                $transaction = $db->beginTransaction();
                try{
                    $rez = Yii::$app->db->createCommand(
                        "UPDATE users SET password=:password WHERE id=:uid; "
                        . "DELETE FROM hashs WHERE users_id=:uid OR hashs_id=:hashs; "
                            )->bindValues([
                        ':uid' => (int)$authrestForm->getStatus(),
                        ':password' => $usersForm->getPass(),
                        ':hashs' => $authrestForm->h,
                    ])->execute();
                    $transaction->commit();

                    $this->session->setFlash('msg', 'Пароль успешно изменен');

                }catch(\Exception $e) {
                    $this->session->setFlash('msg', 'Системная ошибка! Обратитесть к администратору.');
                    $transaction->rollBack();
                    throw $e;
                    Yii::$app->response->redirect( "/login/authrest/?AuthrestForm[h]={$authrestForm->h}" );
                }






                Yii::$app->response->redirect( '/login/' );

            }else{
                $this->session->setFlash('msg', 'Ошибка приходящих данных!');
                Yii::$app->response->redirect( "/login/authrest/?AuthrestForm[h]={$authrestForm->h}" );
            }


            //echo "<pre>";print_r($authrestForm);die;
        }else{
                $this->session->setFlash('msg', 'Поле пароль обязательно!');
                Yii::$app->response->redirect( "/login/authrest/?AuthrestForm[h]={$authrestForm->h}" );
        }}else {
            if ( $authrestForm->hasErrors() ) {
                $this->session->setFlash('msg', 'Ошибка приходящих данных!');
                $this->session->setFlash('msg_arr', $authrestForm->getErrors());
            }
            Yii::$app->response->redirect( "/login/authrest/?AuthrestForm[h]={$authrestForm->h}" );
        }



    }
    /**
     * Запрос на получение ID телеграм
     * 
     * @param type $id
     */
    public function actionGettelegram($id){
        
        if($id){
            $Hashs = new Hashs;
            $Hashs->initTelegram($id);
        }
        
        Yii::$app->response->redirect( "/admin/profil/{$id}/" );
    }
    
    public function actionSettelegram($id){
        if($id){
            $Hashs = new Hashs;
            $Hashs->addIdTelegram($id);
        }
        
        Yii::$app->response->redirect( "/admin/profil/{$id}/" );
    }


}