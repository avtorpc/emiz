<?php
/**
 * Created by PhpStorm.
 * User: avtorpc
 * Date: 2020-04-08
 * Time: 21:28
 */

namespace app\controllers;

use Yii;
class ModerQuestionController extends WebAuthController {

    public function actionIndex(){
        $rez = Yii::$app->db->createCommand('
                     SELECT * FROM `question`  ORDER BY dt_add ASC' )->queryAll();

        return $this->render('index.twig',
            [
                'DATA' => $rez
            ]);
    }
}