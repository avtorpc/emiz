<?php

namespace app\controllers;

use Yii;
use app\models\UsersForm;
use app\models\AdressForm;
use app\models\UserinfoForm;
use app\models\SortByDealer;
use app\assets\AppAsset;
use app\models\CodepForm;
use app\models\ShowBasketItemForm;
use app\models\RepAdmForm;
use app\models\ClientForm;
use yii\helpers\Url;

/**
 * Module admin
 *
 * @author kkk
 */
class AdminController extends WebAuthController {

    use trait_CreateViewBasket;
    use trait_AddGeoSXtoBASE;


    private $basket_table = 'basket';

    /**
     * Главная
     *
     * @return type
     */
    public function actionIndex($id)
    {
        $this->init_();

        $user = new UsersForm;

        //Два последних зарегестрированных диллера
        $dealers = Yii::$app->db->createCommand("SELECT `u`.`id` AS `users_id`,
                                                        `u`.`login` AS `users_login`,
                                                        `i`.`id` AS `info_id`,
                                                        `i`.`name` AS `info_name`,
                                                        `i`.`famuly` AS `info_famuly`,
                                                        `i`.`otchestvo` AS `info_otchestvo`,
                                                        `i`.`birth` AS `info_birth`,
                                                        `i`.`phone` AS `info_phone`,
                                                        `i`.`email` AS `info_email`,
                                                        `luk`.`adrkonts_id`,
                                                        `a`.`city`AS`adr_city`,
                                                        `a`.`street`AS`adr_street`,
                                                        `a`.`house`AS`adr_house`,
                                                        `a`.`korpus`AS`adr_korpus`,
                                                        `a`.`apartament`AS`adr_apartament`,
                                                        `a`.`pochta_index`AS`adr_pindex`,
                                                        (
                                                            SELECT COUNT(*)
                                                            FROM " . $this->basket_table . " AS `b`
                                                            WHERE `b`.`user_id`=`u`.`id`
                                                        )AS`bsk`,
                                                        (
                                                            SELECT COUNT(*)
                                                            FROM `link_basket_basket_items` `lbi`
                                                            LEFT JOIN " . $this->basket_table . " AS `b` ON (`b`.`id`=`lbi`.`id_basket`)
                                                            WHERE `b`.`user_id`=`u`.`id`
                                                        )AS`tvr`,
                                                        (
                                                            SELECT SUM((
                                                                SELECT SUM((`ii`.`unit_price` * `ii`.`cnt`))
                                                                FROM link_basket_basket_items `bb`
                                                                LEFT JOIN `basket_items` `ii` ON(`ii`.`id`=`bb`.`id_basket_item`)
                                                                WHERE `bb`.`id_basket`=`b`.`id`
                                                            ) + `b`.`transport`)
                                                            AS`summ_basket`
                                                            FROM " . $this->basket_table . " AS `b`
                                                            WHERE `b`.`user_id`=`u`.`id`
                                                            GROUP BY `b`.`user_id`
                                                        )AS`summ`
                                                FROM `users` `u`
                                                LEFT JOIN `link_users_users_info` `lui` ON(`lui`.`id_users`=`u`.`id`)
                                                LEFT JOIN `user_info` `i` ON(`i`.`id`=`lui`.`id_users_info`)
                                                LEFT JOIN `link_users_adrkont` `luk` ON(`luk`.`users_id`=`u`.`id`)
                                                LEFT JOIN `adress` `a` ON(`luk`.`adrkonts_id`=`a`.`adress_id`)
                                                WHERE `u`.`role`='dealer'
                                                ORDER BY `u`.`id` DESC
                                                LIMIT 2")->queryAll();


        //Два последних заказа клиентов
        $clients = Yii::$app->db->createCommand("SELECT `b`.`id`AS`basket_id`,
                                                        `b`.`name`AS`info_name`,
                                                        `b`.`surname`AS`info_famuly`,
                                                        `b`.`middle_name`AS`info_otchestvo`,
                                                        `b`.`city`AS`adr_city`,
                                                        `b`.`address`AS`adr_street`,
                                                        `b`.`home`AS`adr_house`
                                                    FROM " . $this->basket_table . " AS `b`
                                                    WHERE `b`.`user_id` IS NULL
                                                    ORDER BY `b`.`id` DESC
                                                    LIMIT 2")->queryAll();


                $dealer = Yii::$app->db->createCommand("SELECT `u`.`id` AS `users_id`,
                                                        `u`.`login` AS `users_login`,
                                                        `i`.`id` AS `info_id`,
                                                        `i`.`name` AS `info_name`,
                                                        `i`.`famuly` AS `info_famuly`,
                                                        `i`.`otchestvo` AS `info_otchestvo`,
                                                        `i`.`birth` AS `info_birth`,
                                                        `i`.`phone` AS `info_phone`,
                                                        `i`.`email` AS `info_email`,
                                                        `luk`.`adrkonts_id`,
                                                        `a`.`city`AS`adr_city`,
                                                        `a`.`street`AS`adr_street`,
                                                        `a`.`house`AS`adr_house`,
                                                        `a`.`korpus`AS`adr_korpus`,
                                                        `a`.`apartament`AS`adr_apartament`,
                                                        `a`.`pochta_index`AS`adr_pindex`,
                                                        (
                                                            SELECT COUNT(*)
                                                            FROM " . $this->basket_table . " AS `b`
                                                            WHERE `b`.`dealer_id`=`u`.`id` AND `b`.`date_in` >=DATE_ADD(NOW(), INTERVAL -1 MONTH)
                                                        )AS`bsk`,
                                                        (
                                                            SELECT COUNT(*)
                                                            FROM `link_basket_basket_items` `lbi`
                                                            RIGHT JOIN " . $this->basket_table . " AS `b`ON(`b`.`id`=`lbi`.`id_basket`)
                                                            WHERE `b`.`dealer_id`=`u`.`id` AND `b`.`date_in` >=DATE_ADD(NOW(), INTERVAL -1 MONTH)
                                                        )AS`tvr`,
                                                        (
                                                            SELECT SUM((
                                                                SELECT SUM((`ii`.`unit_price` * `ii`.`cnt`))
                                                                FROM link_basket_basket_items `bb`
                                                                LEFT JOIN `basket_items` `ii` ON(`ii`.`id`=`bb`.`id_basket_item`)
                                                                WHERE `bb`.`id_basket`=`b`.`id`
                                                            ) + `b`.`transport`)
                                                            AS`summ_basket`
                                                            FROM  " . $this->basket_table . " AS `b`
                                                            WHERE `b`.`dealer_id`=`u`.`id` AND `b`.`date_in` >=DATE_ADD(NOW(), INTERVAL -1 MONTH)
                                                            GROUP BY `b`.`dealer_id`
                                                        )AS`summ`
                                                FROM `users` `u`
                                                LEFT JOIN `link_users_users_info` `lui`ON(`lui`.`id_users`=`u`.`id`)
                                                LEFT JOIN `user_info` `i`ON(`i`.`id`=`lui`.`id_users_info`)
                                                LEFT JOIN `link_users_adrkont` `luk`ON(`luk`.`users_id`=`u`.`id`)
                                                LEFT JOIN `adress` `a`ON(`luk`.`adrkonts_id`=`a`.`adress_id`)
                                                WHERE `u`.`id`=:uid")->bindValues( [
                                                                                        ':uid' => $id
                                                                                    ] )->queryOne();


        $reports = Yii::$app->db->createCommand("SELECT 
                                                    DATE(`b`.`date_in`)AS`ddd`,
                                                    COUNT(DISTINCT `b`.`id`)AS`bsk`,
                                                    COUNT(*)AS`tvr`,
                                                    SUM(`bi`.`unit_price`)AS`summ`
                                                FROM " . $this->basket_table . " AS `b`
                                                LEFT JOIN `link_basket_basket_items` `lbi`ON(`lbi`.`id_basket`=`b`.`id`)
                                                LEFT JOIN `basket_items` `bi`ON(`bi`.`id`=`lbi`.`id_basket_item`)
                                                GROUP BY DATE(`b`.`date_in`)
                                                ORDER BY DATE(`b`.`date_in`) DESC
                                                LIMIT 5")->queryAll();

        $this->view->registerMetaTag(['name'=>'keywords', 'content'=>'Emiz']);
        $this->view->registerMetaTag(['name'=>'description', 'content'=>'Эликсир молодости и здоровья']);
        $this->view->title="Главная";

        return $this->render('index.twig', [
            'user'=>$user->getCurrUser($id),
            'dealers'=>$dealers,
            'dlr'=>$dealer,
            'clients'=>$clients,
            'reports'=>$reports,
                ]);
    }
    /**
     * Профиль
     *
     * @return type
     */
    public function actionProfil($id)
    {
        $flag_to_form2=0;//for js
        $new_pass="";//for js
        $user = new UsersForm;
        $userInfo = new UserinfoForm;

        //Yii::error( print_r(UserinfoForm::$flag_form_personal, 1) , 'oracle_fail');

        if( $user->load( Yii::$app->request->post() ) && $user->validate() ){
        if( $userInfo->load( Yii::$app->request->post() ) && $userInfo->validate() ){
        if($user->password == $user->password_0 and $user->password != ''){
        if($userInfo->email){


            $flag_to_form2=1;
            $this->session['PASSWORD_CHANGE_HESH']=$user->getPass();
            $this->session['PASSWORD_CHANGE_EMAIL']=$userInfo->email;
            $this->session['PASSWORD_CHANGE_CODE']=rand(100000, 999999);

            if($userInfo->sendEmailChangePass(Yii::$app->params['adminEmail'], $this->session['PASSWORD_CHANGE_CODE'])){
                $this->session->setFlash('msg', "На почту {$userInfo->email} было отправлено письмо с кодом подтверждения");
            }else{
                $this->session->setFlash('msg', 'Ошибка отправки почты!');
            }


        }else{
            $this->session->setFlash('msg', 'Поле Email обязательно!');
        }}else{
            $this->session->setFlash('msg', 'Пароли не совпадают!');
        }}else {
            if ( $userInfo->hasErrors() ) {
                $this->session->setFlash('msg', 'Ошибка приходящих данных!');
                $this->session->setFlash('msg_arr', $userInfo->getErrors());
            }
        }}else {
            if ( $user->hasErrors() ) {
                $this->session->setFlash('msg', 'Ошибка приходящих данных!');
                $this->session->setFlash('msg_arr', $user->getErrors());
            }
        }


        $this->view->registerMetaTag(['name'=>'keywords', 'content'=>'Emiz']);
        $this->view->registerMetaTag(['name'=>'description', 'content'=>'Эликсир молодости и здоровья']);
        $this->view->title="Профиль";


        return $this->render('profil.twig', [
                'user'=>$user->getCurrUser($id),
                'dealer'=>$user->getCurrDealer($id),
                'flag_to_form2'=>$flag_to_form2,
                'new_pass'=>$user->password,
                'userInfo'=>$userInfo,
            ]);
    }
    /**
     * Заказы
     *
     * @return type
     */
    public function actionOrders($id)
    {

        $user = new UsersForm;
        $ShowBasketItemForm = new ShowBasketItemForm;

        if ( $ShowBasketItemForm->load( Yii::$app->request->post() ) && $ShowBasketItemForm->validate() ){

        }else {
            if ( $ShowBasketItemForm->hasErrors() ) {
                $this->session->setFlash('msg', 'Ошибка приходящих данных!');
                $this->session->setFlash('msg_arr', $ShowBasketItemForm->getErrors());
            }
        }

        $ShowBasketItemForm->getBasketItem($id);

        //EXCEL
        if($ShowBasketItemForm->excel == 1){
            header('Content-Type: application/csv');
            header('Content-Disposition: attachment; filename="'.$ShowBasketItemForm->name_csv.'.csv"');
            echo \file_get_contents('../'.$ShowBasketItemForm->path_csv.'/user_id_'.$id.'.csv');
            exit;
        }

        $this->view->registerMetaTag(['name'=>'keywords', 'content'=>'Emiz']);
        $this->view->registerMetaTag(['name'=>'description', 'content'=>'Эликсир молодости и здоровья']);
        $this->view->title="Заказы";
        
        return $this->render('orders.twig', [
            'user'=>$user->getCurrUser($id),
            'ShowBasketItemForm'=>$ShowBasketItemForm,
                ]);
    }
    /**
     * Дилеры
     *
     * @return type
     */
    public function actionDealers($id)
    {

        $user = new UsersForm;
        $model = new SortByDealer;
        $ord = "`u`.`id`";//4 - default

        if ( Yii::$app->request->isPost) {
                if ( $model->load( Yii::$app->request->post() ) && $model->validate() ) {

                    if($model->sort == 1){
                        $ord = "`summ`";
                    }elseif($model->sort == 2){
                        $ord = "`tvr`";
                    }elseif($model->sort == 3){
                        $ord = "`bsk`";
                    }elseif($model->sort == 4){
                        $ord = "`u`.`id`";
                    }

                }else{
                    if ( $model->hasErrors() ) {
                        foreach($model->getErrors() as $k=>$v){
                            foreach($v as $vv){
                                $msg .= "$vv";
                            }
                        }
                    }
                }
        }
        
        $dealers = Yii::$app->db->createCommand("SELECT `u`.`id` AS `users_id`,
                                                        `u`.`login` AS `users_login`,
                                                        `i`.`id` AS `info_id`,
                                                        `i`.`name` AS `info_name`,
                                                        `i`.`famuly` AS `info_famuly`,
                                                        `i`.`otchestvo` AS `info_otchestvo`,
                                                        `i`.`birth` AS `info_birth`,
                                                        `i`.`phone` AS `info_phone`,
                                                        `i`.`email` AS `info_email`,
                                                        `luk`.`adrkonts_id`,
                                                        `a`.`city`AS`adr_city`,
                                                        `a`.`street`AS`adr_street`,
                                                        `a`.`house`AS`adr_house`,
                                                        `a`.`korpus`AS`adr_korpus`,
                                                        `a`.`apartament`AS`adr_apartament`,
                                                        `a`.`pochta_index`AS`adr_pindex`,
                                                        (
                                                            SELECT COUNT(*)
                                                            FROM " . $this->basket_table . " AS `b`
                                                            WHERE date_format( date_in, '%Y%m') = date_format(now(), '%Y%m')
                                                        )AS`bsk`,
                                                        (
                                                            SELECT COUNT(*)
                                                            FROM `link_basket_basket_items` `lbi`
                                                            LEFT JOIN " . $this->basket_table . " AS `b`ON(`b`.`id`=`lbi`.`id_basket`)
                                                            WHERE `b`.`user_id`=`u`.`id`
                                                        )AS`tvr`,
                                                        (
                                                            SELECT SUM((
                                                                SELECT SUM((`ii`.`unit_price` * `ii`.`cnt`))
                                                                FROM link_basket_basket_items `bb`
                                                                LEFT JOIN `basket_items` `ii` ON(`ii`.`id`=`bb`.`id_basket_item`)
                                                                WHERE `bb`.`id_basket`=`b`.`id`
                                                            ) + `b`.`transport`)
                                                            AS`summ_basket`
                                                            FROM " . $this->basket_table . " AS `b`
                                                            WHERE `b`.`user_id`=`u`.`id`
                                                            GROUP BY `b`.`user_id`
                                                        )AS`summ`
                                                FROM `users` `u`
                                                LEFT JOIN `link_users_users_info` `lui`ON(`lui`.`id_users`=`u`.`id`)
                                                LEFT JOIN `user_info` `i`ON(`i`.`id`=`lui`.`id_users_info`)
                                                LEFT JOIN `link_users_adrkont` `luk`ON(`luk`.`users_id`=`u`.`id`)
                                                LEFT JOIN `adress` `a`ON(`luk`.`adrkonts_id`=`a`.`adress_id` AND `a`.`active`=1)
                                                WHERE `u`.`role`='dealer'
                                                ORDER BY {$ord} DESC")->queryAll();


        $this->view->registerMetaTag(['name'=>'keywords', 'content'=>'Emiz']);
        $this->view->registerMetaTag(['name'=>'description', 'content'=>'Эликсир молодости и здоровья']);
        $this->view->title="Диллеры";

        return $this->render('dealers.twig', [
            'user'=>$user->getCurrUser($id),
            'dealers'=>$dealers,
            'model'=>$model,
                ]);
    }
    /**
     * Диллер - описание
     *
     * @return type
     */
    public function actionDealer($id)
    {
        //Страница мульти ID - разделение на {$id} & {$user_id}
        $user_id = $this->session['AUTH_DATA']['ID'];

        $dealer_id = Yii::$app->request->get('id_dealer');

        $model = new SortByDealer;
        $user = new UsersForm;

        $ord = "`b`.`id`";//4 - default

        if ( Yii::$app->request->isPost) {
                if ( $model->load( Yii::$app->request->post() ) && $model->validate() ) {

                    if($model->sort == 1){
                        $ord = "`summ_basket`";
                    }elseif($model->sort == 2){
                        $ord = "`tvr`";
                    }elseif($model->sort == 3){
                        $ord = "`tvr`";//повтор так как сортировка ко кол.заказов нет
                    }elseif($model->sort == 4){
                        $ord = "`u`.`id`";
                    }

                }else{
                    if ( $model->hasErrors() ) {
                        foreach($model->getErrors() as $k=>$v){
                            foreach($v as $vv){
                                $msg .= "$vv";
                            }
                        }
                    }
                }
        }

        $sql = "SELECT `b`.`id`AS`basket_id`,
                                                `b`.`name`AS`basket_name`,
                                                `b`.`surname`AS`basket_surname`,
                                                `b`.`middle_name`AS`basket_middle_name`,
                                                `b`.`index`AS`basket_index`,
                                                `b`.`city`AS`basket_city`,
                                                `b`.`address`AS`basket_address`,
                                                `b`.`home`AS`basket_home`,
                                                `b`.`corpus`AS`basket_corpus`,
                                                `b`.`flat`AS`basket_flat`,
                                                `b`.`phone`AS`basket_phone`,
                                                `b`.`email`AS`basket_email`,
                                                `b`.`transport`AS`basket_transport`,
                                                `b`.`pay_type`AS`basket_pay_type`,
                                                `b`.`card_number`AS`basket_card_number`,
                                                `b`.`card_date1`AS`basket_card_date1`,
                                                `b`.`card_date2`AS`basket_card_date2`,
                                                `b`.`card_numberback`AS`basket_card_numberback`,
                                                `b`.`date_in`AS`basket_date_in`,
                                                `b`.`date_close`AS`basket_date_close`,
                                                `b`.`user_id`AS`basket_user_id`,
                                                `b`.`remember`AS`basket_remember`,
                                                `u`.`nickname`AS`users_nickname`,
                                                (
                                                    SELECT SUM((`ii`.`unit_price` * `ii`.`cnt`))
                                                    FROM link_basket_basket_items `bb`
                                                    LEFT JOIN `basket_items` `ii` ON(`ii`.`id`=`bb`.`id_basket_item`)
                                                    WHERE `bb`.`id_basket`=`b`.`id`
                                                )AS`summ_basket`,
                                                (
                                                    SELECT COUNT(*)
                                                    FROM `link_basket_basket_items` `lbi`
                                                    WHERE `lbi`.`id_basket`=`b`.`id`
                                                )AS`tvr`
                                            FROM " . $this->basket_table . " AS `b`
                                            LEFT JOIN `users` `u` ON(`b`.`user_id`=`u`.`id`)
                                            WHERE `b`.`user_id`=:user_id
                                            ORDER BY {$ord} DESC";

        $baskets = Yii::$app->db->createCommand($sql )->bindValues( [':user_id' => $dealer_id] )->queryAll();

        $items = Yii::$app->db->createCommand("SELECT `b`.`id`AS`basket_id`,
                                                `b`.`name`AS`basket_name`,
                                                `b`.`surname`AS`basket_surname`,
                                                `b`.`middle_name`AS`basket_middle_name`,
                                                `b`.`user_id`AS`basket_user_id`,
                                                `u`.`nickname`AS`users_nickname`,
                                                `b`.`phone`AS`basket_phone`,
                                                `b`.`date_in`AS`basket_date_in`,
                                                `i`.`name`AS`basketitems_name`,
                                                `i`.`volume`AS`basketitems_volume`,
                                                `i`.`unit_price`AS`basketitems_unitprice`,
                                                `i`.`cnt`AS`basketitems_cnt`,
                                                `i`.`uniq_id`AS`basketitems_uniq_id`,
                                                `p`.`id`AS`prod_id`,
                                                `g`.`id`AS`good_id`,
                                                `g`.`name`AS`good_name`,
                                                `g`.`uniq_id`AS`good_uniq_id`
                                            FROM `link_basket_basket_items` `lbi`
                                            LEFT JOIN " . $this->basket_table . " AS `b` ON(`b`.`id`=`lbi`.`id_basket`)
                                            LEFT JOIN `users` `u` ON(`b`.`user_id`=`u`.`id`)
                                            LEFT JOIN `basket_items` `i` ON(`i`.`id`=`lbi`.`id_basket_item`)
                                            LEFT JOIN `products` `p` ON(`p`.`uniq_id`=`i`.`uniq_id`)
                                            LEFT JOIN `link_goods_products` `gp` ON(`gp`.`id_products`=`p`.`id`)
                                            LEFT JOIN `goods` `g` ON(`g`.`id`=`gp`.`id_goods`)
                                            WHERE `b`.`user_id`=:user_id")->bindValues( [
                                                                                        ':user_id' => $dealer_id
                                                                                    ] )->queryAll();


        $dealer = Yii::$app->db->createCommand("SELECT `u`.`id` AS `users_id`,
                                                        `u`.`login` AS `users_login`,
                                                        `i`.`id` AS `info_id`,
                                                        `i`.`name` AS `info_name`,
                                                        `i`.`famuly` AS `info_famuly`,
                                                        `i`.`otchestvo` AS `info_otchestvo`,
                                                        `i`.`birth` AS `info_birth`,
                                                        `i`.`phone` AS `info_phone`,
                                                        `i`.`email` AS `info_email`,
                                                        `luk`.`adrkonts_id`,
                                                        `a`.`city`AS`adr_city`,
                                                        `a`.`street`AS`adr_street`,
                                                        `a`.`house`AS`adr_house`,
                                                        `a`.`korpus`AS`adr_korpus`,
                                                        `a`.`apartament`AS`adr_apartament`,
                                                        `a`.`pochta_index`AS`adr_pindex`,
                                                        (
                                                            SELECT COUNT(*)
                                                            FROM " . $this->basket_table . " AS `b`
                                                            WHERE `b`.`user_id`=`u`.`id`
                                                        )AS`bsk`,
                                                        (
                                                            SELECT COUNT(*)
                                                            FROM `link_basket_basket_items` `lbi`
                                                            LEFT JOIN " . $this->basket_table . " AS `b`ON(`b`.`id`=`lbi`.`id_basket`)
                                                            WHERE `b`.`user_id`=`u`.`id`
                                                        )AS`tvr`,
                                                        (
                                                            SELECT SUM((
                                                                SELECT SUM((`ii`.`unit_price` * `ii`.`cnt`))
                                                                FROM link_basket_basket_items `bb`
                                                                LEFT JOIN `basket_items` `ii` ON(`ii`.`id`=`bb`.`id_basket_item`)
                                                                WHERE `bb`.`id_basket`=`b`.`id`
                                                            ) + `b`.`transport`)
                                                            AS`summ_basket`
                                                            FROM " . $this->basket_table . " AS `b`
                                                            WHERE `b`.`user_id`=`u`.`id`
                                                            GROUP BY `b`.`user_id`
                                                        )AS`summ`
                                                FROM `users` `u`
                                                LEFT JOIN `link_users_users_info` `lui`ON(`lui`.`id_users`=`u`.`id`)
                                                LEFT JOIN `user_info` `i`ON(`i`.`id`=`lui`.`id_users_info`)
                                                LEFT JOIN `link_users_adrkont` `luk`ON(`luk`.`users_id`=`u`.`id`)
                                                LEFT JOIN `adress` `a`ON(`luk`.`adrkonts_id`=`a`.`adress_id`)
                                                WHERE `u`.`id`=:uid")->bindValues( [
                                                                                        ':uid' => $dealer_id
                                                                                    ] )->queryOne();


        $geo_list = Yii::$app->db->createCommand("SELECT * FROM `geo_city`")->queryAll();


        $this->view->registerMetaTag(['name'=>'keywords', 'content'=>'Emiz']);
        $this->view->registerMetaTag(['name'=>'description', 'content'=>'Эликсир молодости и здоровья']);
        $this->view->title="Диллер";

        return $this->render('dealer.twig', [
            'user'=>$user->getCurrUser($id),
            'baskets'=>$baskets,
            'dealer'=>$dealer,
            'items'=>$items,
            'model'=>$model,
            ]);
    }
    /**
     * Клиенты
     *
     * @return type
     */
    public function actionClients($id)
    {

        $this->init_();

        $user = new UsersForm;
        $ClientForm = new ClientForm;

        if ( $ClientForm->load( Yii::$app->request->post() ) && $ClientForm->validate() ){

        }else {
            if ( $ClientForm->hasErrors() ) {
                $this->session->setFlash('msg', 'Ошибка приходящих данных!');
                $this->session->setFlash('msg_arr', $ClientForm->getErrors());
            }
        }

        $ClientForm->inits( $this->basket_table);

        $this->view->registerMetaTag(['name'=>'keywords', 'content'=>'Emiz']);
        $this->view->registerMetaTag(['name'=>'description', 'content'=>'Эликсир молодости и здоровья']);
        $this->view->title="Клиенты";

        return $this->render('clients.twig', [
            'user'=>$user->getCurrUser($id),
            'ClientForm'=>$ClientForm,
                ]);
    }
    /**
     * Отчеты
     *
     * @return type
     */
    public function actionReports($id)
    {
        
        $this->init_(); ///$this->basket_table
        $user = new UsersForm;
        $RepAdmForm = new RepAdmForm;

        if ( $RepAdmForm->load( Yii::$app->request->post() ) && $RepAdmForm->validate() ){
            if(!$RepAdmForm->checkDate()){
                $this->session->setFlash('msg', 'Дата начала должна быть меньше даты окончания');
            }
        }else {
            if ( $RepAdmForm->hasErrors() ) {
                $this->session->setFlash('msg', 'Ошибка приходящих данных!');
                $this->session->setFlash('msg_arr', $RepAdmForm->getErrors());
            }
        }

        $RepAdmForm->inits($this->basket_table);

        if(!$RepAdmForm->checkDateMin()){//запускать только после $RepForm->inits($id)
            $this->session->setFlash('msg_arr', ["checkDateMin"=>["На дату начала {$RepAdmForm->buf_dmin} заказов нет, автоустановка на {$RepAdmForm->d_min}"]]);
        }
        if(!$RepAdmForm->checkDateMax()){//запускать только после $RepForm->inits($id)
            if($this->session->hasFlash('msg_arr')){
                $this->session->setFlash('msg_arr', $this->session->getFlash('msg_arr') + ["checkDateMax"=>["На дату окончания {$RepAdmForm->buf_dmax} заказов нет, автоустановка на {$RepForm->d_max}"]]);
            }else{
                $this->session->setFlash('msg_arr', ["checkDateMax"=>["На дату окончания {$RepAdmForm->buf_dmax} заказов нет, автоустановка на {$RepAdmForm->d_max}"]]);
            }
        }

        $this->view->registerMetaTag(['name'=>'keywords', 'content'=>'Emiz']);
        $this->view->registerMetaTag(['name'=>'description', 'content'=>'Эликсир молодости и здоровья']);
        $this->view->title="Отчеты";

        return $this->render('reports.twig', [
            'user'=>$user->getCurrUser($id),
            'RepAdmForm'=>$RepAdmForm,
                ]);
    }
    /**
     * Добавить диллера
     *
     * @param type $id
     * @return type
     * @throws \Exception
     */
    public function actionAdddealer($id)
    {
        $msg=[];
        $rez=0;

        $model = new UsersForm;
        $model_ui = new UserinfoForm;
        $model_a = new AdressForm;

        $model->role="dealer";
        $model->home_page="/dealer/";

        if ( $model->load( Yii::$app->request->post() ) && $model->validate() ){
        if($model->password != ""){
        if ( $model_ui->load( Yii::$app->request->post() ) && $model_ui->validate() ){
        if ( $model_a->load( Yii::$app->request->post() ) && $model_a->validate() ){

            $db = Yii::$app->db;
            $transaction = $db->beginTransaction();
            try{

                Yii::$app->db->createCommand("INSERT INTO users (login, password, role, home_page) VALUES(:login, :password, :role, :home_page)")->bindValues(
                    [   ':login' => $model->login,
                        ':password' => $model->getPass(),
                        ':role' => $model->role,
                        ':home_page' => $model->home_page
                    ])->execute();

                    $dealer_id = $db->getLastInsertID();

                Yii::$app->db->createCommand("INSERT INTO user_info (name, famuly, otchestvo, birth, phone, email) VALUES (:name, :famuly, :otchestvo, :birth, :phone, :email)" )->bindValues( [
                    ':name' => $model_ui->name,
                    ':famuly' => $model_ui->famuly,
                    ':otchestvo' => $model_ui->otchestvo,
                    ':birth' => $model_ui->birth,
                    ':phone' => $model_ui->phone,
                    ':email' => $model_ui->email,
                ])->execute();

                $info_id = $db->getLastInsertID();

                Yii::$app->db->createCommand( "INSERT INTO link_users_users_info (id_users, id_users_info)VALUES( :id_users, :id_users_info)" )
                    ->bindValues( [
                        ':id_users' => $dealer_id,
                        ':id_users_info' => $info_id
                    ])->execute();


                Yii::$app->db->createCommand( "INSERT INTO adress (city, street, house, korpus, apartament, pochta_index, active) "
                    . "VALUES(:city, :street, :house, :korpus, :apartament, :pochta_index, 1); "
                )->bindValues( [
                    ':city' => $model_a->city,
                    ':street' => $model_a->street,
                    ':house' => $model_a->house,
                    ':korpus' => $model_a->korpus,
                    ':apartament' => $model_a->apartament,
                    ':pochta_index' => $model_a->pochta_index,
                ])->execute();

                $addr_id = $db->getLastInsertID();

                Yii::$app->db->createCommand( "INSERT INTO link_users_adrkont (users_id, adrkonts_id)VALUES( :users_id, :adrkonts_id)"
                        )->bindValues([
                    ':users_id' => $dealer_id,
                    ':adrkonts_id' => $addr_id,
                ])->execute();

                $this->updateGeoSxCity( $db, $model_a, $dealer_id  );

                $transaction->commit();

                $msg['msg'] = 'Диллер успешно добавлен';
                $msg['status'] = 1;

            }catch(\Exception $e) {

                $msg['msg'] = 'Системная ошибка! Обратитесть к администратору.';
                $msg['status'] = 0;

                $transaction->rollBack();
                throw $e;
            }

        }else {
            if ( $model_a->hasErrors() ) {
                $msg['msg'] = 'Ошибка приходящих данных!';
                $msg['status'] = 0;
                $msg['err'] = $model_a->getErrors();
            }
        }
        }else {
            if ( $model_ui->hasErrors() ) {
                $msg['msg'] = 'Ошибка приходящих данных!';
                $msg['status'] = 0;
                $msg['err'] = $model_ui->getErrors();
            }
        }
        }else{
                $msg['msg'] = 'Поле пароль обязательно для заполнения!';
                $msg['status'] = 0;
        }
        }else {
            if ( $model->hasErrors() ) {
                $msg['msg'] = 'Ошибка приходящих данных!';
                $msg['status'] = 0;
                $msg['err'] = $model->getErrors();
            }
        }

        return $this->render('adddealer.twig', [
                'msg' => json_encode($msg),
            ]);

    }
    /**
     * Изменить диллера
     *
     * @param type $id
     * @return type
     * @throws \Exception
     */
    public function actionEditdealer($id)
    {
        $msg=[];
        $rez=0;

        $model = new UsersForm;
        $model_ui = new UserinfoForm;
        $model_a = new AdressForm;


        if ( $model->load( Yii::$app->request->post() ) && $model->validate() ){
        if ( $model_ui->load( Yii::$app->request->post() ) && $model_ui->validate() ){
        if ( $model_a->load( Yii::$app->request->post() ) && $model_a->validate() ){
        if(intval($model->id) or intval($model_ui->id) or intval($model_a->adress_id)){

            //Если password - пустой
            if($model->password)
                $pp=$model->getPass();
            else
                $pp="";

            $db = Yii::$app->db;
            $transaction = $db->beginTransaction();
            try{
                $rez = Yii::$app->db->createCommand(
                    /*users*/
                    /*Внимание!!! Пустое поле password - записывать не будет*/
                    "UPDATE users "
                    . "SET login=:login, password=IF(:password, :password, password) "
                    . "WHERE id=:user_id; "

                    /*user_info*/
                    . "UPDATE user_info SET name=:name, famuly=:famuly, otchestvo=:otchestvo, birth=:birth, phone=:phone, email=:email "
                    . "WHERE id=:userinfo_id; "

                    /*adress*/
                    . "UPDATE adress SET city=:city, street=:street, house=:house, korpus=:korpus, apartament=:apartament, pochta_index=:pochta_index "
                    . "WHERE adress_id=:adress_id; "
                        )->bindValues([

                    /*users*/
                    ':login' => $model->login,
                    ':password' => $pp,
                    ':user_id' => (int)$model->id,

                    /*user_info*/
                    ':name' => $model_ui->name,
                    ':famuly' => $model_ui->famuly,
                    ':otchestvo' => $model_ui->otchestvo,
                    ':birth' => $model_ui->birth,
                    ':phone' => $model_ui->phone,
                    ':email' => $model_ui->email,
                    ':userinfo_id' => (int)$model_ui->id,

                    /*adress*/
                    ':city' => $model_a->city,
                    ':street' => $model_a->street,
                    ':house' => $model_a->house,
                    ':korpus' => $model_a->korpus,
                    ':apartament' => $model_a->apartament,
                    ':pochta_index' => $model_a->pochta_index,
                    ':adress_id' => (int)$model_a->adress_id,
                ])->execute();

                $this->updateGeoSxCity( $db, $model_a, $model->id );

                $transaction->commit();

                $msg['msg'] = 'Диллер успешно изменен';
                $msg['status'] = 1;

            }catch(\Exception $e) {

                $msg['msg'] = 'Системная ошибка! Обратитесть к администратору.';
                $msg['status'] = 0;

                $transaction->rollBack();
                throw $e;
            }

        }else{
            $msg['msg'] = 'Ошибка приходящих данных (id)!';
            $msg['status'] = 0;
        }
        }else {
            if ( $model_a->hasErrors() ) {
                $msg['msg'] = 'Ошибка приходящих данных!';
                $msg['status'] = 0;
                $msg['err'] = $model_a->getErrors();
            }
        }
        }else {
            if ( $model_ui->hasErrors() ) {
                $msg['msg'] = 'Ошибка приходящих данных!';
                $msg['status'] = 0;
                $msg['err'] = $model_ui->getErrors();
            }
        }
        }else {
            if ( $model->hasErrors() ) {
                $msg['msg'] = 'Ошибка приходящих данных!';
                $msg['status'] = 0;
                $msg['err'] = $model->getErrors();
            }
        }

        return $this->render('editdealer.twig', [
                'msg' => json_encode($msg),
            ]);

    }
    /**
     * Метод измененя пароля
     *
     * @param type $id
     * @return type
     */
    public function actionChangep($id){

        $codepForm = new CodepForm;

        if ( $codepForm->load( Yii::$app->request->post() ) && $codepForm->validate() ){
            if($this->session['PASSWORD_CHANGE_CODE'] == $codepForm->getCode()){

                $db = Yii::$app->db;
                $transaction = $db->beginTransaction();
                try{
                    $rez = Yii::$app->db->createCommand(
                        "UPDATE users SET password=:password WHERE id=:user_id; "
                        . "SELECT id_users_info INTO @uiid FROM link_users_users_info WHERE id_users=:user_id; "
                        . "UPDATE user_info SET email=:email WHERE id=@uiid; "
                            )->bindValues([
                        ':password' => $this->session['PASSWORD_CHANGE_HESH'],
                        ':email' => $this->session['PASSWORD_CHANGE_EMAIL'],
                        ':user_id' => (int)$id,
                    ])->execute();
                    $transaction->commit();

                    $this->session->setFlash('msg', 'Пароль успешно изменен');

                }catch(\Exception $e) {
                    $this->session->setFlash('msg', 'Системная ошибка! Обратитесть к администратору.');
                    $transaction->rollBack();
                    throw $e;
                }




            }else{
                $this->session->setFlash('msg', 'Ошибка приходящих данных!');
            }
        }else {
            if ( $codepForm->hasErrors() ) {
                $this->session->setFlash('msg', 'Ошибка приходящих данных!');
                $this->session->setFlash('msg_arr', $codepForm->getErrors());
            }
        }


        unset($this->session['PASSWORD_CHANGE_HESH']);
        unset($this->session['PASSWORD_CHANGE_CODE']);
        unset($this->session['PASSWORD_CHANGE_EMAIL']);
        Yii::$app->response->redirect("/admin/profil/{$id}/");


        return $this->render('changep.twig', [
            ]);
    }
    /**
     * Метод изменения личных данных
     * Вызывается из /admin/profil/
     */
    public function actionEditpersonal($id){

        $userInfo = new UserinfoForm;

        if( $userInfo->load( Yii::$app->request->post() ) && $userInfo->validate() ){
            $userInfo->updPersonal($id);
        }else {
            if ( $userInfo->hasErrors() ) {
                $this->session->setFlash('msg', 'Ошибка приходящих данных!');
                $this->session->setFlash('msg_arr', $userInfo->getErrors());
            }
        }

        Yii::$app->response->redirect( "/admin/profil/{$id}/" );
    }
}
