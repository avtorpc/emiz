<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\ContactForm;

class SiteController extends WebAuthController {

    use trait_Catch_Err_Controller;

    public function actionIndex( $lang=null ){

        $this->view->registerMetaTag(['name'=>'keywords', 'content'=>'Emiz']);
        $this->view->registerMetaTag(['name'=>'description', 'content'=>'Эликсир молодости и здоровья']);
        $this->view->title="Главная";

        $data_title=[];
        $data_title['path']="site";
        $data_title['body_class']="home page-template-default page page-id-34 class-name theme-Emiz woocommerce-js woocommerce-active";

        return $this->render('index.twig', [
            'data_title'=>$data_title,
            'url_for_buy' => '/cart/add-basket-from-main-page/' . $lang .'/'
                ]);
    }

    public function actionFault(){
         die( 'errr');
    }

    public function actionError(){

    }
}
