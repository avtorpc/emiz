<?php

namespace app\controllers;


use Yii;
use yii\helpers\Url;

trait trait_DetectCity {

    public function select_user_dealer( $db, $model, $geo_data ){

        $city = ( method_exists( $model,'getCity') ? $model->getCity() : '' );

        switch ( $this->session['AUTH_DATA']['ROLE'] ) {
            case 'guest':

                $rez_city_rus = $db->createCommand ( 'SELECT LUG.`id_user` AS `id`
                                                                     FROM `geo_city` AS GCT LEFT JOIN `link_user_geo_city` AS LUG ON LUG.id_city = GCT.id 
                                                                     WHERE city_rus=:city_rus AND LUG.`id_user` IS NOT NULL')->
                bindValues([ ':city_rus' => $city ] )->queryOne();

                $rez_city_en = $db->createCommand ( 'SELECT LUG.`id_user` AS `id`
                                                                     FROM `geo_city` AS GCT LEFT JOIN `link_user_geo_city` AS LUG ON LUG.id_city = GCT.id 
                                                                     WHERE name_city=:name_city  AND LUG.`id_user` IS NOT NULL' )->
                bindValues([ ':name_city' => ( $city !='' ? $city : $geo_data['city']['name_en'] ) ] )->queryOne();

                $rez_region = $db->createCommand ( 'SELECT LUG.`id_user` AS `id`
                                                                     FROM `geo_city` AS GCT LEFT JOIN `link_user_geo_city` AS LUG ON LUG.id_city = GCT.id 
                                                                     WHERE GCT.name_region =:name_region  AND LUG.`id_user` IS NOT NULL')->
                bindValues([ 'name_region' => $geo_data['region']['name_en'] ] )->queryOne();

                $rez_country = $db->createCommand ( 'SELECT LUG.`id_user` AS `id` 
                                                                     FROM `geo_city` AS GCT LEFT JOIN `link_user_geo_city` AS LUG ON LUG.id_city = GCT.id 
                                                                     WHERE  name_country=:name_country  AND LUG.`id_user` IS NOT NULL')->
                bindValues([ 'name_country' => $geo_data['country']['name_en'] ] )->queryOne();


                if( $rez_city_rus ){
                    $this->dealer_id = $rez_city_rus;
                } elseif( $rez_city_en ) {
                    $this->dealer_id = $rez_city_en;
                } elseif( $rez_region ) {
                    $this->dealer_id = $rez_region;
                } elseif( $rez_country ) {
                    $this->dealer_id = $rez_country;
                } else {
                   // $this->dealer_id  =  $rez_country = $db->createCommand ( "SELECT id FROM `users` WHERE `role` = 'admin' ORDER BY id ASC")->queryOne();
                    $this->dealer_id  =  $rez_country = $db->createCommand ( "SELECT id FROM `users` WHERE `role` = 'admin' AND `login` = 'alim_admin'")->queryOne();
                }

                break;
            case 'dealer':
                $this->dealer_id = $db->createCommand( "SELECT * FROM `users` WHERE `role` = 'admin' AND `login` = 'alim_admin'" )->queryOne();
                $this->user_id = $this->session['AUTH_DATA']['ID'];
                break;
            case 'moder':
                $this->dealer_id = $db->createCommand( "SELECT * FROM `users` WHERE `role` = 'admin' AND `login` = 'alim_admin'" )->queryOne();
                $this->user_id = $this->session['AUTH_DATA']['ID'];
                break;
            case 'admin':
                $this->dealer_id = $this->session['AUTH_DATA']['ID'];
                $this->user_id = $this->session['AUTH_DATA']['ID'];
                break;
        }
    }
}