<?php

namespace app\commands;

use Yii;
class VOLUME_CONVERTER {

    public static function LitersToL( $volume ){
        $e = explode( '.', $volume );
        if( $e[1] =='00'){
            $e[1] = '0';
        } else {
            $e[1] = rtrim( $e[1], '0');
        }
        return implode( ',', $e);
    }

    public static function PriceCropZero( $price ) {

        return (int) $price;
    }
}