<?php

namespace app\commands;

use Yii;
class GET_LANG {

    public static $lang_list = [
                'ru' => 'ru-RU',
                'en' => 'en-EN',
                'ar' => 'ar-AR'
            ];

    public static function lang(){

        return array_search( Yii::$app->language, self::$lang_list );
     }

     public function get_lang_list(){

         return self::$lang_list;
     }

}
