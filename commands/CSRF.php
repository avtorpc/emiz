<?php
/**
 * Created by PhpStorm.
 * User: avtorpc
 * Date: 2020-04-01
 * Time: 16:42
 */

namespace app\commands;

use Yii;

class CSRF {

    public static function get_param(){
        return  Yii::$app->getRequest()->csrfParam;
    }

    public static function get_token(){
       return Yii::$app->getRequest()->getCsrfToken();
    }

    public static function get_html_token(){

        return '<input type="hidden" name="' . self::get_param() . '" value="' . self::get_token() . '"/>';
    }

}