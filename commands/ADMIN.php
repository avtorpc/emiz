<?php
/**
 * Created by PhpStorm.
 * User: avtorpc
 * Date: 2020-04-06
 * Time: 22:49
 */

namespace app\commands;

use Yii;
class ADMIN {

    public static function nikName(){
        if( isset( Yii::$app->session['AUTH_DATA'] ) ){

            return Yii::$app->session['AUTH_DATA']['NIK_NAME'];
        } else {

            return '';
        }
    }

    public static function NAME(){
        if( isset( Yii::$app->session['AUTH_DATA'] ) ){

            return Yii::$app->session['AUTH_DATA']['NAME'];
        } else {

            return '';
        }
    }

    public static function FAMULY(){
        if( isset( Yii::$app->session['AUTH_DATA'] ) ){

            return Yii::$app->session['AUTH_DATA']['FAMULY'];
        } else {

            return '';
        }
    }

    public static function SECOND_NAME(){
        if( isset( Yii::$app->session['AUTH_DATA'] ) ){

            return Yii::$app->session['AUTH_DATA']['SECOND_NAME'];
        } else {

            return '';
        }
    }

    public static function PHONE(){
        if( isset( Yii::$app->session['AUTH_DATA'] ) ){

            return Yii::$app->session['AUTH_DATA']['PHONE'];
        } else {

            return '';
        }
    }

    public static function EMAIL(){
        if( isset( Yii::$app->session['AUTH_DATA'] ) ){

            return Yii::$app->session['AUTH_DATA']['EMAIL'];
        } else {

            return '';
        }
    }
}