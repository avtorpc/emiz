/**
 * Процедура удалеения диллера
 * 
 * CALL sp_delDealerById(33);
 * 
 * Author:  kkk
 * Created: 14 апр. 2020 г.
 */
DROP PROCEDURE IF EXISTS sp_delDealerById;
DELIMITER |
CREATE PROCEDURE sp_delDealerById(IN uid INT)
COMMENT 'Процедура удалеения диллера'
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
BEGIN

    /*adress kontaktniy*/
    DELETE `ak` FROM `adress` `ak`
    LEFT JOIN `link_users_adrkont` `luk` ON(`luk`.`adrkonts_id`=`ak`.`adress_id`)
    WHERE `luk`.`users_id`=uid;

    /*adress dostavku*/
    DELETE `ad` FROM `adress` `ad`
    LEFT JOIN `link_users_adress` `lua` ON(`lua`.`adress_id`=`ad`.`adress_id`)
    WHERE `lua`.`users_id`=uid;

    /*user_info*/
    DELETE `i` FROM `user_info` `i` 
    LEFT JOIN `link_users_users_info` `lui`ON(`lui`.`id_users_info`=`i`.`id`)
    WHERE `lui`.`id_users`=uid;

    /*users*/
    DELETE FROM users WHERE id=uid;

END;
|
DELIMITER ;

