/**
 * Список товаов к заказу
 * 
 * Author:  kkk
 * Created: 14 апр. 2020 г.
 */
DROP VIEW IF EXISTS vw_baskets_prods;
CREATE VIEW vw_baskets_prods AS

SELECT `b`.`id`AS`basket_id`,
    `b`.`date_in`AS`basket_date_in`,
    `b`.`date_close`AS`basket_date_close`,
    `i`.`unit_price`AS`basketitems_unitprice`,
    `i`.`cnt`AS`basketitems_cnt`,
    `b`.`name`AS`basket_name`,
    `b`.`surname`AS`basket_surname`,
    `b`.`middle_name`AS`basket_middle_name`,
    `b`.`user_id`AS`basket_user_id`,
    `u`.`nickname`AS`users_nickname`,
    `b`.`phone`AS`basket_phone`,
    `i`.`name`AS`basketitems_name`,
    `i`.`volume`AS`basketitems_volume`,
    `i`.`uniq_id`AS`basketitems_uniq_id`,
    `p`.`id`AS`prod_id`,
    `g`.`id`AS`good_id`,
    `g`.`name`AS`good_name`,
    `g`.`uniq_id`AS`good_uniq_id`
FROM `link_basket_basket_items` `lbi`
LEFT JOIN `basket` `b` ON(`b`.`id`=`lbi`.`id_basket`)
LEFT JOIN `users` `u` ON(`b`.`user_id`=`u`.`id`)
LEFT JOIN `basket_items` `i` ON(`i`.`id`=`lbi`.`id_basket_item`)
LEFT JOIN `products` `p` ON(`p`.`uniq_id`=`i`.`uniq_id`)
LEFT JOIN `link_goods_products` `gp` ON(`gp`.`id_products`=`p`.`id`)
LEFT JOIN `goods` `g` ON(`g`.`id`=`gp`.`id_goods`)
ORDER BY `b`.`id` DESC;
