/**
 * Список продкутов магазина
 * 
 * Author:  kkk
 * Created: 16 апр. 2020 г.
 */
DROP VIEW IF EXISTS vw_products;
CREATE VIEW vw_products AS

SELECT `p`.`id`AS`prods_id`, 
`p`.`name` AS `prods_name`,  
`p`.`price` AS `prods_price`,
`p`.`volume` AS `prods_volume`,
`p`.`uniq_id` AS `prods_uniqid`,
`p`.`img` AS `prods_img`,
`p`.`home_page` AS `prods_homepage`,
`p`.`active` AS `prods_active`,
`g`.`id`AS`goods_id`,
`g`.`name` AS `goods_name`,
`g`.`uniq_id` AS `goods_uniqid`,
`g`.`short_desc` AS `goods_shortdesc`,
`g`.`notes` AS `goods_notes`,
`g`.`sostav` AS `goods_sostav`,
`g`.`active` AS `goods_active`
FROM `products` `p`
LEFT JOIN `link_goods_products` `lgp` ON(`lgp`.`id_products`=`p`.`id`)
LEFT JOIN `goods` `g` ON(`g`.`id`=`lgp`.`id_goods`);

