/**
 * Список платежных карт привязанных к пользователям
 * 
 * Author:  kkk
 * Created: 2 апр. 2020 г.
 */
DROP VIEW IF EXISTS vw_cards;
CREATE VIEW vw_cards AS 
SELECT `c`.`cards_id`,
`u`.`users_id`,
`u`.`nickname`AS`users_nickname`,
`c`.`cards_type`,
`c`.`cards_name`,
`c`.`cards_num`,
`c`.`date_mes`AS`cards_datemes`,
`c`.`date_god`AS`cards_dategod`,
`c`.`card_numback`
FROM `cards` `c`
LEFT JOIN `link_users_cards` `luc` ON(`luc`.`cards_id`=`c`.`cards_id`)
LEFT JOIN `users` `u` ON(`u`.`users_id`=`luc`.`users_id`);

