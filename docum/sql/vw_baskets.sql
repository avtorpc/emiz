/**
 * Список заказов (корзина)
 * 
 * Author:  kkk
 * Created: 14 апр. 2020 г.
 */
DROP VIEW IF EXISTS vw_baskets;
CREATE VIEW vw_baskets AS 

SELECT `b`.`id`AS`basket_id`,
    `b`.`name`AS`basket_name`,
    `b`.`surname`AS`basket_surname`,
    `b`.`middle_name`AS`basket_middle_name`,
    `b`.`index`AS`basket_index`,
    `b`.`city`AS`basket_city`,
    `b`.`address`AS`basket_address`,
    `b`.`home`AS`basket_home`,
    `b`.`corpus`AS`basket_corpus`,
    `b`.`flat`AS`basket_flat`,
    `b`.`phone`AS`basket_phone`,
    `b`.`email`AS`basket_email`,
    `b`.`transport`AS`basket_transport`,
    `b`.`pay_type`AS`basket_pay_type`,
    `b`.`card_number`AS`basket_card_number`,
    `b`.`card_date1`AS`basket_card_date1`,
    `b`.`card_date2`AS`basket_card_date2`,
    `b`.`card_numberback`AS`basket_card_numberback`,
    `b`.`date_in`AS`basket_date_in`,
    `b`.`date_close`AS`basket_date_close`,
    `b`.`user_id`AS`basket_user_id`,
    `b`.`remember`AS`basket_remember`,
    `u`.`nickname`AS`users_nickname`,
    `i`.`id`AS`uinfo_id`,
    (
        SELECT SUM((`ii`.`unit_price` * `ii`.`cnt`))
        FROM link_basket_basket_items `bb`
        LEFT JOIN `basket_items` `ii` ON(`ii`.`id`=`bb`.`id_basket_item`)
        WHERE `bb`.`id_basket`=`b`.`id`
    )
    AS`summ_basket`
FROM `basket` `b`
LEFT JOIN `users` `u` ON(`b`.`user_id`=`u`.`id`)
LEFT JOIN `link_users_users_info` `lui`ON(`lui`.`id_users`=`u`.`id`)
LEFT JOIN `user_info` `i`ON(`i`.`id`=`lui`.`id_users_info`)
ORDER BY `b`.`id` DESC;
