/**
 * Процедура установить как активную карту
 * 
 * CALL sp_activeCard(54, 2);
 * 
 * Author:  kkk
 * Created: 14 апр. 2020 г.
 */
DROP PROCEDURE IF EXISTS sp_activeCard;
DELIMITER |
CREATE PROCEDURE sp_activeCard(IN cidd INT, IN uidd INT)
COMMENT 'Процедура установить как активную карту'
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
BEGIN
	
	UPDATE card c LEFT JOIN link_users_card luc ON(luc.card_id=c.card_id)
	SET c.active=NULL
	WHERE luc.users_id=uidd AND c.active=1;
	
	SELECT ROW_COUNT() INTO @up_null;
	
	UPDATE card c LEFT JOIN link_users_card luc ON(luc.card_id=c.card_id)
	SET c.active=1
	WHERE luc.users_id=uidd AND c.card_id=cidd; 
  
	SELECT ROW_COUNT() INTO @up_act;
	
	SELECT @up_null AS `up_all_null`, @up_act AS `up_act`;
  
END;
|
DELIMITER ;

