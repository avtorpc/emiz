/**
 * Список пользователей
 * Внимание!!! Подключение обоих адресов(lua и luk) приведет к дублированию пользователей
 * 
 * Author:  kkk
 * Created: 12 апр. 2020 г.
 */
DROP VIEW IF EXISTS vw_users;
CREATE VIEW vw_users AS

SELECT `u`.`id` AS `users_id`,
    `u`.`login` AS `users_login`,
    `u`.`role` AS `users_role`,
    `u`.`home_page` AS `users_homepage`,
    `u`.`nickname` AS `users_nickname`,
    `i`.`name` AS `info_name`,
    `i`.`famuly` AS `info_famuly`,
    `i`.`otchestvo` AS `info_otchestvo`,
    `i`.`birth` AS `info_birth`,
    `i`.`phone` AS `info_phone`,
    `i`.`email` AS `info_email`,
    `i`.`organization` AS `info_organization`,
    /*`lua`.`adress_id`,*/
    `luk`.`adrkonts_id`,
    `a`.`city`AS`adr_city`,
    `a`.`street`AS`adr_street`,
    `a`.`house`AS`adr_house`,
    `a`.`korpus`AS`adr_korpus`,
    `a`.`apartament`AS`adr_apartament`,
    `a`.`pochta_index`AS`adr_pindex`
FROM `users` `u`
LEFT JOIN `link_users_users_info` `lui`ON(`lui`.`id_users`=`u`.`id`)
LEFT JOIN `user_info` `i`ON(`i`.`id`=`lui`.`id_users_info`)
/*LEFT JOIN `link_users_adress` `lua`ON(`lua`.`users_id`=`u`.`id`)*/
LEFT JOIN `link_users_adrkont` `luk`ON(`luk`.`users_id`=`u`.`id`)
LEFT JOIN `adress` `a`ON(`luk`.`adrkonts_id`=`a`.`adress_id` AND `a`.`active`=1);
