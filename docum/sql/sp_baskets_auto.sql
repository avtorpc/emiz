/**
 * Процедура для теста: авто заполненеие корзины
 * 
 * CALL sp_baskets_auto(2);
 */
DROP PROCEDURE IF EXISTS sp_baskets_auto ;
DELIMITER |
CREATE PROCEDURE sp_baskets_auto(IN uss INT)
COMMENT 'Процедура для теста: авто заполненеие корзины'
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER

BEGIN
  
  DECLARE done INT DEFAULT 0;
  DECLARE ddd INT UNSIGNED;
  DECLARE nnn VARCHAR(256);
  DECLARE ppp DECIMAL(10,2);
  DECLARE vvv DECIMAL(10,2);
  DECLARE uuu VARCHAR(128);
  DECLARE cur1 CURSOR FOR SELECT id, `name`, price, volume, uniq_id FROM products ORDER BY RAND() LIMIT 5;
  DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
        
        /*Получть имя пользователя*/
	SELECT i.`name`, i.`famuly`, i.`otchestvo` INTO @n1, @n2, @n3
        FROM users u
        LEFT JOIN `link_users_users_info` `lui`ON(`lui`.`id_users`=`u`.`id`)
        LEFT JOIN `user_info` `i`ON(`i`.`id`=`lui`.`id_users_info`)
        WHERE u.id=uss;

	INSERT INTO basket(user_id, `name`, surname, middle_name, transport,remember)
	VALUES(uss, @n1, @n2, @n3, 300, 'on');
	SET @last=LAST_INSERT_ID();
	
  OPEN cur1;

REPEAT
    FETCH cur1 INTO ddd, nnn, ppp, vvv, uuu;
	IF NOT done THEN
		
		SET @cnt=FLOOR(3 + RAND() * (20 - 3 + 1));/*3-20*/
                
                /*(baskets_id, prods_id, curr_price, cnt) VALUES(@last, idd, cpp, @cnt)*/
		INSERT INTO basket_items (`name`, cnt, unit_price, volume, prod_id, uniq_id)VALUES(nnn, @cnt, ppp, vvv, ddd, uuu);
                SET @last_item=LAST_INSERT_ID();
                
                INSERT INTO link_basket_basket_items (id_basket, id_basket_item)VALUES(@last, @last_item);
		
	END IF;
UNTIL done END REPEAT;

  CLOSE cur1;
END;
|
DELIMITER ;
