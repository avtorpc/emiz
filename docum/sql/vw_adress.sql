/**
 * Список адресов привязанных к пользователям
 * 
 * Author:  kkk
 * Created: 12 апр. 2020 г.
 */
DROP VIEW IF EXISTS vw_adress;
CREATE VIEW vw_adress AS
SELECT `a`.`adress_id`,
    `a`.`region`,
    `a`.`city`,
    `a`.`street`,
    `a`.`house`,
    `a`.`korpus`,
    `a`.`apartament`,
    `a`.`pochta_index`,
    `a`.`active`,
    `u`.`id`AS`users_id`,
    `u`.`nickname`AS`users_nickname`,
    `i`.`id`AS`userinfo_id`,
    `i`.`name`AS`userinfo_name`,
    `i`.`famuly`AS`userinfo_famuly`,
    `i`.`otchestvo`AS`userinfo_otchestvo`,
    `i`.`phone`AS`userinfo_phone`,
    `i`.`email`AS`userinfo_email`
FROM `adress` `a`
LEFT JOIN `link_users_adress` `lua` ON(`lua`.`adress_id`=`a`.`adress_id`)
LEFT JOIN `users` `u` ON(`u`.`id`=`lua`.`users_id`)
LEFT JOIN `link_users_users_info` `lui`ON(`lui`.`id_users`=`u`.`id`)
LEFT JOIN `user_info` `i`ON(`i`.`id`=`lui`.`id_users_info`);