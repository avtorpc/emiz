<?php

return [
        '_TEXT_1_' => 'Наши продукты разработаны для здорового образа жизни, а так же входят в сегмент товаров "Здоровое питание".',
        '_TEXT_2_' => 'ВСЕГДА НА СВЯЗИ',
        '_TEXT_3_' => 'Для вашего удобстав мы на связи с Вами 24/7.',
        '_TEXT_4_' => 'задать вопрос',
        '_TEXT_5_' => 'РАДЫ ПАРТНЕРАМ',
        '_TEXT_6_' => 'Мы рады интересным партнерствам. Давайте вместе развивать полезный продукт.',
        '_TEXT_7_' => 'стать партнером',
        '_TEXT_8_' => 'Предложение №1',
        '_TEXT_8_1_' => 'Свяжитесь с нами для получения более подробной информации о наших продуктах и услугах. Наши опытные консультанты помогут вам выбрать нужный продукт и дадут рекомендации по сопутствующим товарам и вариантам доставки.<br><br> Вы можете связаться с нашими консультантами ежедневно с 8:00 до 20:00. В целях безопасности возможна запись и контроль телефонных звонков в службу работы с клиентами.',
        '_TEXT_9_' => 'Предложение №2',
        '_TEXT_10_' => 'Свяжитесь с нами для получения более подробной информации о наших продуктах и услугах. Наши опытные консультанты помогут вам выбрать нужный продукт и дадут рекомендации по сопутствующим товарам и вариантам доставки.<br><br> Вы можете связаться с нашими консультантами ежедневно с 8:00 до 20:00. В целях безопасности возможна запись и контроль телефонных звонков в службу работы с клиентами.',
        '_TEXT_11_' => 'Предложение №3',
        '_TEXT_12_' => 'Свяжитесь с нами для получения более подробной информации о наших продуктах и услугах. Наши опытные консультанты помогут вам выбрать нужный продукт и дадут рекомендации по сопутствующим товарам и вариантам доставки.<br><br> Вы можете связаться с нашими консультантами ежедневно с 8:00 до 20:00. В целях безопасности возможна запись и контроль телефонных звонков в службу работы с клиентами.',
        '_TEXT_13_' => 'Стать <br>партнером',
        '_TEXT_14_' => 'Ваш вопрос успешно сохранен!',
        '_TEXT_15_' => 'Вы не дали согласие на обработку ваших данных.',
        '_TEXT_16_' => 'Ваш запрос успешно сохранен!',
        '_TEXT_17_' => 'Вы не дали согласие на обработку ваших данных.',
        '_TEXT_18_' => 'Имя',
        '_TEXT_19_' => 'Телефон',
        '_TEXT_20_' => 'Введите ваш вопрос',
        '_TEXT_21_' => 'Ваше предложение',
        '_TEXT_22_' => 'Поле Вопрос не может быть пустым',
        '_TEXT_23_' => 'Поле Телефон не может быть пустым',
        '_TEXT_24_' => 'Поле Имя не может быть пустым',
        '_TEXT_25_' => 'Нужно согласие на использование ваших данных',
        '_TEXT_26_' => 'Emiz, Партнерам',
        '_TEXT_27_' => 'Эликсир молодости и здоровья',
        '_TEXT_28_' => 'Партнерам'
    ];
