<?php

return [
    '_EMIZ_TAVR_GOOD_SHORT_DESC_' => 'Настощий концентрат здоровья – изготовлен только из мякоти, но и кожуры, гребня и косточки экологического чистого винограда.',
    '_EMIZ_TAVR_GOOD_SOSTAV_' => 'виноград сортов Каберне-Совиньон, Саперави, Мерло.',
    '_EMIZ_TAVR_GOOD_NOTE_1_' => '360 ПОЛИФЕНОЛОВ-АНТИОКСИДАНТОВ',
    '_EMIZ_TAVR_GOOD_NOTE_2_' => 'ИЗ КРЫМСКОГО ВИНОГРАДА 3 СОРТОВ',
    '_EMIZ_TAVR_GOOD_NOTE_3_' => 'БЕЗ САХАРА И КОНСЕРВАНТОВ',
    '_NAME_' => 'ЭМИЗ таврический',
    '_EMIZ_TAVR_GOOD_LONG_DESC_' => '<h3 class="products-posts__title">
                        Эмиз. Чистый виноград.
                    </h3>
                    <div class="products-posts__it"></div>
                    <div class="products-posts__text">
                        <p>Эмиз – это виноград и только виноград: в нем нет сахара и консервантов. Виноград 3 сортов (Каберне-Совиньон, Саперави и Мерло) собирается вручную в экологически чистых районах Крыма, на диких полях.
                        <p>Эмиз – это концентрат здоровья. Рекомендуется разводить его водой в пропорции 1:6. Можно разводить и в меньшей пропорции – перед вкусом Эмиза трудно устоять.
                        <p>Обычно Эмиз принимают во время еды. Если у вас нет хронических заболеваний желудочно-кишечного тракта, можно принимать Эмиз и в другое время – заряд бодрости гарантирован!
                        <p>Чтобы Эмиз не терял свои уникальные свойства, после открытия его нужно хранить в холодильнике с полностью закрытой пробкой!
                    </div>
                    <div class="products-posts__frame">
                        <div class="plyr__video-embed js-player" id="player">
                            <iframe
                                src="https://www.youtube.com/embed/P2ie22LtwsQ?origin=https://plyr.io&iv_load_policy=3&modestbranding=1&playsinline=1&showinfo=0&rel=0&enablejsapi=1"
                                allowfullscreen allowtransparency allow="autoplay"></iframe>
                        </div>
                        <h2 class="products-posts__frametitle">
                            Сосуды начинают правильно работать и выравнивается давление
                        </h2>
                    </div>
                    <div class="products-posts-extra">
                        <div class="products-posts-extra__left">
                            <div class="plyr__video-embed js-player">
                                <iframe
                                    src="https://www.youtube.com/embed/1KxmS2iUCeI?origin=https://plyr.io&iv_load_policy=3&modestbranding=1&playsinline=1&showinfo=0&rel=0&enablejsapi=1"
                                    allowfullscreen allowtransparency allow="autoplay"></iframe>
                            </div>
                            <h3 class="products-posts-extra__title">
                                Сосуды начинают правильно работать и выравнивается давление
                            </h3>
                        </div>
                        <div class="products-posts-extra__right">
                            <div class="plyr__video-embed js-player">
                                <iframe
                                    src="https://www.youtube.com/embed/pmHK_688U8w?origin=https://plyr.io&iv_load_policy=3&modestbranding=1&playsinline=1&showinfo=0&rel=0&enablejsapi=1"
                                    allowfullscreen allowtransparency allow="autoplay"></iframe>
                            </div>
                            <h3 class="products-posts-extra__title">
                                Сосуды начинают правильно работать и выравнивается давление
                            </h3>
                        </div>
                    </div>'
    ];
