<?php

return [
        '_TEXT_1_' => 'ЧАСТО ЗАДАВАЕМЫЕ ВОПРОСЫ',
        '_TEXT_2_' => 'Наши продукты разработаны для здорового образа жизни, а так же входят в сегмент товаров "Здоровое питание".',
        '_TEXT_3_' => 'Все',
        '_TEXT_4_' => 'Доставка',
        '_TEXT_5_' => 'употребление',
        '_TEXT_6_' => 'Что такое Эмиз и чем он полезен?',
        '_TEXT_7_' => 'Эмиз – это виноград и только виноград: в нем нет сахара и консервантов. 
                                    Виноград 3 сортов (Каберне-Совиньон, Саперави и Мерло) собирается вручную в экологически чистых
                                    районах Крыма, на диких полях. Чтобы получить Эмиз мы собираем виноград 3 сортов и 
                                    экстрактируем из него полифенолы. Мякоти недостаточно – мы выжимаем максимум из 
                                    кожуры, гребня и косточки, чтобы получить более 360 полифенолов, которые являются 
                                    мощными антиоксидантами. Вкус Эмиз отличается яркостью и богатством оттенков. 
                                    Несмотря на молодой возраст Эмиз уже получил в 2018 году престижную награду международной выставки «Золотая гроздь 2018».',
        '_TEXT_8_' => 'Что такое полифенолы?',
        '_TEXT_9_' => 'Полифенолы – это антиоксиданты. Именно они противостоят свободным радикалам, прерывая губительный процесс окисления и повреждения клеток. 
                                    Главный среди антиоксидантов – ресвератол. Он задерживает рост раковых клеток и                                 
                                    подавляет их. Ресвератол оказывает противовоспалительное и антибактериальное действие.                                
                                    Полифенолы поддерживают функциональную способность тромбоцитов, обеспечивая                                 
                                    свободный кровоток в сосудах и нормализуя уровень холестерина в крови.                                 
                                    Полифенолы стимулируют рост коллагеновых волокон, поддерживая гладкость кожи и                                 
                                    предотвращая ее преждевременное старение.                                 
                                    Полифенолы повышают устойчивость организма к стрессам и помогают в периоды усталости                                 
                                    и быстрой утомляемости.                                 
                                    Эффективность и механизм действия полифенолов при различных заболеваниях хорошо                                 
                                    изучены – вы можете самостоятельно найти информацию по конкретному заболеванию в интернете.  ',
        '_TEXT_10_' => 'Как принимать Эмиз?',
        '_TEXT_11_' => 'Эмиз – это концентрат здоровья. Рекомендуется разбавлять его водой по вкусу. Перед Эмизом трудно устоять. Обычно Эмиз принимают во время еды. 
                                    Если у вас нет хронических заболеваний желудочно-кишечного тракта, можно принимать Эмиз и в другое время – заряд бодрости гарантирован! 
                                    Чтобы Эмиз не терял свои уникальные свойства, после открытия его нужно хранить в холодильнике с полностью закрытой пробкой!',
        '_TEXT_12_' => 'Условия доставки',
        '_TEXT_13_' => 'Доставке осуществляется транспортными компаниями и Почтой России. Возможен самовывоз в пунктах выдачи компаний-партнёров.',
        '_TEXT_14_' => 'спросить консультанта',
        '_TEXT_15_' => 'Ваш вопрос успешно сохранен!',
        '_TEXT_16_' => 'Вы не дали согласие на обработку ваших данных.',
        '_TEXT_17_' => 'Emiz, Часто задаваемые вопросы',
        '_TEXT_18_' => 'Эликсир молодости и здоровья',
        '_TEXT_19_' => 'Введите ваш вопрос'
    ];