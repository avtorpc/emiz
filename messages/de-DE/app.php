<?php
/**
 * Created by PhpStorm.
 * User: avtorpc
 * Date: 13.07.18
 * Time: 16:32
 */

return [
    'Username should contain at least 11 characters.' => 'Die Bedeutungen sollen in den Grenzen von 11 bis zu 30 Zeichen sein.',
    'Password should contain at least 11 characters.' => 'Die Bedeutungen sollen in den Grenzen von 11 bis zu 30 Zeichen sein.',
    'Password should contain at most 30 characters.' => 'Die Bedeutungen sollen in den Grenzen von 11 bis zu 30 Zeichen sein.',
    'Username should contain at most 30 characters.' => 'Die Bedeutungen sollen in den Grenzen von 11 bis zu 30 Zeichen sein.',
    'Username cannot be blank.' => 'Das Feld kann leer nicht sein.',
    'Password cannot be blank.' => 'Das Feld kann leer nicht sein.',
    'Name' => 'das Login',
    'Password' => 'das Passwort',
    'Submit' => 'Schicken',
    'Name cannot be empty' => 'Das Login kann leer nicht sein.',
    'Password cannot be empty' => 'Die Parole kann leer nicht sein.',
    'Login Page' => 'Die Seite der Autorisation',
    'Summ' => 'Die Summe für die Aufnahme auf das Konto',
    'Exit' => 'die Abmeldung',
    'Err close session' => 'Der Fehler des Ausgangs',
    'Close' => 'Zu schließen',
    'System tiket' => 'Die Systemmitteilung',
    'Summ cannot be blank.' => "Die Summe kann leer nicht sein.",
    'Trn Id cannot be blank.' => 'Die Nummer der Transaktion ist verloren.'
];