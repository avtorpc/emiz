<?php

return [
        '_TEXT_1_' => 'FAQ',
        '_TEXT_2_' => 'EMIZ IS DESIGNED FOR A HEALTHY LIFESTYLE AND FALL INTO THE WELLNESS PRODUCTS CATEGORY.',
        '_TEXT_3_' => 'ALL',
        '_TEXT_4_' => 'DELIVERY',
        '_TEXT_5_' => 'ABOUT EMIZ',
        '_TEXT_6_' => 'WHAT IS EMIZ AND WHAT IS IT THE VALUE OF THIS DRINK?',
        '_TEXT_7_' => 'Emiz is made of grapes and only grapes: it contains no sugar or preservatives. Grapes of 3 varieties (cabernet sauvignon, saperavi and merlot) are harvested by hand in ecologically clean areas of crimea, in wild fields. To get emiz, we collect grapes of 3 varieties and extract polyphenols from it. The pulp is not enough - we squeeze the maximum out of the skin, peduncle and seed to get more than 360 polyphenols, which are powerful antioxidants. The taste of emiz is bright and rich in shades. The taste of emiz is bright and rich in shades. Despite young age of the emiz brand it already received the prestigious award of the «golden bunch 2018» at international exhibition.',
        '_TEXT_8_' => 'WHAT ARE POLYPHENOLS?',
        '_TEXT_9_' => "Polyphenols are antioxidants. It is they who resist free radicals, interrupting the destructive process of oxidation and damage to cells. The main antioxidant is resveratol. It retards the growth of cancer cells and inhibits them. Resveratol has anti-inflammatory and antibacterial effects. Polyphenols support the functional ability of platelets, providing free blood flow in the vessels and normalizing the level of cholesterol in the blood. Polyphenols stimulate the growth of collagen fibers, maintaining the smoothness of the skin and preventing its premature aging. Polyphenols increase the body's resistance to stress and help during periods of tiredness and rapid fatigue. The effectiveness and mechanism of action of polyphenols in various diseases has been well studied - you can independently find information on a specific disease on the internet.",
        '_TEXT_10_' => 'HOW TO TAKE EMIZ?',
        '_TEXT_11_' => "Emiz is a health concentrate. It is recommended to dilute it with water to taste. It's hard to resist emiz. Emiz is usually taken with meals. If you do not have chronic diseases of the gastrointestinal tract, you can take emiz at other times - a charge of vigor is guaranteed! So that emiz does not lose its unique properties, after opening it, it must be stored in the refrigerator with the cork completely closed!",
        '_TEXT_12_' => 'DELIVERY CONDITIONS',
        '_TEXT_13_' => 'Delivery is carried out by transport companies and russian post. Pickup is possible at points of delivery of partner companies.',
        '_TEXT_14_' => 'ASK CONSULTANT',
        '_TEXT_15_' => 'Your question has been accepted!',
        '_TEXT_16_' => 'Вы не дали согласие на обработку ваших данных.',
        '_TEXT_17_' => 'Emiz, Часто задаваемые вопросы',
        '_TEXT_18_' => 'ELIXIR OF LIFE AND WELLNESS',
        '_TEXT_19_' => 'Enter your question'
    ];