<?php

return [
    '_PASTA_GOOD_SHORT_DESC_' => 'It is made of a bunch of grapes (seed, skin and pulp of grapes) with the addition of blue clay (source of selenium, magnesium, calcium).',
    '_PASTA_GOOD_SOSTAV_' => 'Grape of cabernet sauvignon, saperavi, merlo varieties.',
    '_PASTA_GOOD_NOTE_1_' => 'FROM CRIMEAN GRAPES OF 3 VARIETIES',
    '_PASTA_GOOD_NOTE_2_' => 'EMIZ PLUS TANNINS AND PECTINS',
    '_PASTA_GOOD_NOTE_3_' => 'WITHOUT SUGAR AND PRESERVATIVES',
    '_NAME_' => 'ПАСТА',
    '_PASTA_GOOD_LONG_DESC_' => '<h3 class="products-posts__title">
                        EMIZ. PURE GRAPES.
                    </h3>
                    <div class="products-posts__it"></div>
                    <div class="products-posts__text">
                        <p>Emiz is made of grapes and only grapes: it contains no sugar or preservatives. Grapes of 3 varieties (cabernet sauvignon, saperavi and merlot) are harvested by hand in ecologically clean areas of crimea, in wild fields.</p>
                        <p>Emiz is a health concentrate. It is recommended to dilute with water in a ratio of 1: 6. You can breed in a smaller proportion - it’s hard to resist the taste of emiz.</p>
                        <p>Emiz is usually taken with meals. If you do not have chronic diseases of the gastrointestinal tract, you can take emiz anytime - a charge of vigor is guaranteed!</p>
                        <p>In order that emiz does not lose its unique properties, after opening it, it must be stored in the refrigerator with the cork completely closed!</p>
                    </div>
                    <div class="products-posts__frame">
                        <div class="plyr__video-embed js-player" id="player">
                            <iframe
                                src="https://www.youtube.com/embed/P2ie22LtwsQ?origin=https://plyr.io&iv_load_policy=3&modestbranding=1&playsinline=1&showinfo=0&rel=0&enablejsapi=1"
                                allowfullscreen allowtransparency allow="autoplay"></iframe>
                        </div>
                        <h2 class="products-posts__frametitle">
                            Blood vessels begin to work correctly and the pressure is normalized
                        </h2>
                    </div>
                    <div class="products-posts-extra">
                        <div class="products-posts-extra__left">
                            <div class="plyr__video-embed js-player">
                                <iframe
                                    src="https://www.youtube.com/embed/1KxmS2iUCeI?origin=https://plyr.io&iv_load_policy=3&modestbranding=1&playsinline=1&showinfo=0&rel=0&enablejsapi=1"
                                    allowfullscreen allowtransparency allow="autoplay"></iframe>
                            </div>
                            <h3 class="products-posts-extra__title">
                                Blood vessels begin to work correctly and the pressure is normalized
                            </h3>
                        </div>
                        <div class="products-posts-extra__right">
                            <div class="plyr__video-embed js-player">
                                <iframe
                                    src="https://www.youtube.com/embed/pmHK_688U8w?origin=https://plyr.io&iv_load_policy=3&modestbranding=1&playsinline=1&showinfo=0&rel=0&enablejsapi=1"
                                    allowfullscreen allowtransparency allow="autoplay"></iframe>
                            </div>
                            <h3 class="products-posts-extra__title">
                                Blood vessels begin to work correctly and the pressure is normalized
                            </h3>
                        </div>
                    </div>'
    ];
