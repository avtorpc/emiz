<?php

return [
        '_TEXT_1_' => 'EMIZ IS DESIGNED FOR A HEALTHY LIFESTYLE AND FALL INTO THE WELLNESS PRODUCTS CATEGORY.',
        '_TEXT_2_' => 'ALWAYS IN TOUCH',
        '_TEXT_3_' => 'For your convenience, we are available 24/7.',
        '_TEXT_4_' => 'ASK A QUESTION',
        '_TEXT_5_' => 'GLAD TO PARTNERS',
        '_TEXT_6_' => "We are glad to have mutually beneficial partnerships. Let's develop a useful product together.",
        '_TEXT_7_' => 'APPLY FOR PARTNERSHIP',
        '_TEXT_8_' => 'OFFER № 1',
        '_TEXT_8_1_' => 'Contact us for more information about our products. Our experienced consultants will help you choose the right product and give recommendations on related products and delivery options.',
        '_TEXT_9_' => 'OFFER № 2',
        '_TEXT_10_' => 'Contact us for more information about our products. Our experienced consultants will help you choose the right product and give recommendations on related products and delivery options. You can contact our consultants daily from 8:00 to 20:00. For security reasons, calls can be recorded and controlled by customer service.',
        '_TEXT_11_' => 'OFFER № 3',
        '_TEXT_12_' => 'Contact us for more information about our products. Our experienced consultants will help you choose the right product and give recommendations on related products and delivery options. You can contact our consultants daily from 8:00 to 20:00. For security reasons, calls can be recorded and controlled by customer service.',
        '_TEXT_13_' => 'APPLY FOR PARTNERSHIP',
        '_TEXT_14_' => 'Your question has been accepted!',
        '_TEXT_15_' => 'You have not given consent to the processing of your data.',
        '_TEXT_16_' => 'Your question has been accepted!',
        '_TEXT_17_' => 'You have not given consent to the processing of your data.',
        '_TEXT_18_' => 'Name',
        '_TEXT_19_' => 'Phone',
        '_TEXT_20_' => 'Enter your question',
        '_TEXT_21_' => 'Your offer',
        '_TEXT_22_' => 'Question field cannot be empty',
        '_TEXT_23_' => 'Phone number field cannot be empty',
        '_TEXT_24_' => 'Name field cannot be empty',
        '_TEXT_25_' => 'Please confirm your consent to the processing of your data',
        '_TEXT_26_' => 'Emiz, for Partners',
        '_TEXT_27_' => 'ELIXIR OF LIFE AND WELLNESS',
        '_TEXT_28_' => 'FOR PARTNERS'
    ];
